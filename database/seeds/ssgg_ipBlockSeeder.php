<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\ssgg_ipBlock;


class ssgg_ipBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 50; $i++) { 
    		factory(ssgg_ipBlock::class)->create();
    	}
    }
}
