<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\vul_sites;


class vul_sitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vul_sites')->insert([
            'client' => 1,
            'name' => 'SITE-TEST',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
