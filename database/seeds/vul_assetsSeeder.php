<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\vul_assets;

class vul_assetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vul_assets')->insert([
            'ip' => '10.155.255.20',
            'site' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('vul_assets')->insert([
            'ip' => '10.155.255.21',
            'site' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('vul_assets')->insert([
            'ip' => '10.155.255.22',
            'site' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
