<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;


class cf_typosquattingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cf_typosquatting')->insert([
            'domain' => 1,
            'typo' => 'examplr.com',
            'ip' => '81.82.83.85',
            'ns' => 'ns1.example.com',
            'mx' => 'mx0.example.com',
            'status' => 'Activo',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('cf_typosquatting')->insert([
            'domain' => 1,
            'typo' => 'example.com.com',
            'ip' => '81.82.83.86',
            'ns' => 'ns1.example.com',
            'mx' => 'mx0.example.com',
            'status' => 'Activo',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('cf_typosquatting')->insert([
            'domain' => 1,
            'typo' => 'exanple.com',
            'ip' => '81.82.83.95',
            'ns' => 'ns1.example.com',
            'mx' => 'mx0.example.com',
            'status' => 'Activo',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('cf_typosquatting')->insert([
            'domain' => 1,
            'typo' => 'examplee.com',
            'ip' => '81.82.83.115',
            'ns' => 'ns1.example.com',
            'mx' => 'mx0.example.com',
            'status' => 'Activo',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('cf_typosquatting')->insert([
            'domain' => 1,
            'typo' => 'example.ru',
            'ip' => '81.82.83.18',
            'ns' => 'ns1.example.com',
            'mx' => 'mx0.example.com',
            'status' => 'Activo',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('cf_typosquatting')->insert([
            'domain' => 1,
            'typo' => 'exam-ple.com',
            'ip' => '81.82.83.41',
            'ns' => 'ns1.example.com',
            'mx' => 'mx0.example.com',
            'status' => 'Activo',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
