<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\groups;

class groupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            'name' => 'Servicios Gestionados',
            'owner' => '1',
            'users' => '[1,2,4,5]',
        ]);
    }
}
