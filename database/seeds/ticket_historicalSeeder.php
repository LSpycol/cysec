<?php

use Illuminate\Database\Seeder;

class ticket_historicalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
        {"title":"[Cliente] SQL Injection attack on website.com","type":"Servicios Gestionados","status":"Abierto","priority":"Alto","asigned_to":1,"solution":null,"client":1}
        */

        DB::table('ticket_historical')->insert([

        	'id_ticket' => 1,
        	'created_at' => now(),
        	'updated_at' => now(),
            'user' => 0,
            'text' => 'Se ha creado el ticket',
        ]);

        DB::table('ticket_historical')->insert([

            'id_ticket' => 1,
            'created_at' => now(),
            'updated_at' => now(),
            'user' => 0,
            'actualization' => 'Contactar con equipo forense',
            'text' => 'Se ha actualizado el ticket',
        ]);

        DB::table('ticket_historical')->insert([

            'id_ticket' => 2,
            'created_at' => now(),
            'updated_at' => now(),
            'user' => 0,
            'text' => 'Se ha creado el ticket',
        ]);

        DB::table('ticket_historical')->insert([

        	'id_ticket' => 2,
            'created_at' => now(),
            'updated_at' => now(),
            'user' => 1,
            'text' => 'Se ha modificado el ticket',
            'changes_before' => '{"status":"Abierto", "asigned_to": null}',
            'changes_after' => '{"status":"En Proceso", "asigned_to": 1}',
        ]);


    }
}
