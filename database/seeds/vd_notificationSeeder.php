<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\vd_notifications;

class vd_notificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 50; $i++) { 
    		factory(vd_notifications::class)->create();
    	}
    }
}
