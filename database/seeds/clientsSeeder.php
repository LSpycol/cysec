<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\clients;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \App\permissionModel;

class clientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = new permissionModel;

        $cliente = DB::table('clients')->insert([
            'name' => 'Prueba',
            'ip' => '10.1.5.0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $permisos->crearPermisos(1);

        for ($i=0; $i < 4; $i++) { 
    		$client = factory(clients::class)->create();
            $permisos->crearPermisos($client->id);
    	}
    }
}
