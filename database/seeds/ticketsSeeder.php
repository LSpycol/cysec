<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\tickets;

class ticketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tickets')->insert([
            'creation' => now(),
            'title' => '[Cliente] Apache Struts Jakarta on 192.168.10.20',
            'type' => 'Servicios Gestionados',
            'status' => 'Abierto',
            'priority' => 'Alto',
            'generated' => 'Automático',
            'client' => 1,
        ]);

        DB::table('tickets')->insert([
            'creation' => now(),
            'title' => '[Cliente] SQL Injection attack on website.com',
            'type' => 'Servicios Gestionados',
            'status' => 'En Proceso',
            'priority' => 'Alto',
            'generated' => 'Automático',
            'assigned_to_group' => 1,
            'client' => 1,
        ]);

    	for ($i=0; $i < 100; $i++) { 
    		factory(tickets::class)->create();
    	}

        $tickets = App\tickets::all();
        $faker = Faker\Factory::create();
        foreach ($tickets as $ticket) {
            if($ticket->status == 'Cerrado'){
                $ticket->closed = $ticket->creation;
                $ticket->update();
            }
        }
    }
}
