<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(clientsSeeder::class);
        $this->call(ticketsSeeder::class);
        $this->call(ticket_historicalSeeder::class);
        $this->call(ticket_infoSeeder::class);
        $this->call(type_ticketsSeeder::class);
        $this->call(userSeeder::class);
        $this->call(groupsSeeder::class);
        $this->call(assetsSeeder::class);
        $this->call(servicesSeeder::class);
        $this->call(ssgg_ipBlockSeeder::class);
        $this->call(ssgg_engineSeeder::class);
        $this->call(vul_sitesSeeder::class);
        $this->call(vul_scansSeeder::class);
        $this->call(vul_assetsSeeder::class);
        $this->call(vul_vulnerabilitysSeeder::class);
        $this->call(vul_occurrencesSeeder::class);
        $this->call(cf_domainSeeder::class);
        $this->call(cf_typosquattingSeeder::class);
        $this->call(anvd_productsSeeder::class);
        $this->call(vd_wordsSeeder::class);
        $this->call(vd_notificationSeeder::class);
    }
}
