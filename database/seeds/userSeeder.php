<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\User;


class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@cysec.com',
            'password' => bcrypt('admin'),
            'language' => 'Español',
            'phone' => '918282737',
            'role' => 'Administrador'
        ]);

        DB::table('users')->insert([
            'name' => 'Analista de Servicios Gestionados',
            'email' => 'analyst@cysec.com',
            'password' => bcrypt('analista'),
            'language' => 'Español',
            'phone' => '918282739',
            'role' => 'Analista'
        ]);

        DB::table('users')->insert([
            'name' => 'Cliente',
            'email' => 'client@cysec.com',
            'password' => bcrypt('cliente'),
            'language' => 'Español',
            'phone' => '918282324',
            'role' => 'Cliente'
        ]);

        for ($i=0; $i < 20; $i++) { 
    		factory(User::class)->create();
    	}


    }
}
