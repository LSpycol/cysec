<?php

use Illuminate\Database\Seeder;

class ssgg_engineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ssgg_engine')->insert([
            'autoassign_group' => 1,
            'regexpr' => '.*[Web Attack].*',
            'position' => 1,
            'client' => 1,
            'created_at' => now()
        ]);

        DB::table('ssgg_engine')->insert([
            'autoassign_group' => 1,
            'regexpr' => '.*account blocked.*',
            'position' => 2,
            'client' => 1,
            'created_at' => now()
        ]);

        DB::table('ssgg_engine')->insert([
            'autoassign_group' => 1,
            'regexpr' => '.*[scan].*',
            'type_close' => 'Autocerrar',
            'position' => 3,
            'client' => 1,
            'created_at' => now()
        ]);
    }
}