<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\vul_scans;

class vul_scansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vul_scans')->insert([
            'type' => 'Pruebas',
            'site' => 1, 
            'template' => 'Sin-template',
            'engine' => 'sin-engine',
            'vulnerabilitys' => 100,
            'start_at' => now(),
            'finish_at' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
