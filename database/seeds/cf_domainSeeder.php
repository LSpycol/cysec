<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class cf_domainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cf_domains')->insert([
            'domain' => 'example.com',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
