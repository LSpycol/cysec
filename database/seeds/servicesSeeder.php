<?php

use Illuminate\Database\Seeder;

class servicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ssgg_services')->insert([
            'name' => 'Arcsight ESM',
            'service' => 'arc_arsight',
            'asset' => 1,
            'client' => 1,
            'info' => 'Servicio del ESM.',
            'status' => "Up"
        ]);

        DB::table('ssgg_services')->insert([
            'name' => 'Conector FWD -> CySec',
            'service' => 'arc_superconnector_CySec',
            'asset' => 1,
            'client' => 1,
            'info' => 'Servicio del del conector que hace forwarding al portal.',
            'status' => "Down"
        ]);

        DB::table('ssgg_services')->insert([
            'name' => 'Arcsight Databse',
            'service' => 'arc_db',
            'asset' => 1,
            'client' => 1,
            'info' => 'Servicio de la base de datos de Arcsight.',
            'status' => "Up"
        ]);

        DB::table('ssgg_services')->insert([
            'name' => 'Arcsight logger',
            'service' => 'arc_arsight',
            'asset' => 2,
            'client' => 1,
            'info' => 'Servicio del logger.',
            'status' => "Down"
        ]);
    }
}
