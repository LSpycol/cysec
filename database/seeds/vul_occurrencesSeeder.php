<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\vul_occurrences;


class vul_occurrencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 100; $i++) { 
            factory(vul_occurrences::class)->create();
        }
    }
}
