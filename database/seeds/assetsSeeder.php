<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class assetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ssgg_assets')->insert([
            'name' => '[Cliente] Arcsight ESM',
            'ip' => '192.168.0.250',
            'client' => 1,
            'info' => 'Máquina de la consola de Arcsight.',
            'status' => "Up"
        ]);

        DB::table('ssgg_assets')->insert([
            'name' => '[Cliente] Arcsight Logger',
            'ip' => '192.168.0.251',
            'client' => 1,
            'info' => 'Máquina del logger de Arcsight.',
            'status' => "Down"
        ]);

        DB::table('ssgg_assets')->insert([
            'name' => '[Cliente] Arcsight ArcMC',
            'ip' => '192.168.0.252',
            'client' => 1,
            'info' => 'Máquina de conectores de Arcsight.',
            'status' => "Down"
        ]);
    }
}
