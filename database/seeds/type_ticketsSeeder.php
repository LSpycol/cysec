<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use \App\type_ticket;


class type_ticketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_ticket')->insert([
            'type' => 'Servicios Gestionados',
            'short' => 'MSS',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('type_ticket')->insert([
            'type' => 'Vulnerabilidades',
            'short' => 'Vul',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('type_ticket')->insert([
            'type' => 'Ciber Fraude',
            'short' => 'CF',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('type_ticket')->insert([
            'type' => 'Vigilancia Digital',
            'short' => 'VD',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
