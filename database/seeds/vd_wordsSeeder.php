<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class vd_wordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vd_words')->insert([
            'client' => 1,
            'word' => 'ejemplo',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('vd_words')->insert([
            'client' => 1,
            'word' => 'accidente',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('vd_words')->insert([
            'client' => 1,
            'word' => 'politico',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
