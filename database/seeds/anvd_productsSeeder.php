<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;


class anvd_productsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('anvd_products')->insert([
            'vendor' => 'microsoft',
            'product' => 'windows_10',
            'email' => '',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('anvd_products')->insert([
            'vendor' => 'oracle',
            'product' => 'solaris',
            'email' => '',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('anvd_products')->insert([
            'vendor' => 'debian',
            'product' => 'debian_linux',
            'email' => '',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('anvd_products')->insert([
            'vendor' => 'apache',
            'product' => 'apache_server',
            'email' => '',
            'client' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
