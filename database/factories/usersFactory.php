<?php

use Faker\Generator as Faker;

$factory->define(\App\User::class, function (Faker $faker) {
    return [
        'name' =>$faker->name,
        'email' =>$faker->email,
        'phone' =>$faker->e164PhoneNumber,
        'language' =>'Español',
        'password' => bcrypt($faker->password),
        'role' => $faker->randomElement($array = array ('Analista','Cliente')),
    ];
});
