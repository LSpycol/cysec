<?php

use Faker\Generator as Faker;

$factory->define(\App\vd_notifications::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement($array = array ('Notificación','Nota','Alerta')),
        'title' => $faker->sentence,
        'source' => $faker->word,
        'link' => $faker->url,
        'tool' => $faker->randomElement($array = array ('Portal','Herramienta1','Herramienta2')),
        'notificated_at' => $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now', $timezone = null),
        'client' => 1,
        'created_at' => $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now', $timezone = null)
    ];
});
