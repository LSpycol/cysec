<?php

use Faker\Generator as Faker;

$factory->define(\App\tickets::class, function (Faker $faker) {
    return [
        'creation' => $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now', $timezone = null),
        'title' => $faker->sentence,
        'type' => $faker->randomElement($array = array ('Vigilancia Digital','Ciber Fraude','Servicios Gestionados', 'Vulnerabilidades')),
        'status' => $faker->randomElement($array = array ('Abierto','En proceso','Cerrado')),
        'priority' => $faker->randomElement($array = array ('Bajo','Medio','Alto', 'Crítico')),
        'generated' => $faker->randomElement($array = array ('Manual','Automático')),
        'client' => $faker->numberBetween($min = 1, $max = 5),
    ];
});
