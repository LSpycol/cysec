<?php

use Faker\Generator as Faker;

$factory->define(\App\clients::class, function (Faker $faker) {
    return [
        'name' => 'Client: ' . $faker->word(1),
        'ip' => $faker->ipv4,
    ];
});
