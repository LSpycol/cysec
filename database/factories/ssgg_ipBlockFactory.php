<?php

use Faker\Generator as Faker;

$factory->define(\App\ssgg_ipBlock::class, function (Faker $faker) {
    return [
        //
        'blocked_at' => $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now', $timezone = null),
        'ip' => $faker->ipv4,
        'client' => $faker->numberBetween($min = 1, $max = 5),
        'reason' => $faker->sentence,
        'created_at' => $faker->dateTimeBetween($startDate = '-1 month', $endDate = 'now', $timezone = null),
    ];
});
