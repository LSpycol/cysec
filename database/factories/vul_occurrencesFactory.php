<?php

use Faker\Generator as Faker;

$factory->define(\App\vul_occurrences::class, function (Faker $faker) {
    return [
        'asset' => $faker->numberBetween(1, 3),
        'port' => $faker->numberBetween(20, 2000),
        'status' => $faker->randomElement($array = array ('Pendiente','Activa','Asumida', 'Corregida', 'Descartada')),
        'vulnerability' => $faker->numberBetween(1, 5),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
