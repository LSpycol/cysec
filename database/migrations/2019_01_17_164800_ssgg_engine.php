<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SsggEngine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('ssgg_engine', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoassign_group')->nullable();
            $table->string('regexpr');
            $table->string('type_close')->nullable();
            $table->integer('position')->nullable();
            $table->integer('client');    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
