<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketHistorical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(500);
        Schema::create('ticket_historical', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ticket');
            $table->string('user');
            $table->string('changes_before')->nullable();
            $table->string('changes_after')->nullable();
            $table->string('text')->nullable();
            $table->string('actualization')->nullable();
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
