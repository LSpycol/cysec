<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VulScans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('vul_scans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('site');
            $table->string('template');
            $table->string('engine');
            $table->integer('vulnerabilitys');
            $table->timestamp('start_at'); 
            $table->timestamp('finish_at')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
