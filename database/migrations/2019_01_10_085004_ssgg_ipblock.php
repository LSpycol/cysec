<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SsggIpblock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('ssgg_ipBlock', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('blocked_at')->nullable();
            $table->string('ip');
            $table->integer('client');
            $table->string('reason')->nullable();
            $table->timestamp('revision')->nullable();     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
