<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('creation')->nullable();
            $table->string('title');
            $table->string('type');
            $table->string('status');
            $table->string('priority');
            $table->string('generated');
            $table->string('assigned_to_group')->nullable();
            $table->timestamp('closed')->nullable();
            $table->string('solution')->nullable();
            $table->integer('client');          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
