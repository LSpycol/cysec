<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CfTyposquatting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('cf_typosquatting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domain');
            $table->string('typo');
            $table->string('ip')->nullable();
            $table->string('ns')->nullable();
            $table->string('mx')->nullable();
            $table->string('status')->nullable();  
            $table->integer('client');
            $table->timestamp('reported_at')->nullable();
            $table->timestamp('discarted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
