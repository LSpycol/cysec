import datetime
import time
import json
from pprint import pprint
import requests
import zipfile
import StringIO
import os
from configuracion import *


url = 'https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-recent.json.zip'

def buscarcve(cve, cur):
	sql = "SELECT * FROM vul_vulnerabilitys where cve = '" + cve + "'"
	cur.execute(sql)
	vul = cur.fetchone()
	return vul

def obtenerIdyCliente(product, datos):
	
	idyCliente = list()
	for d in datos:
		if d[1] == product:
			idyCliente.append(d[0])
			idyCliente.append(d[3])
	return idyCliente
try:
	r = requests.get(url, stream=True)
	z = zipfile.ZipFile(StringIO.StringIO(r.content))
	z.extractall()

	db = connection()
	cur = db.cursor()

	ts = time.time()
	now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

	sql = "SELECT id, product, vendor, client FROM anvd_products"
	cur.execute(sql)
	products_db = cur.fetchall()

	product_list = list()
	for p in products_db:
		product_list.append(p[1])
	
	with open("./nvdcve-1.0-recent.json", "r") as nist:
			data_all = json.load(nist)['CVE_Items']
	
			errores = 0
			vul_nuevas = 0
			oc_nuevas = 0
	
			for data in data_all:
	
				cve = data['cve']['CVE_data_meta']['ID']
				if('2019' in cve):
	
					try:
						try:
							vendor = data['cve']['affects']['vendor']['vendor_data'][0]['vendor_name']
							product = data['cve']['affects']['vendor']['vendor_data'][0]['product']['product_data'][0]['product_name']
						except:
							vendor  = 0
							product = 0

						description = data['cve']['description']['description_data'][0]['value']
						severidad = data['impact']['baseMetricV3']['cvssV3']['baseScore']
						published = data['publishedDate']

						if(severidad > 5 and product in product_list):

							#buscamos si esta la vulnerabilidad en la BD
							vul = buscarcve(cve, cur)
							datos_producto = obtenerIdyCliente(product, products_db)

							if vul:
								id_vul = vul[0]
								#si esta la vulnerabilidad, buscamos si esta la ocurrencia
								sql = "SELECT * FROM anvd_occurrences WHERE vulnerability = " + str(id_vul) + " AND client = " + str(datos_producto[1])
								cur.execute(sql)
								occ = cur.fetchone()
								
								if not occ:
									#creamos la ocurrencia
									sql = "INSERT INTO `anvd_occurrences`(`id`, `product_id`, `vulnerability`, `status`, `client`, `created_at`, `updated_at`) VALUES (NULL," + str(datos_producto[0]) + "," + str(id_vul) + ",'Activa'," + str(datos_producto[1]) + ", '"+ now + "','" + now + "')"
									cur.execute(sql)
									oc_nuevas += 1
							else:
								#creamos la vulnerabilidad
								sql = "INSERT INTO `vul_vulnerabilitys`(`id`, `name`, `severity`, `published`, `description`, `cve`, `created_at`, `updated_at`) VALUES (null,'" + cve + "'," + str(severidad) + ", '" + published + "', '" + description + "', '" + cve + "', '"+ now + "','" + now + "')"
								cur.execute(sql)
								id_vul = cur.lastrowid

            					#creamos la ocurrencia
								sql = "INSERT INTO `anvd_occurrences`(`id`, `product_id`, `vulnerability`, `status`, `client`, `created_at`, `updated_at`) VALUES (NULL," + str(datos_producto[0]) + "," + str(id_vul) + ",'Activa'," + str(datos_producto[1]) + ", '"+ now + "','" + now + "')"
								cur.execute(sql)
								vul_nuevas += 1
								oc_nuevas += 1

							#print('[' + vendor + ' - ' + product + '] (' + cve + '): ' + str(severidad))
	
					except Exception as e:
						errores += 1
	

			#print (str(vul_nuevas) + ' nuevas vulnerabilidades. ' + str(oc_nuevas) + ' nuevas ocurrencias.')
			log_cve(vul_nuevas, oc_nuevas)

	if os.path.exists("./nvdcve-1.0-recent.json"):
		os.remove("./nvdcve-1.0-recent.json")

	db.commit()
	db.close()
except Exception as e:
	print('Error al descargar el archivo de Nist')
	log_cve(0, 0, 1)