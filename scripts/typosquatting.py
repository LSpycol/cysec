# coding=utf-8
###################################
#
# Busca todas las variaciones de typosquatting de los dominios dados de alta. Utiliza el script dnstwist
#
# Por: Luis Mate Carmona
# https://github.com/elceef/dnstwist
#
####################################


import pymysql
import datetime
import time
from configuracion import *


#-----------------------------
#----------------------------
#---------------------------
db = connection()
cur = db.cursor()

ts = time.time()
now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

sql = "SELECT * FROM cf_domains"
cur.execute(sql)
assets = cur.fetchall()

for a in assets:
	idDomain = a[0]
	domain = a[1]
	client = a[2] 

	try:
        #[[ejecutar dnstiwtt]] python dnstwist.py --format csv --registered  DOMINIO > out.csv

		with open("./out.csv", "r") as nist:
			contenido =  nist.readlines()
			cantidad = 0
			i  = 2
			while i < len(contenido):
				linea = contenido[i].split(',')

				typo = linea[1]
				ip = linea[2]
				mx = linea[4]
				ns = linea[5]

				sql = "SELECT * FROM cf_typosquatting WHERE typo = '" + typo + "'"
				cur.execute(sql)
				if( not cur.fetchone()):
					sql = "INSERT INTO `cf_typosquatting`(`id`, `domain`, `typo`, `ip`, `ns`, `mx`, `status`, `client`, `reported_at`, `discarted_at`, `created_at`, `updated_at`) VALUES (null," + str(idDomain) + ",'" + typo + "','" + ip + "','" + ns + "','" + mx +"','Activo'," + str(client) + ",null,null,'" + now + "','" + now + "')"
					cur.execute(sql)
					cantidad += 1

				i += 1

		log_typo(domain, cantidad)

	except Exception as e:
		log_typo(domain, cantidad, 1)


	#if os.path.exists("./out.csv"):
	#	os.remove("./out.csv")
db.commit()
db.close()