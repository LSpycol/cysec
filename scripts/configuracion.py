###################################
#
# Configuracion de la base de datos y funciones utiles
#
# Por: Luis Mate Carmona
#
#
####################################


import pymysql
import datetime
import time


def connection():
  try:
    db = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='cysec')
    return db
  except:
    print('Error in Database')


def log_typo(domain, cantidad, error = 0):
	ts = time.time()
	now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	with open("./logs/log_typosquatting.log", "a") as myfile:
		if(error):
			myfile.write(now + ": Error al obtener typosquatting de '" + domain + "'.\n")
		else:	
			myfile.write(now + ": Typosquatting obtenido de '" + domain + "' -> " + str(cantidad) + " registros.\n")

def log_cve(vul_nuevas, oc_nuevas, error = 0):
	ts = time.time()
	now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	with open("./logs/log_cve_nist.log", "a") as myfile:
		if(error):
			myfile.write(now + ": Error al descargar el archivo de nist.\n")
		else:	
			myfile.write(now + ": " +  str(vul_nuevas) + ' nuevas vulnerabilidades. ' + str(oc_nuevas) + ' nuevas ocurrencias.\n')


def log_noticias(noticias, error = 0):
	ts = time.time()
	now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	with open("./logs/log_noticias.log", "a") as myfile:
		if(error):
			myfile.write(now + ": Error al encontrar noticias.\n")
		else:	
			myfile.write(now + ": " +  str(noticias) + ' nuevas noticias encontradas.\n')