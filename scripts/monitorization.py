###################################
#
# Realiza peticiones SNMP a todos los assets y comprueba el estado los servicios
#
# Por: Luis Mate Carmona
# https://blyx.com/2008/10/04/instalacion-y-configuracion-del-servidor-snmp-en-linux-redhat-way/
#
####################################


import pymysql
from fabric import Connection
from pysnmp.hlapi import *
import datetime
import time
from configuracion import *

def getSnmp(ip):
  oid = '1.3.6.1.2.1.25.4.2.1.2'
  comunidad = 'clave'
  procesos = ''
  for (errorIndication,
       errorStatus,
       errorIndex,
       varBinds) in nextCmd(SnmpEngine(),
                            CommunityData(comunidad),
                            UdpTransportTarget((ip, 161), timeout=0.5, retries=1),
                            ContextData(),
                            ObjectType(ObjectIdentity(oid)),
                            lookupMib=False):

      for varBind in varBinds:
        procesos = procesos + ', ' + varBind[1].prettyPrint()
        print(varBind)
        if oid not in varBind[0].prettyPrint():
          return procesos;
  return procesos

#-----------------------------
#----------------------------
#---------------------------
db = connection()
cur = db.cursor()

sql = "SELECT * FROM ssgg_assets"
cur.execute(sql)
assets = cur.fetchall()

sql = "SELECT * FROM ssgg_services"
cur.execute(sql)
services = cur.fetchall()

ts = time.time()
now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')


#(1, '[Cliente] Arcsight ESM', '192.168.0.250', 1, 'Máquina de la consola de Arcsight.', 
	#'Up', None, None, None, None, None)
for r in assets:
  id = r[0]
  ip = r[2]

  #obtenemos todos los servicios corriendo
  services_snmp = getSnmp(ip)
  print(services_snmp)
  #obtenemos todos los servicios que hay que monitorizar
  services_bd = []
  for s in services:
    service_id = s[0]
    service_ps = s[2]
    service_asset = s[3]
    if (id == service_asset):
      if service_ps in services_snmp:
        print('Asset: ' + ip +  '. Servicio: ' + service_ps + ' UP.')

        sql = "UPDATE ssgg_services SET status = 'Up', lastCheck = " + now + " WHERE id = " + id
        cur.execute(sql)

  db.commit()

db.close()