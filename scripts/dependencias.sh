#!/bin/bash

python -m pip install --upgrade pip

#https://snimpy.readthedocs.io/en/latest/installation.html
pip install mysql-connector-python
pip install pymysql
pip install setuptools
pip install --upgrade setuptools

#https://github.com/elceef/dnstwist
pip install dnspython 
pip install python-geoip
pip install whois 
pip install requests 
pip install cffi
yum install python-ssdeep