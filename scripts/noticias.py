# -*- coding: utf-8 -*- 
###################################
#
# Visita las paginas de los periodicos y busca noticias de cada palabra de cada cliente.
# No crea nueva noticias si ya estan en la base de datos.
#
# Por: Luis Mate Carmona
# 
#
####################################

import sys
import datetime
import time
import urllib3
from bs4 import BeautifulSoup
from configuracion import *

reload(sys)  
sys.setdefaultencoding('utf8')

enlace = ['https://www.abc.es', 'https://www.elpais.com', 'https://www.elmundo.es']

ts = time.time()
now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')


db = connection()
cur = db.cursor()
sql = "SELECT word, client FROM vd_words"
cur.execute(sql)
palabras = cur.fetchall()

contador = 0

try:
	for e in enlace:
		http = urllib3.PoolManager()

		r = http.request('GET', e)
		soup = BeautifulSoup(r.data, 'html.parser')

		noticias = soup.find_all('a')
		for n in noticias:
			for p in palabras:
				if p[0].lower() in n.get_text().lower():
					if '.es' in n.get('href') or '.com' in n.get('href'):
						enlace = n.get('href')
					else:
						enlace = e + n.get('href')
					
					titulo = n.get_text().lower()

					sql = "SELECT * FROM vd_news WHERE client = " + str(p[1]) + " AND title='" + titulo + "'"
					cur.execute(sql)
					encontrado = cur.fetchone()

					if not encontrado:
						origen = e.replace("https://www.", "")
						origen = origen.replace(".com", "").replace(".es", "")
						sql = "INSERT INTO `vd_news`(`id`, `title`, `source` ,`link`, `status`, `client`, `created_at`, `updated_at`) VALUES (NULL,'" + titulo + "', '" + origen + "' , '" + enlace + "','Activa'," + str(p[1]) + ",'" + now + "','" + now + "')"
						cur.execute(sql)

						contador += 1

	log_noticias(contador)
	db.commit()
	db.close()
except Exception as e:
	print e
	log_noticias(0, 1)