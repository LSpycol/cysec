<?php

/***** LOGIN ******/

/* activar esto para ver las consultar sql y el tiempo que tardan */
/*
DB::listen(function($query){
	echo "<pre>{$query->sql} - {$query->time}</pre>";
});
*/
/* ******************** */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'ticketsController@main');

Route::get('/general/loadClients', 'controlPanelController@loadClients')
	->name('loadClients');

Route::post('/general/changeClient', 'controlPanelController@changeClient')
	->name('changeClient');

/***** TICKETS *****/

Route::get('/tickets','ticketsController@main')
	->name('tickets.main');

Route::get('/tickets/listado','ticketsController@listado')
	->name('tickets.listado');

Route::get('/tickets/ticket/{ticket}','ticketsController@individual')
	->where('id', '[0-9]+')
	->name('tickets.individual');

Route::get('/tickets/nuevo','ticketsController@nuevo')
	->name('tickets.nuevo');

Route::post('/tickets/crear','ticketsController@crear_ticket')
	->name('tickets.crear_ticket');

Route::get('/tickets/editar/{ticket}','ticketsController@editar')
	->where('id', '[0-9]+')
	->name('tickets.editar');

Route::put('/tickets/editar_ticket/{ticket}','ticketsController@editar_ticket')
	->where('id', '[0-9]+')
	->name('tickets.editar_ticket');

Route::get('/tickets/actualizar/{ticket}','ticketsController@actualizar')
	->where('ticket', '([0-9]+\,?)+')
	->name('tickets.actualizar');

Route::post('/tickets/actualizar/{tickets}','ticketsController@actualizar_tickets')
	->name('tickets.actualizar_ticket');

Route::get('/tickets/editar_varios/{ticket}','ticketsController@editar_varios')
	->where('ticket', '([0-9]+\,?)+')
	->name('tickets.editar_varios');

Route::post('/tickets/editar_varios/{ticket}','ticketsController@editar_varios_tickets')
	->name('tickets.editar_varios_tickets');

Route::get('/tickets/exportar','ticketsController@exportar_tickets')
	->name('tickets.exportar_tickets');

/***** CONTROL PANEL ******/
Route::get('/control','controlPanelController@main')
	->name('control.main');

Route::put('/editar','controlPanelController@editar_datos')
	->name('control.editar_datos');

Route::put('/password','controlPanelController@password')
	->name('control.password');

/*clientes*/
Route::get('/control/clientes','clientsPanelController@clientes')
	->name('control.clientes');

Route::get('/control/clientes/nuevo','clientsPanelController@nuevo_cliente')
	->name('control.nuevo_cliente');

Route::get('/control/clientes/editar/{client}','clientsPanelController@editar_cliente')
	->name('control.editar_cliente');

Route::put('/control/clientes/editar/{client}','clientsPanelController@edicion_cliente')
	->name('control.edicion_cliente');

Route::get('/control/clientes/eliminar/{client}','clientsPanelController@eliminar_cliente')
	->name('control.eliminar_cliente');

Route::post('/control/clientes/crear','clientsPanelController@crear_cliente')
	->name('control.crear_cliente');

/*usuarios*/
Route::get('/control/usuarios','usersPanelController@usuarios')
	->name('control.usuarios');

Route::get('/control/usuarios/nuevo','usersPanelController@nuevo_usuario')
	->name('control.nuevo_usuario');

Route::post('/control/usuarios/crear','usersPanelController@crear_usuario')
	->name('control.crear_usuario');

Route::get('/control/usuarios/privilegios/{user}','usersPanelController@privilegios_usuario')
	->name('control.privilegios_usuario');

Route::post('/control/usuarios/privilegios/{user}','usersPanelController@privilegios_usuario_guardar')
	->name('control.privilegios_usuario_guardar');

Route::get('/control/usuarios/eliminar/{user}','usersPanelController@eliminar_usuario')
	->name('control.eliminar_usuario');

/*grupos*/
Route::get('/control/grupos','groupsPanelController@grupos')
	->name('control.grupos');

Route::get('/control/grupos/nuevo','groupsPanelController@nuevo_grupo')
	->name('control.nuevo_grupo');

Route::post('/control/grupos/crear','groupsPanelController@crear_grupo')
	->name('control.crear_grupo');

Route::get('/control/grupos/eliminar/{group}','groupsPanelController@eliminar_grupo')
	->name('control.eliminar_grupo');

Route::get('/control/grupos/editar/{group}','groupsPanelController@editar_grupo')
	->name('control.editar_grupo');

Route::put('/control/grupos/editar/{group}','groupsPanelController@edicion_grupo')
	->name('control.edicion_grupo');
/*****  *****/

/***** SERVICIOS GESTIONADOS *****/
Route::get('/ssgg','ssggController@main')
	->name('ssgg.main');

Route::get('/ssgg/monitorizacion','ssggController@monitorizacion')
	->name('ssgg.monitorizacion');

Route::get('/ssgg/nuevoAsset','ssggController@nuevo_asset')
	->name('ssgg.nuevo_asset');

Route::post('/ssgg/nuevoAsset','ssggController@crear_asset')
	->name('ssgg.crear_asset');

Route::get('/ssgg/asset/{asset}','ssggController@ver_asset')
	->name('ssgg.ver_asset');

Route::get('/ssgg/asset/{asset}/nuevoservicio','ssggController@nuevo_servicio')
	->name('ssgg.nuevo_servicio');

Route::post('/ssgg/nuevoservicio','ssggController@crear_servicio')
	->name('ssgg.crear_servicio');

Route::get('/ssgg/editarAsset/{asset}','ssggController@editar_asset')
	->name('ssgg.editar_asset');

Route::put('/ssgg/editarAsset/{asset}','ssggController@edicion_asset')
	->name('ssgg.edicion_asset');

Route::get('/ssgg/eliminarAsset/{asset}','ssggController@eliminar_asset')
	->name('ssgg.eliminar_asset');

Route::get('/ssgg/eliminarServicio/{service}','ssggController@eliminar_servicio')
	->name('ssgg.eliminar_servicio');

/*Ips Bloqueadas*/

Route::get('/ssgg/ipBlock','ssggController@ver_ipblock')
	->name('ssgg.ipBlock');

Route::get('/ssgg/ipBlock/editar/{ip}','ssggController@editar_ipblock')
	->name('ssgg.editar_ipBlock');

Route::post('/ssgg/IpBlock/nueva','ssggController@crear_ipBlock')
	->name('ssgg.nueva_ipBlock');

Route::get('/ssgg/ipBlock/eliminar/{ip}','ssggController@eliminar_ipblock')
	->name('ssgg.eliminar_ipBlock');

Route::put('/ssgg/ipBlock/editar/{ip}','ssggController@edicion_ipBlock')
	->name('ssgg.editar_ipBlock');

Route::get('/ssgg/ipBlock/exportar','ssggController@exportar_ipBlock')
	->name('ssgg.exportar_ipBlock');

/* Engine */

Route::get('/ssgg/engine','ssggController@engine')
	->name('ssgg.engine');

Route::get('/ssgg/engine/subir/{engine}','ssggController@subir_engine')
	->name('ssgg.subir_engine');

Route::get('/ssgg/engine/bajar/{engine}','ssggController@bajar_engine')
	->name('ssgg.bajar_engine');

Route::get('/ssgg/engine/editar/{engine}','ssggController@editar_engine')
	->name('ssgg.editar_engine');

Route::put('/ssgg/engine/editar/{engine}','ssggController@edicion_engine')
	->name('ssgg.edicion_engine');

Route::get('/ssgg/engine/eliminar/{engine}','ssggController@eliminar_engine')
	->name('ssgg.eliminar_engine');

Route::get('/ssgg/engine/nuevo','ssggController@nuevo_engine')
	->name('ssgg.nuevo_engine');

Route::post('/ssgg/engine/crear','ssggController@crear_engine')
	->name('ssgg.crear_engine');

/*****  *****/

/***** VULNERABILIDADES *****/
Route::get('/vul','vulController@main')
	->name('vul.main');

Route::get('/vul/sites','vulController@sites')
	->name('vul.sites');

Route::get('/vul/sites/{site}','vulController@ver_site')
	->name('vul.ver_site');

Route::get('/vul/assets','vulController@assets')
	->name('vul.assets');

Route::get('/vul/scans','vulController@scans')
	->name('vul.scans');

Route::get('/vul/ocurrencias','vulController@ocurrencias')
	->name('vul.ocurrencias');

Route::get('/vul/ocurrencias/editar/{ocurrencias}','vulController@editar_varias_ocurrencias')
	->name('vul.editar_varias_ocurrencias');

Route::get('/vul/assets/eliminar/{asset}','vulController@eliminar_asset')
	->name('vul.eliminar_asset');

Route::get('/vul/scans/eliminar/{scan}','vulController@eliminar_scan')
	->name('vul.eliminar_scan');

Route::get('/vul/ocurrencias/eliminar/{ocurrencias}','vulController@eliminar_varias_ocurrencias')
	->name('vul.eliminar_varias_ocurrencias');

Route::put('/vul/ocurrencias/cambiar_estado/{ocurrencia}s', 'vulController@cambiar_estado_ocurrencia')
	->name('vul.cambiar_estado');

Route::get('/vul/ocurrencia/{ocurrencia}','vulController@ver_ocurrencia')
	->name('vul.ver_ocurrencia');

Route::get('/vul/ocurrencias/export','vulController@exportar_ocurrencias')
	->name('vul.exportar_ocurrencias');
/*****  *****/


/***** CIBER FRAUDE *****/
Route::get('/cf','cfController@main')
	->name('cf.main');

Route::get('/cf/typosquatting','cfController@typosquatting')
	->name('cf.typosquatting');

Route::get('/cf/typosquatting/nuevo','cfController@nuevo_dominio')
	->name('cf.nuevo_dominio');

Route::get('/cf/typosquatting/{domain}','cfController@typosquatting')
	->name('cf.typosquatting_d');

Route::get('/cf/typosquatting/activar/{typo}','cfController@activar_typosquatting')
	->name('cf.activar_typosquatting');

Route::get('/cf/typosquatting/descartar/{typo}','cfController@descartar_typosquatting')
	->name('cf.descartar_typosquatting');

Route::get('/cf/typosquatting/reportar/{typo}','cfController@reportar_typosquatting')
	->name('cf.reportar_typosquatting');

Route::get('/cf/typosquatting/exportar/{domain}','cfController@exportar_typosquatting')
	->name('cf.exportar_typosquatting');

Route::post('/cf/typosquatting/crear','cfController@crear_dominio')
	->name('cf.crear_dominio');


/*****  *****/

/***** VIGILANCIA DIGITAL *****/
Route::get('/vd','vdController@main')
	->name('vd.main');

Route::get('/vd/noticias','vdController@noticias')
	->name('vd.noticias');

Route::post('/vd/palabra/crear','vdController@crear_palabra')
	->name('vd.crear_palabra');

Route::get('/vd/palabra/eliminar/{palabra}','vdController@eliminar_palabra')
	->name('vd.eliminar_palabra');

Route::get('/vd/noticias/noticia/descartar/{noticia}','vdController@descartar_noticia')
	->name('vd.descartar_noticia');

Route::get('/vd/noticias/noticia/reportar/{noticia}','vdController@reportar_noticia')
	->name('vd.reportar_noticia');

Route::get('/vd/noticias/noticia/activar/{noticia}','vdController@activar_noticia')
	->name('vd.activar_noticia');

Route::get('/vd/notificaciones','vdController@notificaciones')
	->name('vd.notificaciones');

Route::get('/vd/notificaciones/nueva','vdController@nueva_notificacion')
	->name('vd.nueva_notificacion');

Route::post('/vd/notificaciones/crear','vdController@crear_notificacion')
	->name('vd.crear_notificacion');

Route::get('/vd/notificaciones/exportar','vdController@exportar_notificaciones')
	->name('vd.exportar_notificaciones');

Route::post('/vd/noticias/crear_ticket_notificacion/{noticia}','vdController@crear_ticket_notificacion')
	->name('vd.crear_ticket_notificacion');
/*****  *****/

/***** ANTICIPACION DE NUEVAS VULNERABILIDADES DESCUBIERTAS *****/
Route::get('/anvd','anvdController@main')
	->name('anvd.main');

Route::get('/anvd/productos','anvdController@productos')
	->name('anvd.productos');

Route::get('/anvd/productos/eliminar/{product}','anvdController@eliminar_producto')
	->name('anvd.eliminar_producto');

Route::get('/anvd/productos/nuevo','anvdController@nuevo_producto')
	->name('anvd.nuevo_producto');

Route::post('/anvd/productos/nuevo','anvdController@crear_producto')
	->name('anvd.crear_producto');

Route::get('/anvd/productos/editar/{product}','anvdController@editar_producto')
	->name('anvd.editar_producto');

Route::put('/anvd/productos/editar/{product}','anvdController@edicion_producto')
	->name('anvd.edicion_producto');

Route::get('/anvd/ocurrencias/{estado?}','anvdController@ocurrencias')
	->name('anvd.ocurrencias');

Route::get('/anvd/ocurrencias/descartar/{ocurrencias}','anvdController@descartar_ocurrencias')
	->name('anvd.descartar_ocurrencias');

Route::get('/anvd/ocurrencias/resolver/{ocurrencias}','anvdController@resolver_ocurrencias')
	->name('anvd.resolver_ocurrencias');

Route::get('/anvd/ocurrencias/activar/{ocurrencias}','anvdController@activar_ocurrencias')
	->name('anvd.activar_ocurrencias');

Route::get('/anvd/ocurrencias/ver/{ocurrencia}','anvdController@ver_ocurrencia')
	->name('anvd.ver_ocurrencia');

Route::get('/anvd/cargar_productos/{text}/{text2}','anvdController@cargar_productos')
	->name('anvd.cargar_productos');

Route::get('/anvd/cargar_vendedor/{text}','anvdController@cargar_vendedor')
	->name('anvd.cargar_vendedor');
/*****  *****/

/***** PRIMERA LINEA *****/
Route::get('/pl','plController@main')
	->name('pl.main');

Route::get('/pl/tickets','plController@tickets')
	->name('pl.tickets');

Route::get('/pl/monitorizacion','plController@monitorizacion')
	->name('pl.monitorizacion');
/*****  *****/

/***** LOGS *****/
Route::get('/logs/portal','logsController@portal')
	->name('logs.portal');

Route::get('/logs/monitorizacion','logsController@monitorizacion')
	->name('logs.monitorizacion');

Route::get('/logs/typosquatting','logsController@typosquatting')
	->name('logs.typosquatting');

Route::get('/logs/anvd','logsController@anvd')
	->name('logs.anvd');

Route::get('/logs/noticias','logsController@noticias')
	->name('logs.noticias');
/*****  *****/