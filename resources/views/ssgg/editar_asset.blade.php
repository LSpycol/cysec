@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Edición del asset : {{ $asset->name }}</h3></div>
</div>
<br>
<div class="w3-responsive w3-card-4 w3-white" >
  <div class="w3-col s12 m12 l4 w3-center">
      <img class="w3-margin-top" src="{{ asset('img/asset.png') }}" width="200px">
  </div>
  <div class="w3-col s12 m12 l8">
    <form method="POST" action="{{ route('ssgg.edicion_asset', $asset->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Nombre: </strong><input class="w3-input" type="text" value="{{$asset->name}}" name="name" required>
    </div>
    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Ip: </strong> <input class="w3-input" type="text" value="{{$asset->ip}}" name="ip" required>
    </div>
    <div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
      <strong>Información: </strong><input class="w3-input" type="text" value="{{$asset->info}}" name="info">
    </div>  
    <div class="w3-col s12 m12 l12 w3-margin-bottom w3-container w3-center">
      <input type="submit" class="w3-btn w3-theme">
    </div> 
  </div>
</div>
<br>



@endsection