@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')

<br>

<div id="infoDiv" class="w3-panel w3-light-blue w3-display-container">
  <span onclick="this.parentElement.style.display='none'"
  class="w3-button w3-large w3-display-topright">&times;</span>
  <p>Esta página se recarga automáticamente.</p>
</div>


<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Monitorización - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
    @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggCreate' . Auth::user()->client))
  	 <a href="{{ route('ssgg.nuevo_asset') }}"><button class="w3-btn w3-theme-d2">Nuevo Asset</button></a>
    @endif
  </div>
</div>
<br>
<div class="w3-responsive w3-card-4 w3-white" id="monitorizationDiv" >
</div>
<br>

<script type="text/javascript">

//Se carga la monitorización
function load(){
  $.ajax({
      url: '{{ route('ssgg.monitorizacion') }}',
      type: "GET",
      success: function(data){
        $('#monitorizationDiv').html(data);    
      }
  });	
}

load();
setInterval(function(){ load()}, 30000)
</script>

@endsection