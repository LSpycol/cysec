@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Engine - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
 	 <a href="{{ route('ssgg.nuevo_engine') }}"><button class="w3-btn w3-theme-d2">Nuevo Engine</button></a>
  </div>
</div>

<div class="w3-responsive w3-card-4 w3-white w3-margin-top w3-container" >
	@forelse($engines as $e)
		<div class="w3-responsive w3-card-2 w3-white w3-margin-bottom w3-margin-top">
			<div class="w3-col s12 m12 l12 w3-light-grey">
				<div class="w3-left">
					<p><strong>Expresión:</strong> {{ $e->regexpr }}</p>
				</div>
				<div class="w3-right">
					<a href="{{ route('ssgg.subir_engine', $e->id) }}"><img width="20" ="" src="{{ asset('img/arriba.png') }}"></a>
					<a href="{{ route('ssgg.bajar_engine', $e->id) }}"><img width="20" ="" src="{{ asset('img/abajo.png') }}"></a>
					<a href="{{ route('ssgg.editar_engine', $e->id) }}"><img width="20" ="" src="{{ asset('img/edit.png') }}"></a>
					<a style="cursor: pointer;" onclick="modalDelete({{ $e->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
				</div>
			</div>
			<div class="w3-col s6 m6 l3">
				<p><strong>Grupo Asignado:</strong> {{ $e->name }}</p>
			</div>
			<div class="w3-col s6 m6 l3">
				<p><strong>Tipo de cierre:</strong> {{ $e->type_close }}</p>
			</div>
			<div class="w3-col s6 m6 l3">
				<p><strong>Posición:</strong> {{ $e->position }}</p>
			</div>
			<div class="w3-col s6 m6 l3">
				<p><strong>Creación:</strong> {{ $e->created_at }}</p>
			</div>
		</div>
	@empty
		No hay Engines disponibles.
	@endforelse
</div>
<br>

<!-- modal -->
<div id="modalDelete" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalDelete').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Eliminar Engine</h3>
	  </header>
	    <br> ¿Desea realmente eliminar el engine?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
	  		<br><br>
	  </div>
	</div>
</div>


<script>
function modalDelete(id){
	document.getElementById('modalDelete').style.display='block';
	var url = '{{ route("ssgg.eliminar_engine", ":id") }}';
	url = url.replace(':id', id);
	document.getElementById('idDelete').href= url;
}

</script>


@endsection