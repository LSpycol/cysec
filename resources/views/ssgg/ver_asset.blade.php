@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Asset : {{ $asset->name }}</h3></div>
  <div class="w3-display-right">
    @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggEdit' . Auth::user()->client))
      <a onclick="document.getElementById('modalEliminar').style.display='block'"><button class="w3-btn w3-red">Eliminar Asset</button></a>
      <a href="{{ route('ssgg.editar_asset', $asset->id) }}"><button class="w3-btn w3-theme-d2">Editar Asset</button></a>
     @endif
     @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggCreate' . Auth::user()->client))
      <a href="{{ route('ssgg.nuevo_servicio', $asset->id) }}"><button class="w3-btn w3-theme-d2">Nuevo Servicio</button></a>
    @endif
  </div>
</div>
<br>
<div class="w3-responsive w3-card-4 w3-white" >
  <div class="w3-col s12 m12 l4 w3-center @if($asset->status == 'Up') w3-green @else w3-red @endif">
      <img class="w3-margin-top" src="{{ asset('img/asset.png') }}" width="200px">
  </div>
  <div class="w3-col s12 m12 l8">
    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Nombre: </strong>{{$asset->name}}
    </div>
    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Ip: </strong>{{$asset->ip}}
    </div>
    <div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
      <strong>Info: </strong>{{$asset->info}}
    </div>
    <div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
      <strong> Estado:<font color="@if($asset->status == 'Up') green @else red @endif"> {{$asset->status}}</font></strong>
    </div>
    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Último Check: </strong>{{$asset->lastCheck}}
    </div>
    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Última inactividad: </strong>{{$asset->lastDown}}
    </div>
    <div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
      <strong>Creación: </strong>{{$asset->created_at}}
    </div>    
  </div>

  <div class="w3-col s12 m12 l12">
    <div class="w3-col s12 m12 l12 w3-grey w3-center">
        <h4>Servicios</h4>
    </div>
    <div class="w3-col s12 m12 l12">
      <table class="w3-table w3-small">
        <thead>
            <tr>
              <th class="w3-center">Nombre</th>
              <th class="w3-center">Servicio</th>
              <th class="w3-center">Estado</th>
              <th class="w3-center">Información</th>
              <th class="w3-center">Último Check</th>
              <th class="w3-center">Última Inactividad</th>
               @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggEdit' . Auth::user()->client))
              <th></th>
              @endif
            </tr>
        </thead>
        <tbody>
          @forelse($services as $s)
            <tr>
              <td class="w3-center">{{$s->name}}</td>
              <td class="w3-center">{{$s->service}}</td>
              <td class="w3-center @if($s->status == 'Up') w3-green @else w3-red @endif">{{$s->status}}</td>
              <td class="w3-center">{{$s->info}}</td>
              <td class="w3-center">{{$s->lastCheck}}</td>
              <td class="w3-center">{{$s->lastDown}}</td>
               @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggEdit' . Auth::user()->client))
              <td>
                <a style="cursor: pointer;" onclick="modalDelete({{ $s->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
              </td>
              @endif
            </tr>
          @empty
          No hay servicios para este Asset
          @endforelse
        </tbody>
      </table>
    </div>
  </div>

</div>
<br>



<!-- modal -->
<div id="modalEliminar" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalEliminar').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Eliminar Asset</h3>
  </header>
  <div class="w3-container">
    <p>Para poder borrar un asset, no debe tener ningún servicio en monitorización.</p>
    <p>¿Desea realmente borrar el asset?</p>
  </div>
  <footer class="w3-container w3-light-grey w3-center">
    <a href="{{ route('ssgg.eliminar_asset', $asset->id) }}"><button class="w3-button w3-red">Eliminar</button></a>
  </footer>
</div>
</div>


<!-- modal -->
  <div id="modalDeleteService" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-opacity">
      <header class="w3-container w3-theme-d1"> 
        <span onclick="document.getElementById('modalDelete').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h3>Eliminar Servicio</h3>
      </header>
        <br> ¿Desea realmente eliminar el servicio?
        <br><br>
        <div class="w3-container w3-center">
          <a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
          <br><br>
      </div>
    </div>
  </div>


<script>

function modalDelete(id){
  document.getElementById('modalDeleteService').style.display='block';
  var url = '{{ route("ssgg.eliminar_servicio", ":id") }}';
  url = url.replace(':id', id);
  document.getElementById('idDelete').href= url;
}

</script>



@endsection