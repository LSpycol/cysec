@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-col s12 m12 l6 w3-display-left"><h3>Ips Bloqueada - {{ $ip->ip }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('ssgg.editar_ipBlock', $ip) }}">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<div class="w3-col s12 m12 l4 w3-padding-small w3-margin-bottom">
		<label>Ip</label>
		<input class="w3-input" type="text" name="ip" pattern="\d+.\d+.\d+.\d+." value="{{ $ip->ip }}" required>
	</div>
	<div class="w3-col s6 m6 l4 w3-padding-small w3-margin-bottom">
		<label>Fecha Bloqueo</label>
		<input class="w3-input" type="datetime-local" value="{{ $ip->blocked_at }}" name="blocked_at">
	</div>
	<div class="w3-col s6 m6 l4 w3-padding-small w3-margin-bottom">
		<label>Revisión</label>
		<input class="w3-input" type="datetime-local" value="{{ $ip->revision }}" name="revision">
	</div>
	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<label>Ataque</label>
		<input class="w3-input" type="text" value="{{ $ip->reason }}" name="reason">
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
	  <input type="submit" class="w3-btn w3-theme">
	</div>
	</form>


</div>


@endsection