@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-col s12 m12 l6 w3-display-left"><h3>Ips Bloqueadas - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-col s12 m12 l6">
  	<div class="w3-display-right">
	  	@if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggIpEdit' . Auth::user()->client))
	  		<button onclick="document.getElementById('modalNuevaIp').style.display='block'" class="w3-button w3-theme-d2">Nueva Ip</button>
	  	@endif
      <a href="{{ route('ssgg.exportar_ipBlock') }}">
        <button class="w3-button w3-theme-d2">Exportar</button>
      </a>
	</div>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">

	<table class="w3-table w3-striped w3-bordered w3-hoverable w3-small" id="IpTable">
		<thead>
			<tr>
				<th class="w3-center">Ip</th>
				<th class="w3-center">Bloqueada</th>
				<th class="w3-center">Ataque</th>
				<th class="w3-center">Revisión</th>
				<th class="w3-center">Creada</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="tickets_t">
			@forelse($ips as $ip)
			<tr>
				<td class="w3-center">{{ $ip->ip }}</td>
				<td>{{ $ip->blocked_at }}</td>
				<td>{{ $ip->reason }}</td>
				<td>{{ $ip->revision }}</td>
				<td>{{ $ip->created_at }}</td>
				<td>
					<a href="{{route("ssgg.editar_ipBlock", $ip->id)}}"><img width="20" ="" src="{{ asset('img/edit.png') }}"></a>
					<a style="cursor: pointer;" onclick="modalDelete({{ $ip->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
				</td>
			</tr>
			@empty
				<td>No se han encontrado Ips</td>
			@endforelse
		</tbody>
	</table>
</div>


<!-- modal -->
  <div id="modalDeleteIp" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-opacity">
      <header class="w3-container w3-theme-d1"> 
        <span onclick="document.getElementById('modalDeleteIp').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h3>Eliminar IP</h3>
      </header>
        <br> ¿Desea realmente eliminar la  IP?
        <br><br>
        <div class="w3-container w3-center">
          <a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
          <br><br>
      </div>
    </div>
  </div>



<!-- Modal -->

<div id="modalNuevaIp" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalNuevaIp').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Nueva Ip</h3>
  </header>
  <form method="POST" action="{{ route('ssgg.nueva_ipBlock') }}">
  <div class="w3-container">
  	
  		{{ csrf_field() }}
  		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Ip</label>
			<input class="w3-input" type="text" name="ip" pattern="\d+.\d+.\d+.\d+." required>
		</div>
		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Fecha Bloqueo</label>
			<input class="w3-input" type="datetime-local" name="blocked_at" required>
		</div>
		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
			<label>Ataque</label>
			<input class="w3-input" type="text" name="reason">
		</div>


  	
  </div>
  <footer class="w3-container w3-light-grey w3-center">
    <button class="w3-button w3-theme-d2">Crear</button>
  </footer>
  </form>
</div>
</div>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('#IpTable').DataTable( {
    	"pageLength": 50,
    	"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    } );
} );


function modalDelete(id){
  document.getElementById('modalDeleteIp').style.display='block';
  var url = '{{ route("ssgg.eliminar_ipBlock", ":id") }}';
  url = url.replace(':id', id);
  document.getElementById('idDelete').href= url;
}
</script>

@endsection