<div class="w3-light-grey w3-row">	
	<div class="w3-col s4 m4 l2 w3-margin-top">
		<strong>Assets <font color="red">Down</font>/<font color="green">Up</font>:</strong>
	</div>
	<div class="w3-col s8 m8 l10 w3-margin-bottom w3-margin-top w3-container">
		<div id="assetsDown" class="w3-container w3-red w3-center w3-col"></div>
		<div id="assetsUp" class="w3-container w3-green w3-center w3-col"></div>
	</div>

	<div class="w3-col s4 m4 l2">
		<strong>Servicios <font color="red">Down</font>/<font color="green">Up</font>:</strong>
	</div>
	<div class="w3-col s8 m8 l10  w3-margin-bottom w3-container">
		<div id="servicesDown" class="w3-container w3-red w3-center w3-col"></div>
		<div id="servicesUp" class="w3-container w3-green w3-center w3-col"></div>
	</div>

	<div class="w3-container w3-col s12 m12 l12 w3-margin-bottom" id="assetsDiv">

		@forelse(json_decode($assets) as $a)
			<div class="w3-col s6 m6 l4">
				<div class="w3-card-4 w3-margin w3-white">
					<header class="w3-center @if($a->status == 'Up') w3-green @else w3-red @endif">
						<h4><a href="{{ route('ssgg.ver_asset', $a->id) }}">{{ $a->name}}</a></h4>
					</header>
					<div class="w3-center">
						<img src="{{ asset('img/asset.png') }}" width="100">
					</div>
					<footer class="w3-center w3-light-grey w3-container">
						<?php
							$up = $down = 0;
							foreach (json_decode($services) as $s) {
								if($s->asset == $a->id){
									if($s->status == 'Up') $up++;
									else $down++;
								}
							}
						?>

						<strong>Servicios: {{ $up + $down }}</strong>
						<div class="w3-col s12 m12 l12 w3-margin-bottom">
							@if($down != 0)
								<div class="w3-container w3-red w3-center w3-col" style="width: {{ $down/($up+$down)*100 }}%">{{ $down }}
								</div>
							@endif
							@if($up != 0)
								<div class="w3-container w3-green w3-center w3-col" style="width: {{ $up/($up+$down)*100 }}%">{{ $up }}
								</div>
							@endif
							@if($up+$down == 0)
							<div class="w3-container w3-center w3-col" style="width: 1%">&nbsp;</div>
							@endif						
						</div>
					</footer>
				</div>
			</div>
		@empty
			No hay Assets
		@endforelse

	</div>
</div>




<script type="text/javascript">
	var assets = {!! $assets !!};
	var services = {!! $services !!};

	function getCount(data, status) {
		var i  = 0;
		var contador = 0;
		while(data[i] != null){
			if(data[i]['status'] == status){
				contador++;
			}
			i++;
		}
		return contador;
	}

	var assetsUp = getCount(assets, 'Up');
	var assetsDown = getCount(assets, 'Down');

	var servicesUp = getCount(services, 'Up');
	var servicesDown = getCount(services, 'Down');

	document.getElementById('assetsDown').style.width = (assetsDown/(assetsDown + assetsUp)) * 100 + '%';
	document.getElementById('assetsDown').innerHTML = assetsDown;
	document.getElementById('assetsUp').style.width = (assetsUp/(assetsDown + assetsUp)) * 100 + '%';
	document.getElementById('assetsUp').innerHTML = assetsUp;

	document.getElementById('servicesDown').style.width = (servicesDown/(servicesDown + servicesUp)) * 100 + '%';
	document.getElementById('servicesDown').innerHTML = servicesDown;
	document.getElementById('servicesUp').style.width = (servicesUp/(servicesDown + servicesUp)) * 100 + '%';
	document.getElementById('servicesUp').innerHTML = servicesUp;

</script>