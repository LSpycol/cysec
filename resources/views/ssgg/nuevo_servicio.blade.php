@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Nuevo Asset</h3></div>
</div>

<div class="w3-responsive w3-card-4 w3-white" >
  <div class="w3-col s12 m12 l4 w3-center">
      <img class="w3-margin-top" src="{{ asset('img/service.png') }}" width="200px">
  </div>
  <div class="w3-col s12 m12 l8 w3-center">
      <form method="POST" action="{{ route('ssgg.crear_servicio') }}">
        {{ csrf_field() }}

        <input class="w3-input" type="hidden" value="{{ $asset }}" name="asset" required>

        <div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
          <label>Nombre del servicio</label>
          <input class="w3-input" type="text" name="name" required>
        </div>

        <div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
          <label>Servicio</label>
          <input class="w3-input" type="text" placeholder="Nombre que aparece al ejecutar 'ps -aux'" name="service" required>
        </div>

        <div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
          <label>Información relevante:</label>
          <textarea class="w3-input w3-border" style="resize:none" name="info"></textarea>
        </div>

        <div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
        <input type="submit" class="w3-btn w3-theme">
      </div>
      </form>
  </div>
</div>
<br>


@endsection