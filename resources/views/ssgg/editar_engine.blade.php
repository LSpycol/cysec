@extends('general/layout')

@section('submenu')
	@include('ssgg/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Editar engine</h3></div>
</div>

<div class="w3-responsive w3-card-4 w3-white w3-margin-top" >
	<div class="w3-col s12 m12 l12 w3-theme-d5 w3-center">
		<h3>Engine - {{ $engine->id }}</h3>
	</div>
	<form method="POST" action="{{ route('ssgg.edicion_engine', $engine->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}


	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Expresión regular</strong></label>
        <input class="w3-input" type="text" name="regexpr" value="{{ $engine->regexpr }}" required>
	</div>

	<div class="w3-col s12 m12 l3 w3-margin-bottom w3-container">
		<label><strong>Grupo Auto asignado</strong></label>
        <select class="w3-select" name="autoassign_group">
		  <option value="" @if($engine->autoassign_group == null) selected @endif></option>
		  @foreach($groups as $g)
		  	<option value="{{ $g->id}}" @if($engine->autoassign_group == $g->id) selected @endif >{{ $g->name }}</option>
		  @endforeach
		</select>
	</div>

	<div class="w3-col s12 m12 l3 w3-margin-bottom w3-container">
		<label><strong>Tipo cerrado</strong></label>
        <select class="w3-select" name="type_close">
		  <option value="" @if($engine->type_close == null) selected @endif></option>
		  <option value="Autocierre" @if($engine->type_close) selected @endif>Autocierre</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container w3-left">
		<p>- El título de la regla/alerta, pasará por las expresiones regulares desde la posición 1 a la última posición. Se aplicará la configuración de la primera expresión regular en la que coincida.</p>
		<p>- Los tickets que se generen se asignarán al grupo del campo "Grupo Auto asignado".</p>
		<p>- Los tickets se cerrarán automáticamente cuando se creen si el campo "Tipo Cerrado" tiene esta opción.</p>
	</div>


	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
    	<input type="submit" class="w3-btn w3-theme">
    </div>

	</form>
</div>
<br>

@endsection