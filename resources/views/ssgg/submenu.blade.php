<div class="w3-bar">
  <a href="#" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-xlarge" onclick="w3_open()"> &#9776;</a>
  <a href="{{ route('ssgg.main') }}" class="w3-bar-item w3-button w3-hover-none w3-border-theme w3-bottombar "><strong>Servicios Gestionados</strong></a>

  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggIpView' . Auth::user()->client))
   <a href="{{ route('ssgg.ipBlock') }}" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-theme">Ips Bloqueadas</a>
   @endif
   
  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggEngine' . Auth::user()->client))
  	<a href="{{ route('ssgg.engine') }}" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-theme">Engine</a>
  @endif
</div>

<script>
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}
</script>