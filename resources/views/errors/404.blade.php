@extends('general/layout')

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Página no encontrada</h3></div>
</div>
<br>
<div class="w3-responsive w3-card-4 w3-center w3-white">
	<div class="w3-theme">
		<h2>Página no encontrada.</h2>
	</div>
	<div>
		<p>No se ha encontrado la página que buscabas.</p>
		<p>Disculpe las molestias.</p>
		<br>
		<p>Gracias.</p>
	</div>
</div>
<br>


@endsection