@extends('general/layout')

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Acceso registringido</h3></div>
</div>
<br>
<div class="w3-responsive w3-card-4 w3-center w3-white">
	<div class="w3-theme">
		<h2>Acceso registringido.</h2>
	</div>
	<div>
		<p>Parece que no tienes permiso para acceder a esta página para este cliente.</p>
		<p>Si crees que deberías tener acceso, contacta con un administrador.</p>
		<br>
		<p>Gracias.</p>
	</div>
</div>
<br>


@endsection