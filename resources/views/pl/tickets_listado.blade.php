@extends('general/layout')

@section('submenu')
	@include('pl/submenu')
@endsection

@section('contenido')
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-col s12 m12 l6 w3-display-left"><h3>Tickets sin asignar</h3></div>
  <div class="w3-col s12 m12 l6">
  	<div class="w3-display-right">
		<button class="w3-button w3-theme-d2 w3-hide" id="boton_editar">Editar</button>
		<button class="w3-button w3-theme-d2 w3-hide" id="boton_actualizar">Actualizar</button>
	  	<a href="{{ route('tickets.exportar_tickets', $_GET) }}">
			<button class="w3-button w3-theme-d2">Exportar</button>
		</a>
	</div>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">

	<table class="w3-table w3-striped w3-bordered w3-hoverable w3-small" id="ticketTable">
		<thead>
			<tr>
				<td>
					<input type="checkbox" style="transform: scale(1.5);" id="checkbox_ticket_all">
				</td>
				<th class="sortable">ID</th>
				<th class="sortable">Apertura</th>
				<th class="sortable">Cliente</th>
				<th class="sortable">Título</th>
				<th class="sortable">Tipo</th>
				<th class="sortable">Estado</th>
				<th class="sortable">Prioridad</th>
			</tr>
		</thead>
		<tbody id="tickets_t">
			<? //tickets se obtiene del controlador ?>
			@forelse ($tickets as $ticket)
			<tr>
				<td><input type="checkbox" class="checkbox_ticket" id="check_{{ $ticket->id }}" style="transform: scale(1.5);"></td>
				<td><a href="{{ route('tickets.individual', ['id' => $ticket]) }}">{{ $ticket->id }}</a></td>
				<td>{{ $ticket->creation }}</td>
				<th>{{ $ticket->name }}</th>
				<td><a href="{{ route('tickets.individual', ['id' => $ticket]) }}">{{ $ticket->title }}</a></td>
				<td>{{ $ticket->type }}</td>
				<td>{{ $ticket->status }}</td>
				<td>{{ $ticket->priority }}</td>
			</tr>
			@empty
				<td>No se han encontrado tickets</td>
			@endforelse

		</tbody>
	</table>
</div>



<!-- -->

<div id="modalFiltroAvanzado" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalFiltroAvanzado').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Filtro Avanzado</h3>
  </header>
  <div class="w3-container">

  	<form method="GET" action="{{ route('tickets.listado') }}">
  		{{ csrf_field() }}
  	<div class="w3-row-padding">
	  <div class="w3-col s12 l6 w3-padding-small">
	    <label>ID</label>
	    <input class="w3-input w3-border" name="id" type="text">
	  </div>

	  <div class="w3-col s6 l3 w3-padding-small">
	    <label>Apertura: Desde</label>
	    <input class="w3-input w3-border" type="date" name="open_from">
	  </div>

	  <div class="w3-col s6 l3 w3-padding-small">
	  	<label>Hasta</label>
	    <input class="w3-input w3-border" type="date" name="open_to">
	  </div>

	  <div class="w3-col s4 l2 w3-padding-small" name="title_operator">
	  	<label><br>Título</label>
	  	<select class="w3-select" name="title_op">
		  <option value="=" selected>=</option>
		  <option value="!=">!=</option>
		  <option value="contiene">Contiene</option>
		</select>
	  </div>

	  <div class="w3-col s8 l10 w3-padding-small">
	  	<label> <br>&nbsp; </label>
	    <input class="w3-input w3-border" name="title" type="text">
	    <br>
	  </div>

	  <div class="w3-col s6 l4 w3-padding-small">
	  	<label> Tipo </label>
	    <select class="w3-select" name="type">
		  <option value="" selected></option>
		  <option value="Vigilancia Digital">Vigilancia Digital</option>
		  <option value="Servicios Gestionados">Servicios Gestionados</option>
		  <option value="Ciber Inteligencia">Ciber Inteligencia</option>
		  <option value="Vulnerabilidades">Vulnerabilidades</option>
		</select>
	  </div>

	  <div class="w3-col s6 l4 w3-padding-small">
	  	<label> Estado </label>
	    <select class="w3-select" name="status">
		  <option value="" selected></option>
		  <option value="Abierto">Abierto</option>
		  <option value="En Proceso">En Proceso</option>
		  <option value="Cerrado">Cerrado</option>
		</select>
	  </div>

	  <div class="w3-col s6 l4 w3-padding-small">
	  	<label> Prioridad </label>
	    <select class="w3-select" name="priority">
		  <option value="" selected></option>
		  <option value="Bajo">Bajo</option>
		  <option value="Medio">Medio</option>
		  <option value="Alto">Alto</option>
		</select>
	  </div>

	  <div class="w3-col s6 l6 w3-padding-small">
	  	<label> Generado </label>
	    <select class="w3-select" name="generated">
		  <option value="" selected></option>
		  <option value="Automático">Automáticamente</option>
		  <option value="Manual">Manualmente</option>
		</select>
	  </div>


	  <div class="w3-col s6 l3 w3-padding-small">
	    <label>Cierre: Desde</label>
	    <input class="w3-input w3-border" type="date" name="closed_from">
	  </div>

	  <div class="w3-col s6 l3 w3-padding-small">
	  	<label>Hasta</label>
	    <input class="w3-input w3-border" type="date" name="closed_to">
	  </div>


	</div>

	<br>
  </div>
  <footer class="w3-container w3-light-grey w3-center">
    <button class="w3-button w3-theme-d2">Filtrar</button>
  </footer>
  </form>
</div>
</div>

<script src="{{ asset('js/tickets_listado.js') }}" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('#ticketTable').DataTable( {
    	"pageLength": 25,
    	"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    } );
} );
	
$("#boton_actualizar").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("tickets.actualizar", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});

$("#boton_editar").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("tickets.editar_varios", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});

</script>

@endsection