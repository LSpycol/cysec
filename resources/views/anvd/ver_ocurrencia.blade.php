@extends('general/layout')

@section('submenu')
	@include('anvd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Ocurrencia: {{ $ocurrencia->id }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-col s12 m12 l12 w3-border-theme w3-bottombar w3-center"><h3>{{ $ocurrencia->cve}}</h3></div>

	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
		<div class="w3-light-grey"><strong><h3>Datos</h3></strong></div>
		<div class="w3-col s12 m12 l4">
			<p><strong>Vendedor:</strong> {{ $ocurrencia->vendor }}</p>
			<p><strong>Producto:</strong> {{ $ocurrencia->product }}</p>
		</div>
		<div class="w3-col s12 m12 l4">
			<p><strong>Estado:</strong> {{ $ocurrencia->status }}</p>
			<p><strong>Detectada:</strong> {{ $ocurrencia->created_at }}</p>
		</div>
		<div class="w3-col s12 m12 l4 w3-center" id="severidad">
			<strong><h2>Severidad:</h2></strong>
			<h1>{{ $ocurrencia->severity }}</h1>
		</div>
	</div>
	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
		<div class="w3-light-grey"><strong><h3>Vulnerabilidad</h3></strong></div>
		@if($ocurrencia->cve != "")
			<div class="w3-col s12 m12 l4">
				<p><strong>CVE:</strong> {{ $ocurrencia->cve }}</p>
			</div>
			<div class="w3-col s12 m12 l4">
				<p><strong>Referencia: <a target="_blank" href="https://nvd.nist.gov/vuln/detail/{{ $ocurrencia->cve }}">Nist </a></strong></p>
			</div>
		@endif
		@if($ocurrencia->published != "")
			<div class="w3-col s12 m12 l4">
				<p><strong>Publicada:</strong> {{ $ocurrencia->published }}</p>
			</div>
		@endif
		<div class="w3-col s12 m12 l12">
			<p>{{ $ocurrencia->description }}</p>
		</div>
	</div>
</div>


<script type="text/javascript">
	
	var severidad = {{ $ocurrencia->severity}};

	if (severidad < 4) {
		document.getElementById("severidad").classList.add("w3-green");
	} else{
		if (severidad < 7) {
			document.getElementById("severidad").classList.add("w3-yellow");
		}else{
			document.getElementById("severidad").classList.add("w3-red");
		}
	}

</script>


@endsection