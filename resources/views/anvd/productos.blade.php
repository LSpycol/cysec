@extends('general/layout')

@section('submenu')
	@include('anvd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Productos - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
	  <a href="{{ route('anvd.nuevo_producto') }}">
		<button class="w3-button w3-green">Nuevo Producto</button>
	  </a>
   </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Vendedor</th>
					<th class="w3-center">Producto</th>
					<th class="w3-center">Email</th>
					<th></th>

				</tr>
			</thead>
			<tbody>
				@forelse($products as $p)
				<tr>
					<td class="w3-center">{{ $p->vendor }}</td>
					<td class="w3-center">{{ $p->product }}</td>
					<td class="w3-center">{{ $p->email }}</td>
					<td>
						<a href="{{ route('anvd.editar_producto', $p->id) }}"><img width="20" ="" src="{{ asset('img/edit.png') }}"></a>
						<a style="cursor: pointer;" onclick="modalDelete({{ $p->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
					</td>
				</tr>
				@empty
				<td>No se han encontrado Productos</td>
				@endforelse
				
			</tbody>
		</table>
</div>

<!-- modal -->
<div id="modalDelete" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalDelete').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Eliminar Producto</h3>
	  </header>
	    <br> ¿Desea realmente eliminar el producto?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
	  		<br><br>
	  </div>
	</div>
</div>


<script>
function modalDelete(id){
	document.getElementById('modalDelete').style.display='block';
	var url = '{{ route("anvd.eliminar_producto", ":id") }}';
	url = url.replace(':id', id);
	document.getElementById('idDelete').href= url;
}
</script>

@endsection