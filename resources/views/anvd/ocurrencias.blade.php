@extends('general/layout')

@section('submenu')
	@include('anvd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Ocurrencias - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
  	<button class="w3-button w3-blue w3-hide" id="boton_editar">Activar</button>
  	<button class="w3-button w3-green w3-hide" id="boton_actualizar" >Resolver</button>
  	<button class="w3-button w3-grey w3-hide" id="boton_3">Descartar</button>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<td>
						<input type="checkbox" style="transform: scale(1.5);" id="checkbox_ticket_all">
					</td>
					<th class="w3-center">CVE</th>
					<th class="w3-center">Producto</th>
					<th class="w3-center">Publicación</th>
					<th class="w3-center">Estado</th>
					<th class="w3-center">Severidad</th>
				</tr>
			</thead>
			<tbody>
				@forelse($ocurrencias as $o)
				<tr>
					<td><input type="checkbox" class="checkbox_ticket" id="check_{{ $o->id }}" style="transform: scale(1.5);"></td>
					<td class="w3-center"><a href="{{ route('anvd.ver_ocurrencia', $o->id) }}">{{ $o->cve }}</a></td>
					<td class="w3-center">{{ $o->product }}</td>
					<td class="w3-center">{{ $o->published }}</td>
					<td class="w3-center">{{ $o->status }}</td>
					<td class="w3-center">{{ $o->severity }}</td>
				</tr>
				@empty
				<td>No se han encontrado Ocurrencias</td>
				@endforelse
				
			</tbody>
		</table>
</div>

<!-- modal -->
<div id="modalDelete" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalDelete').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Eliminar Producto</h3>
	  </header>
	    <br> ¿Desea realmente eliminar el producto?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
	  		<br><br>
	  </div>
	</div>
</div>


<script src="{{ asset('js/tickets_listado.js') }}" type="text/javascript"></script>
<script type="text/javascript">

$("#boton_editar").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("anvd.activar_ocurrencias", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});

$("#boton_actualizar").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("anvd.resolver_ocurrencias", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});

$("#boton_3").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("anvd.descartar_ocurrencias", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});

</script>

@endsection