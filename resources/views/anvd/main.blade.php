@extends('general/layout')

@section('submenu')
	@include('anvd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Anticipación a Nuevas Vulnerabilidades Descubiertas - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom w3-light-grey">

	<div class="w3-col s12 m12 l12 w3-center w3-animate-opacity">
		<div class="w3-col s4 m4 l4 w3-padding-small w3-center w3-blue">
			<h3><a href="{{ route('anvd.ocurrencias', 'Activa') }}">Activas: {{ $ocurrencias->count() }}</a></h3>
		</div>
		<div class="w3-col s4 m4 l4 w3-padding-small w3-center w3-green">
			<h3><a href="{{ route('anvd.ocurrencias', 'Resuelta') }}">Resueltas: {{ $resueltas }}</a></h3>
		</div>
		<div class="w3-col s4 m4 l4 w3-padding-small w3-center w3-grey">
			<h3><a href="{{ route('anvd.ocurrencias', 'Descartada') }}">Descartadas: {{ $descartadas }}</a></h3>
		</div>
	</div>

</div>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<div class="w3-col s6 m6 l4 w3-padding-small w3-center w3-theme-l4" style="height: 500px; overflow-y: scroll;">
		@forelse($ocurrencias as $o)
			<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-top w3-center w3-button w3-white w3-border w3-border-blue divCVE" id="divCVE{{$o->id}}"">
				<header><h3>{{$o->cve}}</h3></header>
			</div>
		@empty
			No se han encontrado nuevas vulnerabilidades
		@endforelse

	</div>
	@if(isset($ocurrencias[0]))
	<div class="w3-col s6 m6 l8 w3-padding-small w3-center w3-animate-opacity" id="cveDatos">
		<div class="w3-col s12 m12 l12 w3-theme-d5"><h3 id="titulocve">{{$ocurrencias[0]->cve}}</h3></div>
		<div class="w3-col s12 m12 l12"> 
			<table class="w3-table w3-border">
				<tbody>
					<tr>
						<td class="w3-center"><strong>Producto Afectado:</strong> <a id="productocve">{{ $ocurrencias[0]->product }}</a></td>
						<td class="w3-center"><strong>Publicación:</strong> <a id="publicacioncve">{{ $ocurrencias[0]->published }}</a></td>
					</tr>
					<tr>
						<td colspan="2" class="w3-center w3-border"> <strong>Severidad: <a id="severidadcve">{{ $ocurrencias[0]->severity }}</a></strong></td>
					</tr>
				</tbody>
			</table>

			<p align="justify" id="descripcioncve">{{ $ocurrencias[0]->description }}</p>
			<br>
			<footer>
				<div class="w3-left"><a id="resolverOcurrencia"><button class="w3-button w3-green" id="boton_actualizar" >Resolver</button></a></div>
				<div class="w3-right"><a id="descartarOcurrencia"><button class="w3-button w3-grey" id="boton_3">Descartar</button></a></div>
			</footer>
		</div>
	</div>
	@else
		No hay datos.
	@endif
</div>

<script type="text/javascript">
	var ocurrencias = {!! $ocurrencias !!};
</script>
<script type="text/javascript" src="{{asset('js/anvd_main.js')}}"></script>

<script type="text/javascript">

	$( ".divCVE" ).first().removeClass( "w3-white" ).addClass( "w3-green" );

	function seleccionar(id) {
		$( "#divCVE" + id ).addClass( "w3-green" );
	}

	$(document).ready(function() {
        $('.divCVE').click(function() {
        	$( '.divCVE' ).addClass( "w3-white" );
        	$( this ).removeClass( "w3-white" );
            $( this ).addClass( "w3-green" );

            var id = $( this ).attr('id').replace("divCVE", "");
            var i = 0;

            while(ocurrencias[i]['id'] != id) i++;

            $( '#cveDatos' ).hide();
            $( '#titulocve' ).html(ocurrencias[i]['cve']);
            $( '#productocve' ).html(ocurrencias[i]['product']);
            $( '#publicacioncve' ).html(ocurrencias[i]['published']);
            $( '#severidadcve' ).html(ocurrencias[i]['severity']);
            $( '#descripcioncve' ).html(ocurrencias[i]['description']);

            var url = '{{ route("anvd.resolver_ocurrencias", "id") }}';
			url = url.replace('id', id);
            $('#resolverOcurrencia').attr("href", url);
            url = '{{ route("anvd.descartar_ocurrencias", "id") }}';
			url = url.replace('id', id);
            $('#descartarOcurrencia').attr("href", url);

            $( '#cveDatos' ).show();

        });
    });

</script>

@endsection