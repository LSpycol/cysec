@extends('general/layout')

@section('submenu')
	@include('anvd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Editar Producto - {{ $product->product }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
<form method="POST" action="{{ route('anvd.edicion_producto', $product->id) }}">
		{{ method_field('PUT') }}
        {{ csrf_field() }}
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Vendedor</strong></label>
	    <input class="w3-input" type="text" name="vendor" value="{{ $product->vendor }}" placeholder="apache" required>
	</div>
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Producto</strong></label>
	    <input class="w3-input" type="text" name="product" value="{{ $product->product }}" placeholder="apache_server" required>
	</div>
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Email</strong></label>
	    <input class="w3-input" type="email" value="{{ $product->email }}" name="email">
	</div>

	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
		<p> * Cuando aparezcan nuevas vulnerabilidades del producto elegido, se notificará al correo introducido y se podrán visualizar en el panel general.</p>
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
    	<input type="submit" class="w3-btn w3-theme">
    </div>
	</form>
</div>




@endsection