@extends('general/layout')

@section('submenu')
	@include('anvd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Nuevo Producto - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
<form method="POST" action="{{ route('anvd.crear_producto') }}">
        {{ csrf_field() }}
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Vendedor</strong></label>
		<input id="vendor" name="vendor" list="Datavendor" class="w3-input" type="text" required>
		<datalist id="Datavendor">
		</datalist>
	</div>
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Producto</strong></label>
		<input id="product" name="product" list="Dataproduct" class="w3-input" type="text" required>
	    <datalist id="Dataproduct">
		</datalist>
	</div>
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Email</strong></label>
	    <input class="w3-input" type="email" name="email">
	</div>

	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
		<p> - Escribe el nombre del vendedor y selecciona uno de la lista.</p>
		<p> - Escribe el nombre del producto asociado al vendedor y selecciona uno de la lista</p>
		<p> - Si no aparecen datos en la lista, el producto o vendedor no se encuentra en la base de datos.</p>
		<br>
		<p> * Cuando aparezcan nuevas vulnerabilidades del producto elegido, se notificará al correo introducido y se podrán visualizar en el panel general.</p>
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
    	<input type="submit" class="w3-btn w3-theme">
    </div>
	</form>
</div>

<script type="text/javascript">

	$('#vendor').on("keyup", function() {
		if($('#vendor').val().length > 2){
			var url = '{{ route("anvd.cargar_vendedor", ":txt") }}';
			url = url.replace(':txt', $('#vendor').val());
			
			$.ajax({
			      url: url,
			      type: "GET",
			      success: function(data){
			        $('#Datavendor').html(data);    
			      }
			  });
			}
	});


	$('#product').on("keyup", function() {
		if($('#product').val().length > 2){
			var url = '{{ route("anvd.cargar_productos", ["text"=>":txt", "text2"=>":txt2"]) }}';
			url = url.replace(':txt', $('#vendor').val());
			url = url.replace(':txt2', $('#product').val());

			$.ajax({
			      url: url,
			      type: "GET",
			      success: function(data){
			        $('#Dataproduct').html(data);    
			      }
			  });
		}
	});
</script>


@endsection