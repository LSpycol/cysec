@extends('general/layout')

@section('submenu')
	@include('tickets/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>
	  @if($longitud > 1)
	  	Actualizando Tickets:
	  	@foreach($tickets as $ticket)
	  		{{$ticket}} -  
	  	@endforeach
	  @else
	  	Actualizando Ticket: {{ $tickets[0] }}
	  @endif
	</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('tickets.editar_varios_tickets', implode(',', $tickets)) }}">
		{{ csrf_field() }}

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Tipo</label>
		<select class="w3-select" name="type">
		  <option value="" selected></option>
		  @foreach($types as $t)
		  	<option value="{{ $t->type}}">{{ $t->type }}</option>
		  @endforeach
		</select>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Prioridad</label>
		<select class="w3-select" name="priority">
		  <option value="" selected></option>
		  <option value="Bajo">Bajo</option>
		  <option value="Medio">Medio</option>
		  <option value="Alto">Alto</option>
		  <option value="Crítico">Crítico</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l4 w3-padding-small w3-margin-bottom">
		<label>Estado</label>
		<select class="w3-select" name="status" id="form_status">
		  <option value="" selected></option>
		  <option value="Abierto">Abierto</option>
		  <option value="En Proceso">En Proceso</option>
		  <option value="Cerrado">Cerrado</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l4 w3-padding-small w3-margin-bottom w3-hide" id="div_group_assigned">
		<label>Grupo asignado</label>
		<select class="w3-select" name="assigned_to_group" id="form_assigned_to_group" disabled>
		  <option value="" selected></option>
		  @foreach($groups as $g)
		  	<option value="{{ $g->id}}">{{ $g->name }}</option>
		  @endforeach
		</select>
	</div>

	<div class="w3-col s12 m12 l4 w3-padding-small w3-margin-bottom w3-hide" id="div_solution">
		<label>Solución</label>
		<select class="w3-select" name="solution" required id="form_solution" disabled>
		  <option value="" selected></option>
		  <option value="Solucionado">Solucionado</option>
		  <option value="Falso Positivo">Falso Positivo</option>
		  <option value="No solucionado">No solucionado</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" class="w3-btn w3-theme">
	</div>
	</form>

</div>

<script type="text/javascript">
	$("#form_status").click(function(){
		var opcionstatus = $("#form_status").val();


		switch(opcionstatus){
			case 'En Proceso':
				$("#form_assigned_to_group").removeAttr('disabled');
				$("#form_solution").attr('disabled','disabled');
				$("#div_solution").addClass("w3-hide");
				$("#div_group_assigned").removeClass("w3-hide");
				break;
			case 'Cerrado':
				$("#form_assigned_to_group").attr('disabled','disabled');
				$("#form_solution").removeAttr('disabled');
				$("#div_solution").removeClass("w3-hide");
				$("#div_group_assigned").addClass("w3-hide");
				break;
			default:
				$("#form_assigned_to_group").attr('disabled','disabled');
				$("#form_solution").attr('disabled','disabled');
				$("#div_solution").addClass("w3-hide");
				$("#div_group_assigned").addClass("w3-hide");
				break;
		}
	});
</script>

<br>
<hr>

@endsection