@extends('general/layout')

@section('submenu')
	@include('tickets/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<strong><p id="openTickets"> </p></strong>
	<div class="w3-light-grey w3-row">
	  <a href="{{ route('tickets.listado', 'priority=Crítico&status=Abierto,En Proceso&')}}">
	  	<div id="barCritico" class="w3-container w3-red w3-center w3-col"></div>
	  </a>
	  <a href="{{ route('tickets.listado', 'priority=Alto&status=Abierto,En Proceso')}}">
	 	 <div id="barAlto" class="w3-container w3-deep-orange w3-center w3-col"></div>
	  </a>
	  <a href="{{ route('tickets.listado', 'priority=Medio&status=Abierto,En Proceso')}}">
	  	<div id="barMedio" class="w3-container w3-yellow w3-center w3-col"></div>
	  </a>
	  <a href="{{ route('tickets.listado', 'priority=Bajo&status=Abierto,En Proceso')}}">
	 	 <div id="barBajo" class="w3-container w3-green w3-center w3-col"></div>
	  </a>
	</div>
	<br>
	<div class="w3-col s12 m12 l6">
		<canvas id="plot1" height="150"></canvas>
	</div>
	<div class="w3-col s12 m12 l6">
		<canvas id="plot2" height="150"></canvas>
	</div>
</div>

<div class="w3-responsive w3-card-4 w3-white w3-padding-24">
	<u><h3 onclick="document.getElementById('modalFecha').style.display='block'" id="historico" style="cursor: pointer;"></h3></u>
	<div class="w3-col s12 m12 l12 w3-padding-24">
		<canvas id="plot3" height="100"></canvas>
	</div>
	<div class="w3-col s12 m12 l12 w3-padding-24">
		<canvas id="plot4" height="100"></canvas>
	</div>
</div>


<!-- modal -->
<div id="modalFecha" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
<form method="GET" action="{{ route('tickets.main') }}">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalFecha').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Origen del histórico</h3>
  </header>
  <div class="w3-container">
  	<div style="width: 50%; margin-left: 25%;">
	    <label>Desde: </label>
	    <input class="w3-input w3-border" type="date" name="from">
	  </div>

	<br>
  </div>
  <footer class="w3-container w3-light-grey w3-center">
    <button class="w3-button w3-white w3-border w3-border-theme w3-round-large">Filtrar</button>
  </footer>
  </form>
</div>
</div>




<script type="text/javascript">
	var tickets = {!! $tickets !!};
</script>
<script type="text/javascript" src="{{asset('js/chart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tickets_main.js')}}"></script>

@endsection