@extends('general/layout')

@section('submenu')
	@include('tickets/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Ticket: {{ $ticket->id }}</h3></div>
  <div class="w3-display-right">
  	@if(Auth::user()->role == 'Administrador' || Auth::user()->can('TicketEdit' . Auth::user()->client))
  		<a href="{{ route('tickets.editar', $ticket->id) }}"><button class="w3-button w3-white w3-border w3-border-theme w3-round-large">Editar</button></a>
  	@endif
  	@if(Auth::user()->role == 'Administrador' || Auth::user()->can('TicketUpdate' . Auth::user()->client))
  		<a href="{{ route('tickets.actualizar', $ticket->id) }}"><button class="w3-button w3-white w3-border w3-border-theme w3-round-large">Actualizar</button></a>
  	@endif
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<header class="w3-container w3-theme-d1">
	  <h2>{{ $ticket->title }}</h2>
	</header>

	<div class="w3-container">
		 <div class="w3-col m9 s9 l5">
		 	<table class="w3-table w3-mobile">
		 		<tbody>
		 			<tr>
		 				<th>Ticket ID</th>
		 				<td>{{ $ticket->id }}</td>
		 			</tr>
		 			<tr>
		 				<th>Cliente:</th>
		 				<td>{{ $ticket->client }}</td>
		 			</tr>
		 			<tr>
		 				<th>Tipo:</th>
		 				<td>{{ $ticket->type }}</td>
		 			</tr>
		 			<tr>
		 				<th>Grupo asignado:</th>
		 				<td>{{ $ticket->assigned_to_group }}</td>
		 			</tr>
		 			<tr>
		 				<th>Solución:</th>
		 				<td>{{ $ticket->solution }}</td>
		 			</tr>
		 		</tbody>
		 	</table>
		 </div>
		 <div class="w3-col m9 s9 l5">
		 	<table class="w3-table w3-mobile">
		 		<tbody>
		 			<tr>
		 				<th>&nbsp;</th>
		 				<td></td>
		 			</tr>
		 			<tr>
		 				<th>Estado:</th>
		 				<td>{{ $ticket->status }}</td>
		 			</tr>
		 			<tr>
		 				<th>Fecha creación:</th>
		 				<td>{{ $ticket->creation }}</td>
		 			</tr>
		 			<tr>
		 				<th>Fecha cierre:</th>
		 				<td>{{ $ticket->closed }}</td>
		 			</tr>
		 		</tbody>
		 	</table>
		 </div>
	 <div class="w3-col m3 s3 l2 {{$ticket->getPriority()}}">

 		<div class="w3-center w3-padding-16">
 			Prioridad <br>
 			{{ $ticket->priority }}
 		</div>

	 </div>
	</div>

	<hr>

	@if(isset($info[0]->information_1))
		<div class="w3-container">
			<div class="w3-col m12 s12 l12">
				<h3>Información</h3>
			</div>

			<div class="w3-col m12 s12 l12 w3-margin-bottom" id="info1"></div>
			<div class="w3-col m12 s12 l12 w3-margin-bottom" id="info2"></div>
		</div>
	@endif

	<hr>

	<div class="w3-container">
		<div class="w3-col m12 s12 l12">
			<h3>Histórico</h3>
		</div>

		@forelse ($historico as $accion)
			<div class="w3-col m12 s12 l12 w3-card-2 w3-margin-bottom">
				<div class="w3-container">
					<strong><p>{{ $accion->text }}</p></strong>
					
					<?php $antes = json_decode($accion->changes_before, true) ?>
					<?php $despues = json_decode($accion->changes_after, true) ?>

					@if(isset($accion->actualization))
						<p>{{ $accion->actualization }}</p>
					@endif

					@if(isset($antes))
						@foreach($antes as $key => $value)
							<p>Se ha cambiado: "{{ $value }}", por "{{ $despues[$key]}}" </p>
						@endforeach
					@endif
				</div>
				<footer class="w3-container w3-theme-l4">
				 	<p>Usuario: {{ $accion->user }}. A: {{ $accion->created_at }}.</p>
				</footer>
				 
			</div>
		@empty
			No se ha encontrado el histórico del ticket
		@endforelse

	</div>

</div>

<script type="text/javascript">
	var info1 = "";
	var info2 = "";
	@if(isset($info[0]->information_1))
		var info1 = "{{$info[0]->information_1}}";
	@endif
	@if(isset($info[0]->information_2))
		var info2 = "{{$info[0]->information_2}}";
	@endif

	if(info1 != ""){
		if(true){
			document.getElementById("info1").innerHTML += info1;
		}
		//aqui hay que comprobar si info1 es un json
		//https://stackoverflow.com/questions/9804777/how-to-test-if-a-string-is-json-or-not
	}

	if(info2 != ""){
		if(true){
			document.getElementById("info2").innerHTML += info1;
		}
		//aqui hay que comprobar si info1 es un json
	}
	


</script>

@endsection