@extends('general/layout')

@section('submenu')
	@include('tickets/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>
	  @if($longitud > 1)
	  	Actualizando Tickets:
	  	@foreach($tickets as $ticket)
	  		{{$ticket}} -  
	  	@endforeach
	  @else
	  	Actualizando Ticket: {{ $tickets[0] }}
	  @endif
	</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('tickets.actualizar_ticket', implode(',', $tickets)) }}">
		{{ csrf_field() }}
	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<label>Actualización</label>
		<textarea class="w3-input w3-border" style="resize:none" name="actualization" required></textarea>
	</div>

	<div class="w3-col s12 m12 l4 w3-padding-small w3-margin-bottom">
		<input class="w3-check" type="checkbox" id="checkbox_close">
		<label>Resolver ticket</label>
	</div>
	
	<div class="w3-col s12 m12 l4 w3-padding-small w3-margin-bottom w3-hide" id="div_solution">
		<label>Solución</label>
		<select class="w3-select" name="solution" required id="form_solution" disabled>
		  <option value="" selected></option>
		  <option value="Solucionado">Solucionado</option>
		  <option value="Falso Positivo">Falso Positivo</option>
		  <option value="No solucionado">No solucionado</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" class="w3-btn w3-theme">
	</div>
	</form>

</div>

<br>
<hr>

<script type="text/javascript">
	$("#checkbox_close").click(function(){
		if($("#checkbox_close").is(':checked')){
			$("#div_solution").removeClass('w3-hide');
			$("#form_solution").removeAttr('disabled');
		}else{
			$("#div_solution").addClass('w3-hide');
			$("#form_solution").attr('disabled','disabled');
		}
	});

</script>

@endsection