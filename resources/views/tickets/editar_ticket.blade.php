@extends('general/layout')

@section('submenu')
	@include('tickets/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Editar Ticket: {{ $ticket->id }}</h3></div>
  <div class="w3-display-right">
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('tickets.editar_ticket', $ticket->id) }}">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
	<header class="w3-container w3-theme-d1">
	  <h2><input class="w3-input" type="text" value="{{ $ticket->title }}" name="title" required></h2>
	</header>

	<div class="w3-container">
		 <div class="w3-col m9 s9 l5">
		 	<table class="w3-table w3-mobile">
		 		<tbody>
		 			<tr>
		 				<th>Ticket ID</th>
		 				<td>{{ $ticket->id }}</td>
		 			</tr>
		 			<tr>
		 				<th>Cliente:</th>
		 				<td>{{ $ticket->client }}</td>
		 			</tr>
		 			<tr>
		 				<th>Tipo:</th>
		 				<td>
		 					<select class="w3-select" name="type" required>
		 						@foreach($types as $t)
							  		<option value="{{ $t->type}}" @if($ticket->type == $t->type) selected @endif>
							  			{{ $t->type }}
							  		</option>
							 	@endforeach
							</select>
		 				</td>
		 			</tr>
		 			<tr>
		 				<th>Grupo asignado:</th>
		 				<td>
		 					<select class="w3-select" name="assigned_to_group" id="form_assigned_to_group" disabled>
							  <option value="" selected></option>
							  @foreach($groups as $g)
							  	<option value="{{ $g->id}}">{{ $g->name }}</option>
							  @endforeach
							</select>
		 				</td>
		 			</tr>
		 			<tr>
		 				<th>Solución:</th>
		 				<td>
		 					<select class="w3-select" name="solution" required id="form_solution" disabled>
							  <option value="" selected></option>
							  <option value="Solucionado">Solucionado</option>
							  <option value="Falso Positivo">Falso Positivo</option>
							  <option value="No solucionado">No solucionado</option>
							</select>
		 				</td>
		 			</tr>
		 		</tbody>
		 	</table>
		 </div>
		 <div class="w3-col m9 s9 l5">
		 	<table class="w3-table w3-mobile">
		 		<tbody>
		 			<tr>
		 				<th>&nbsp;</th>
		 				<td></td>
		 			</tr>
		 			<tr>
		 				<th>Estado:</th>
		 				<td>
		 					<select class="w3-select" name="status" id="form_status" required>
							  <option value="Abierto" <?php if($ticket->status == 'Abierto') echo 'selected'; ?>>Abierto</option>
							  <option value="En Proceso" <?php if($ticket->status == 'En Proceso') echo 'selected'; ?>>En Proceso</option>
							  <option value="Cerrado" <?php if($ticket->status == 'Cerrado') echo 'selected'; ?>>Cerrado</option>
							</select>
		 				</td>
		 			</tr>
		 			<tr>
		 				<th>Fecha creación:</th>
		 				<td>{{ $ticket->creation }}</td>
		 			</tr>
		 			<tr>
		 				<th>Fecha cierre:</th>
		 				<td>{{ $ticket->closed }}</td>
		 			</tr>
		 		</tbody>
		 	</table>
		 </div>
	 <div class="w3-col m3 s3 l2 {{$ticket->getPriority()}}" id="div_priority">

 		<div class="w3-center w3-padding-16">
 			Prioridad <br>
 			<div class="w3-center w3-container">
	 			<select class="w3-select" id="form_priority" name="priority" required>
				  <option value="Bajo" <?php if($ticket->priority == 'Bajo') echo 'selected'; ?>>Bajo</option>
				  <option value="Medio" <?php if($ticket->priority == 'Medio') echo 'selected'; ?>>Medio</option>
				  <option value="Alto" <?php if($ticket->priority == 'Alto') echo 'selected'; ?>>Alto</option>
				  <option value="Crítico" <?php if($ticket->priority == 'Crítico') echo 'selected'; ?>>Crítico</option>
				</select>
			</div>
 		</div>

	 </div>

	 <div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" class="w3-btn w3-theme">
	</div>

	</div>
	</form>
</div>

<script type="text/javascript">
	$("#form_status").click(function(){
		var opcionstatus = $("#form_status").val();
		if(opcionstatus == 'Abierto'){
			$("#form_assigned_to_group").attr('disabled','disabled');
			$("#form_solution").attr('disabled','disabled');
		}else{
			if(opcionstatus == 'En Proceso'){
				$("#form_assigned_to_group").removeAttr('disabled');
				$("#form_solution").attr('disabled','disabled');
			}else{
				$("#form_solution").removeAttr('disabled');
			}
		}
	});

	$("#form_priority").click(function(){
		var opcionPriority = $("#form_priority").val();

		switch(opcionPriority) {
		    case 'Bajo':
		        $("#div_priority").addClass('w3-green');
		        $("#div_priority").removeClass('w3-yellow');
		        $("#div_priority").removeClass('w3-deep-orange');
		        $("#div_priority").removeClass('w3-red');
		        break;
		    case 'Medio':
		        $("#div_priority").addClass('w3-yellow');
		        $("#div_priority").removeClass('w3-green');
		        $("#div_priority").removeClass('w3-deep-orange');
		        $("#div_priority").removeClass('w3-red');
		        break;
		    case 'Alto':
		        $("#div_priority").addClass('w3-deep-orange');
		        $("#div_priority").removeClass('w3-yellow');
		        $("#div_priority").removeClass('w3-green');
		        $("#div_priority").removeClass('w3-red');
		        break;
		    case 'Crítico':
		        $("#div_priority").addClass('w3-red');
		        $("#div_priority").removeClass('w3-yellow');
		        $("#div_priority").removeClass('w3-deep-orange');
		        $("#div_priority").removeClass('w3-green');
		        break;
		}
	});


</script>

@endsection