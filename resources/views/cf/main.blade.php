@extends('general/layout')

@section('submenu')
	@include('cf/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Ciber Fraude - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom w3-center">
		<h2>TypoSquatting</h2>
		<canvas id="plot1" height="150"></canvas>
	</div>
</div>


<script type="text/javascript">
	var typos = {!! $typos !!};
</script>
<script type="text/javascript" src="{{asset('js/chart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cf_main.js')}}"></script>



@endsection