@extends('general/layout')

@section('submenu')
	@include('tickets/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Nuevo Ticket</h3></div>
  <div class="w3-display-right">
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('tickets.crear_ticket') }}">

		{{ csrf_field() }}

	<div class="w3-col s12 m12 l7 w3-padding-small w3-margin-bottom">
		<label><strong>Cliente:</strong> </label>
		{{ Auth::user()->clientName}}
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Título</label>
		<input class="w3-input" type="text" name="title" value="[{{Auth::user()->clientName}}] Posible dominio fraudulento: {{ $typo->typo }}." required>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Tipo</label>
		<select class="w3-select" name="type" required>
		  <option value="" disabled selected></option>
		  @foreach($types as $t)
		  	<option value="{{ $t->type}}" @if($t->type =="Ciber Fraude") selected @endif>{{ $t->type }}</option>
		  @endforeach
		</select>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Prioridad</label>
		<select class="w3-select" name="priority" required>
		  <option value="" disabled selected></option>
		  <option value="Bajo">Bajo</option>
		  <option value="Medio" selected>Medio</option>
		  <option value="Alto">Alto</option>
		  <option value="Crítico">Crítico</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Estado</label>
		<select class="w3-select" name="status" id="form_status" required>
		  <option value="Abierto" selected>Abierto</option>
		  <option value="En Proceso">En Proceso</option>
		</select>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label>Asigar a grupo:</label>
		<select class="w3-select" name="assigned_to_group" id="form_assigned_to_group" disabled>
		  <option value="" selected></option>
		  @foreach($groups as $g)
		  	<option value="{{ $g->id}}">{{ $g->name }}</option>
		  @endforeach
		</select>
	</div>
	<br>
	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<hr>
	</div>	
	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<label>Información:</label>
		<textarea class="w3-input w3-border" style="resize:none" name="information">Se ha encontrado un posible dominio fraudulento: "{{ $typo->typo }}" cuya IP es {{ $typo->ip }}.
		</textarea>
	</div>



	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" class="w3-btn w3-theme">
	</div>
	</form>
	
</div>


<!-- modal -->
<div id="modalInfo" class="w3-modal" style="display: block;">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalInfo').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Reportar TypoSquatting</h3>
  </header>
  <div class="w3-container">
    <p>Se ha cambiado el estado del typosquatting "{{ $typo->typo }}" a <strong>Reportado</strong>.</p>
    <p>Puedes crear un ticket automáticamente desde esta página, de lo contrario, no envíes el formulario.</p>
  </div>
</div>
</div>




<script type="text/javascript">

	$("#form_status").click(function(){
		var opcionstatus = $("#form_status").val();
		if(opcionstatus == 'Abierto'){
			$("#form_assigned_to_group").attr('disabled','disabled');
		}else{
			$("#form_assigned_to_group").removeAttr('disabled');
		}
	});

</script>

@endsection

      
