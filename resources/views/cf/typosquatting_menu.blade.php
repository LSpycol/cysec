@extends('general/layout')

@section('submenu')
	@include('cf/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>TypoSquatting - <strong>Cliente: {{ Auth::user()->clientName }}</strong></h3></div>
  <div class="w3-display-right">
  	  <a href="{{ route('cf.nuevo_dominio') }}">
  		  <button class="w3-button w3-green">Nuevo Dominio</button>
      </a>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">

	@foreach($domain as $d)
		<div class="w3-col s6 m6 l4">
			<div class="w3-card-4 w3-margin w3-white">
				<header class="w3-center w3-theme w3-padding-8">
					<h3><a href="{{ route('cf.typosquatting_d', $d->id) }}">{{ $d->domain }}</a></h3>
				</header>
				<div class="w3-center w3-margin-bottom">
					Activos: {{ $d->typos }}
					<br>
				</div>
				<footer class="w3-center w3-light-grey w3-container">
				</footer>
			</div>
		</div>

	@endforeach


</div>




@endsection