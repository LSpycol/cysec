@extends('general/layout')

@section('submenu')
	@include('cf/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>TypoSquatting - <strong>Cliente: {{ Auth::user()->clientName }}</strong></h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<form method="POST" action="{{ route('cf.crear_dominio') }}">
        {{ csrf_field() }}
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Cliente</strong></label>
	    <input class="w3-input" type="text" name="client" value="{{ Auth::user()->clientName }}" disabled>
	</div>
	<div class="w3-col s12 m12 l6 w3-margin-bottom w3-container">
		<label><strong>Dominio</strong></label>
	    <input class="w3-input" type="text" name="domain" placeholder="ejemplodominio.com" required>
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
    	<input type="submit" class="w3-btn w3-theme">
    </div>
	</form>
</div>




@endsection