@extends('general/layout')

@section('submenu')
	@include('cf/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
	@if($domain->first())
	  <div class="w3-display-left"><h3>TypoSquatting - <strong>Dominio: {{ $domain->domain }}</strong></h3></div>
	  <div class="w3-display-right">
	  	@if(Auth::user()->role == 'Administrador' || Auth::user()->can('CfTypoEdit' . Auth::user()->client))
	  	  <a href="{{ route('cf.nuevo_dominio') }}">
	  		  <button class="w3-button w3-green">Nuevo Dominio</button>
	      </a>
	    @endif
	      <a href="{{ route('cf.exportar_typosquatting', $domain->id) }}">
	  		  <button class="w3-button w3-theme-d2">Exportar</button>
	      </a>
	  </div>
  	@endif
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">

	<div class="w3-col s12 m12 l12">

		<div class="w3-row">
		    <a href="javascript:void(0)" onclick="openTab(event, 'Activos');">
		      <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding w3-center w3-border-theme">Activos</div>
		    </a>
		    <a href="javascript:void(0)" onclick="openTab(event, 'Reportados');">
		      <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding w3-center">Reportados</div>
		    </a>
		    <a href="javascript:void(0)" onclick="openTab(event, 'Descartados');">
		      <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding w3-center">Descartados</div>
		    </a>
	  	</div>

	  	@if(isset($typos))
		<div>
		  <div id="Activos" class="w3-container typo w3-animate-left">
		    <div class="w3-center w3-light-grey"><h2>Dominios Activos</h2></div>

		    <table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Dominio</th>
					<th class="w3-center">IP</th>
					<th class="w3-center">NS</th>
					<th class="w3-center">MX</th>
					<th class="w3-center">Creado</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($typos as $t)
					@if($t->status == 'Activo')
						<tr>
							<td>{{ $t->typo }}</td>
							<td class="w3-center">{{ $t->ip }}</td>
							<td>{{ $t->ns }}</td>
							<td>{{ $t->mx }}</td>
							<td class="w3-center">{{ $t->created_at }}</td>
							<td class="w3-center">
								<a style="cursor: pointer;" id="optionButton{{ $t->id }}" onclick="openOptions('{{ $t->id }}')">
									<img width="20" ="" src="{{ asset('img/options.png') }}">
								</a>
								<div class="w3-hide w3-animate-zoom" id="optionDiv{{ $t->id }}">
									<a href="{{ route('cf.reportar_typosquatting', $t->id) }}"><button class="w3-button w3-tiny w3-green">Reportar</button></a>
									<a href="{{ route('cf.descartar_typosquatting', $t->id) }}"><button class="w3-button w3-tiny w3-grey">Descartar</button></a>
								</div>
							</td>
						</tr>
					@endif
				@empty
				<td>No se han encontrado dominios activos.</td>
				@endforelse
			</tbody>
			</table>

		  </div>

		  <div id="Reportados" class="w3-container typo w3-animate-left" style="display:none">
		    <div class="w3-center w3-light-grey"><h2>Dominios Reportados</h2></div>

		    <table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Dominio</th>
					<th class="w3-center">IP</th>
					<th class="w3-center">NS</th>
					<th class="w3-center">MX</th>
					<th class="w3-center">Reportado</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($typos as $t)
					@if($t->status == 'Reportado')
						<tr>
							<td>{{ $t->typo }}</td>
							<td class="w3-center">{{ $t->ip }}</td>
							<td>{{ $t->ns }}</td>
							<td>{{ $t->mx }}</td>
							<td class="w3-center">{{ $t->reported_at }}</td>
							<td class="w3-center">
								<a style="cursor: pointer;" id="optionButton{{ $t->id }}" onclick="openOptions('{{ $t->id }}')">
									<img width="20" ="" src="{{ asset('img/options.png') }}">
								</a>
								<div class="w3-hide w3-animate-zoom" id="optionDiv{{ $t->id }}">
									<a href="{{ route('cf.activar_typosquatting', $t->id) }}"><button class="w3-button w3-tiny w3-blue">Activar</button></a>
									<a href="{{ route('cf.descartar_typosquatting', $t->id) }}"><button class="w3-button w3-tiny w3-grey">Descartar</button></a>
								</div>
							</td>
						</tr>
					@endif
				@empty
				No se han encontrado dominios reportados.
				@endforelse
			</tbody>
			</table>

		  </div>

		  <div id="Descartados" class="w3-container typo w3-animate-right" style="display:none">
		    <div class="w3-center w3-light-grey"><h2>Dominios Descartados</h2></div>

		    <table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Dominio</th>
					<th class="w3-center">IP</th>
					<th class="w3-center">NS</th>
					<th class="w3-center">MX</th>
					<th class="w3-center">Descartado</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($typos as $t)
					@if($t->status == 'Descartado')
						<tr>
							<td>{{ $t->typo }}</td>
							<td class="w3-center">{{ $t->ip }}</td>
							<td>{{ $t->ns }}</td>
							<td>{{ $t->mx }}</td>
							<td class="w3-center">{{ $t->discarted_at }}</td>
							<td class="w3-center">
								<a style="cursor: pointer;" id="optionButton{{ $t->id }}" onclick="openOptions('{{ $t->id }}')">
									<img width="20" ="" src="{{ asset('img/options.png') }}">
								</a>
								<div class="w3-hide w3-animate-zoom" id="optionDiv{{ $t->id }}">
									<a href="{{ route('cf.activar_typosquatting', $t->id) }}"><button class="w3-button w3-tiny w3-blue">Activar</button></a>
									<a href="{{ route('cf.reportar_typosquatting', $t->id) }}"><button class="w3-button w3-tiny w3-green">Reportar</button></a>
								</div>
							</td>
						</tr>
					@endif
				@empty
				No se han encontrado dominios descartados.
				@endforelse
			</tbody>
			</table>

		  </div>

		</div>
		@endif

	</div>

</div>

<script>
function openTab(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("typo");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-border-theme", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-theme";
}

function openOptions(id){
	$("#optionButton" + id).addClass( "w3-hide" );
	$("#optionDiv" + id).removeClass( "w3-hide" );
}
</script>


@endsection