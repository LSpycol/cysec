@extends('general/layout')

@section('submenu')
	@include('vd/submenu')
@endsection

@section('contenido')
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Notificaciones - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
  	<a href="{{ route('vd.nueva_notificacion') }}"><button class="w3-button w3-theme-d2">Nueva Notificación</button></a>
  	<a href="{{ route('vd.exportar_notificaciones') }}"><button class="w3-button w3-theme-d2">Exportar</button></a>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<table class="w3-table w3-striped w3-bordered w3-hoverable w3-small" id="notificationsTable">
		<thead>
			<tr>
				<th class="sortable">Tipo</th>
				<th class="sortable">Título</th>
				<th class="sortable">Enlace</th>
				<th class="sortable">Herramienta</th>
				<th class="sortable">Notificado</th>
				<th class="sortable">Creado</th>
			</tr>
		</thead>
		<tbody id="tickets_t">
			@forelse ($notificaciones as $n)
			<tr>
				<td>{{ $n->type }}</td>
				<td>{{ $n->title }}</td>
				<td><a href="{{ $n->link }}" target="_blank">Acceder</a></td>
				<td>{{ $n->tool }}</td>
				<td>{{ $n->notificated_at }}</td>
				<td>{{ $n->created_at }}</td>
			</tr>
			@empty
				<td>No se han encontrado notificaciones.</td>
			@endforelse

		</tbody>
	</table>
</div>



<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('#notificationsTable').DataTable( {
    	"order": [[ 5, "desc" ]],
    	"pageLength": 25,
    	"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    } );
} );
</script>

@endsection