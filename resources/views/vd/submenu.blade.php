<div class="w3-bar">
  <a href="#" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-xlarge" onclick="w3_open()"> &#9776;</a>
  <a href="{{ route('vd.main') }}" class="w3-bar-item w3-button w3-hover-none w3-border-theme w3-bottombar "><strong>Vigilancia Digital</strong></a>
  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('VdNews' . Auth::user()->client))
  	<a href="{{ route('vd.noticias') }}" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-theme">Noticias</a>
  @endif
  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('VdNotifications' . Auth::user()->client))
  	<a href="{{ route('vd.notificaciones') }}" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-theme">Notificaciones</a>
  @endif
</div>

<script>
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}
</script>