@extends('general/layout')

@section('submenu')
	@include('vd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Reportar Noticia</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<form method="POST" action="{{ route('vd.crear_ticket_notificacion', $noticia->id) }}">

		{{ csrf_field() }}


	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom w3-center w3-border-right">
			<h3><label>Crear Ticket:&nbsp&nbsp</label> <input class="w3-check" type="checkbox" name="crear_ticket" checked="checked"></h3>
			<br>
			<div>
				<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
					<label>Título</label>
					<input class="w3-input" type="text" 
					value="[{{ Auth::user()->clientName }}] Nueva Noticia notificada de: {{ $noticia->source }}"
					 name="title" required>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Tipo</label>
					<select class="w3-select" name="type" required>
					  <option value="" disabled selected></option>
					  @foreach($types as $t)
					  	<option value="{{ $t->type}}" @if($t->type == 'Vigilancia Digital') selected @endif>{{ $t->type }}</option>
					  @endforeach
					</select>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Prioridad</label>
					<select class="w3-select" name="priority" required>
					  <option value="Bajo">Bajo</option>
					  <option value="Medio" selected>Medio</option>
					  <option value="Alto">Alto</option>
					  <option value="Crítico">Crítico</option>
					</select>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Estado</label>
					<select class="w3-select" name="status" id="form_status" required>
					  <option value="Abierto" selected>Abierto</option>
					  <option value="En Proceso">En Proceso</option>
					</select>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Asigar a grupo:</label>
					<select class="w3-select" name="assigned_to_group" id="form_assigned_to_group" disabled>
					  <option value="" selected></option>
					  @foreach($groups as $g)
					  	<option value="{{ $g->id}}">{{ $g->name }}</option>
					  @endforeach
					</select>
				</div>
				<br>
				<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
					<hr>
				</div>	
				<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
					<label>Información:</label>
					<textarea class="w3-input w3-border" name="information">Se ha notificado al cliente una nueva noticia en: {{ $noticia->source }}. La noticia tiene el siguiente titular: "{{ $noticia->title }}".
					</textarea>
				</div>
			</div>

		</div>
		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom w3-center w3-border-left">
			<h3><label>Crear Notificación:&nbsp&nbsp</label><input class="w3-check" name="crear_notificacion" type="checkbox" checked="checked"></h3>
			<br>
			<div>

				<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
					<label>Título</label>
					<input class="w3-input" type="text" value="{{ $noticia->title }}" name="notification_title" required>
				</div>


				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Tipo</label>
					<select class="w3-select" name="notification_type" required>
					  <option value="Noticia" selected>Noticia</option>
					  <option value="Tipo2">Tipo2</option>
					  <option value="Tipo3">Tipo3</option>
					  <option value="Tipo4">Tipo4</option>
					</select>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Fuente</label>
					<input class="w3-input" type="text" value="{{ $noticia->source }}" name="source" required>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Enlace</label>
					<input class="w3-input" type="text" value="{{ $noticia->link }}" name="link" required>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Herramienta</label>
					<select class="w3-select" name="tool" required>
					  <option value="Portal" selected>Portal</option>
					  <option value="Herramienta2">Herramienta2</option>
					  <option value="Herramienta3">Herramienta3</option>
					  <option value="Herramienta4">Herramienta4</option>
					</select>
				</div>

				<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
					<label>Notificado</label>
					<input class="w3-input" type="datetime-local" value="{{ date("Y-m-d") . "T" . date("H:i") }}" name="notificated_at" required>
				</div>
			</div>
		</div>
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" class="w3-btn w3-theme">
	</div>
	<form>
</div>


<script type="text/javascript">

	$("#form_status").click(function(){
		var opcionstatus = $("#form_status").val();
		if(opcionstatus == 'Abierto'){
			$("#form_assigned_to_group").attr('disabled','disabled');
		}else{
			$("#form_assigned_to_group").removeAttr('disabled');
		}
	});

</script>

@endsection