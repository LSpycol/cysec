@extends('general/layout')

@section('submenu')
	@include('vd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Noticias - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
  	<button onclick="document.getElementById('modalPalabras').style.display='block'" class="w3-button w3-theme-d2">Palabras</button>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">

  <div class="w3-col s12 m12 l12">

    <div class="w3-row">
        <a href="javascript:void(0)" onclick="openTab(event, 'Activos');">
          <div class="w3-col s6 m6 l6 tablink w3-bottombar w3-hover-light-grey w3-padding w3-center w3-border-theme">Activas</div>
        </a>
        <a href="javascript:void(0)" onclick="openTab(event, 'Descartados');">
          <div class="w3-col s6 m6 l6 tablink w3-bottombar w3-hover-light-grey w3-padding w3-center">Descartadas</div>
        </a>
      </div>

      @if(isset($noticias))
    <div>
      <div id="Activos" class="w3-container typo w3-animate-left">
        <div class="w3-center w3-light-grey"><h2>Noticias Activas</h2></div>

        <table class="w3-table w3-bordered w3-striped w3-hoverable">
      <thead>
        <tr>
          <th class="w3-center">Fuente</th>
          <th class="w3-center">Título</th>
          <th class="w3-center">Enlace</th>
          <th class="w3-center">Creado</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @forelse($noticias as $n)
          @if($n->status == 'Activa')
            <tr>
              <td class="w3-center">{{ $n->source }}</td>
              <td>{{ $n->title }}</td>
              <td><a href="{{ $n->link }}" target="_blank"> Acceder </a></td>
              <td class="w3-center">{{ $n->created_at }}</td>
              <td class="w3-center">
                <a style="cursor: pointer;" id="optionButton{{ $n->id }}" onclick="openOptions('{{ $n->id }}')">
                  <img width="20" ="" src="{{ asset('img/options.png') }}">
                </a>
                <div class="w3-hide w3-animate-zoom" id="optionDiv{{ $n->id }}">
                  <a href="{{ route('vd.reportar_noticia', $n->id) }}"><button class="w3-button w3-tiny w3-green">Reportar</button></a>
                  <a href="{{ route('vd.descartar_noticia', $n->id) }}"><button class="w3-button w3-tiny w3-grey">Descartar</button></a>
                </div>
              </td>
            </tr>
          @endif
        @empty
        <td>No se han encontrado noticias activas.</td>
        @endforelse
      </tbody>
      </table>

      </div>

      <div id="Descartados" class="w3-container typo w3-animate-right" style="display:none">
        <div class="w3-center w3-light-grey"><h2>Dominios Descartados</h2></div>

        <table class="w3-table w3-bordered w3-striped w3-hoverable">
      <thead>
        <tr>
          <th class="w3-center">Fuente</th>
          <th class="w3-center">Título</th>
          <th class="w3-center">Enlace</th>
          <th class="w3-center">Descartada</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @forelse($noticias as $n)
          @if($n->status == 'Descartada')
            <tr>
              <td class="w3-center">{{ $n->source }}</td>
              <td>{{ $n->title }}</td>
              <td><a href="{{ $n->link }}" target="_blank"> Acceder </a></td>
              <td class="w3-center">{{ $n->discarted_at }}</td>
              <td class="w3-center">
                <a style="cursor: pointer;" id="optionButton{{ $n->id }}" onclick="openOptions('{{ $n->id }}')">
                  <img width="20" ="" src="{{ asset('img/options.png') }}">
                </a>
                <div class="w3-hide w3-animate-zoom" id="optionDiv{{ $n->id }}">
                  <a href="{{ route('vd.reportar_noticia', $n->id) }}"><button class="w3-button w3-tiny w3-green">Reportar</button></a>
                  <a href="{{ route('vd.activar_noticia', $n->id) }}"><button class="w3-button w3-tiny w3-blue">Activar</button></a>
                </div>
              </td>
            </tr>
          @endif
        @empty
        <td>No se han encontrado noticias activas.</td>
        @endforelse
      </tbody>
      </table>

      </div>

    </div>
    @endif

  </div>

</div>



<!-- -->

<div id="modalPalabras" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalPalabras').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Palabras</h3>
  </header>
  <div class="w3-container">
  	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
	  	@forelse($palabras as $p)
		  	<div class="w3-margin w3-round-large w3-center w3-container w3-col s4 m4 l2 w3-border w3-border-theme">
		  		{{ $p->word }}

		  		<div class="w3-right">
		  			<a href="{{ route('vd.eliminar_palabra', $p->id) }}"><img width="15" ="" src="{{ asset('img/delete.png') }}"></a>
		  		</div>
		  	</div>
	  	@empty
	  		No hay palabras.
	  	@endforelse
 	</div>
  	<br><br>

  	<form method="POST" action="{{ route('vd.crear_palabra') }}">
        {{ csrf_field() }}
        <div class="w3-col s4 m4 l4 w3-padding-small w3-margin-bottom w3-center">
  			<label><p>Nueva palabra:</p></label>
  		</div>
  		<div class="w3-col s4 m4 l4 w3-padding-small w3-margin-bottom w3-center">
  			<input class="w3-input" type="text" name="word" placeholder="Ej: Nombre del cliente" required>
  		</div>
  		<div class="w3-col s4 m4 l4 w3-padding-small w3-margin-bottom w3-center">
  			<input type="submit" class="w3-btn w3-theme">
  		</div>
 	 </form>
  	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
  		<p> - Se buscarán noticias que contengan al menos una de las palabras.</p>
  		<p> - Una palabra puede contener más de una palabra.</p>
 	 </div>
  	<br>
  </div>
</div>
</div>

<script>
function openTab(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("typo");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-border-theme", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-theme";
}

function openOptions(id){
  $("#optionButton" + id).addClass( "w3-hide" );
  $("#optionDiv" + id).removeClass( "w3-hide" );
}
</script>


@endsection