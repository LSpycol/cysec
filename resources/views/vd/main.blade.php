@extends('general/layout')

@section('submenu')
	@include('vd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Vigilancia Digital - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<div class="w3-col s12 m12 l12">
		<canvas id="plot1" height="100"></canvas>
	</div>
</div>


<script type="text/javascript">
	var notificaciones = {!! $notificaciones !!};
</script>
<script type="text/javascript" src="{{asset('js/chart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vd_main.js')}}"></script>



@endsection