@extends('general/layout')

@section('submenu')
	@include('vd/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Nueva Notificación</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<form method="POST" action="{{ route('vd.crear_notificacion') }}">

		{{ csrf_field() }}

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Cliente:</strong> </label>
			<input class="w3-input" type="text" value="{{ Auth::user()->clientName }}" disabled>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Tipo</label>
			<select class="w3-select" name="type" required>
			  <option value="" disabled selected></option>
			  <option value="Noticia">Noticia</option>
			  <option value="Tipo2">Tipo2</option>
			  <option value="Tipo3">Tipo3</option>
			  <option value="Tipo4">Tipo4</option>
			</select>
		</div>

		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
			<label>Título</label>
			<input class="w3-input" type="text" name="title" required>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Fuente</label>
			<input class="w3-input" type="text" name="source" required>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Enlace</label>
			<input class="w3-input" type="text" name="link" required>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Herramienta</label>
			<select class="w3-select" name="tool" required>
			  <option value="" disabled selected></option>
			  <option value="Portal">Portal</option>
			  <option value="Herramienta2">Herramienta2</option>
			  <option value="Herramienta3">Herramienta3</option>
			  <option value="Herramienta4">Herramienta4</option>
			</select>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label>Notificado</label>
			<input class="w3-input" type="datetime-local" name="notificated_at" required>
		</div>

		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
			<input type="submit" class="w3-btn w3-theme">
		</div>


	</form>
</div>






@endsection