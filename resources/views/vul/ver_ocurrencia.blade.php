@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Ocurrencia: {{ $ocurrencia->id }}</h3></div>
  <div class="w3-display-right">
	  	@if(Auth::user()->role == 'Administrador')
	  	<button class="w3-button w3-green" onclick="document.getElementById('modalExtract').style.display='block'">Extraer</button>
	  	@endif
  		@if(Auth::user()->role == 'Administrador' || Auth::user()->can('VulEdit' . Auth::user()->client))
			<button class="w3-button w3-red" onclick="document.getElementById('modalDelete').style.display='block'" id="boton_actualizar">Eliminar</button>
			<a href="{{ route('vul.editar_varias_ocurrencias', $ocurrencia->id) }}"><button class="w3-button w3-theme-d2" id="boton_editar">Editar</button></a>
		@endif
	</div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-col s12 m12 l12 w3-border-theme w3-bottombar w3-center"><h3>{{ $ocurrencia->vulnerability}} -> {{ $ocurrencia->ip }}</h3></div>

	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
		<div class="w3-light-grey"><strong><h3>Datos</h3></strong></div>
		<div class="w3-col s12 m12 l4">
			<p><strong>Asset:</strong> {{ $ocurrencia->ip }}</p>
			<p><strong>Puerto:</strong> {{ $ocurrencia->port }}</p>
			<p><strong>Site:</strong> {{ $ocurrencia->site }}</p>
		</div>
		<div class="w3-col s12 m12 l4">
			<p><strong>Estado:</strong> {{ $ocurrencia->status }}</p>
			<p><strong>Detectada:</strong> {{ $ocurrencia->created_at }}</p>
		</div>
		<div class="w3-col s12 m12 l4 w3-center" id="severidad">
			<strong><h2>Severidad:</h2></strong>
			<h1>{{ $ocurrencia->severity }}</h1>
		</div>
	</div>
	<div class="w3-col s12 m12 l12 w3-margin-bottom w3-container">
		<div class="w3-light-grey"><strong><h3>Vulnerabilidad</h3></strong></div>
		@if($ocurrencia->cve != "")
			<div class="w3-col s12 m12 l4">
				<p><strong>CVE:</strong> {{ $ocurrencia->cve }}</p>
			</div>
			<div class="w3-col s12 m12 l4">
				<p><strong>Referencia: <a target="_blank" href="https://nvd.nist.gov/vuln/detail/{{ $ocurrencia->cve }}">Nist </a></strong></p>
			</div>
		@endif
		@if($ocurrencia->published != "")
			<div class="w3-col s12 m12 l4">
				<p><strong>Publicada:</strong> {{ $ocurrencia->published }}</p>
			</div>
		@endif
		<div class="w3-col s12 m12 l12">
			<p>{{ $ocurrencia->description }}</p>
		</div>
	</div>
</div>


<!-- modal -->
<div id="modalDelete" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalDelete').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Eliminar Ocurrencia</h3>
	  </header>
	    <br> ¿Desea realmente eliminar la ocurrencia?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a href="{{ route('vul.eliminar_varias_ocurrencias', $ocurrencia->id) }}><button type="button" class="w3-btn w3-red" >Eliminar </button></a>
	  		<br><br>
	  </div>
	</div>
</div>

<!-- modal -->
<div id="modalExtract" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalExtract').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Extraer datos de la vulnerabilidad</h3>
	  </header>
	    <br>Es posible, que al cargar un gran volúmen de vulnerabiidades, algunas no se hayan descargado correctamente. Con esta opción, se puede volver a extraer los datos individualmente. <br>
	    ¿Desea realmente volver a extraer los datos?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a href=""><button type="button" class="w3-btn w3-red" >Extraer </button></a>
	  		<br><br>
	  </div>
	</div>
</div>



<script type="text/javascript">
	
	var severidad = {{ $ocurrencia->severity}};

	if (severidad < 4) {
		document.getElementById("severidad").classList.add("w3-green");
	} else{
		if (severidad < 7) {
			document.getElementById("severidad").classList.add("w3-yellow");
		}else{
			document.getElementById("severidad").classList.add("w3-red");
		}
	}

</script>


@endsection