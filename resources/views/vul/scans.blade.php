@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Scans - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
  	@if(Auth::user()->role == 'Administrador' || Auth::user()->can('VulEdit' . Auth::user()->client))
  	<a href=""><button class="w3-btn w3-theme-d2">Subir Escaneo</button></a>
  	@endif
  </div>
</div>
<br>

<div class="w3-responsive w3-card-4 w3-white">
	<table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Tipo</th>
					<th class="w3-center">Site</th>
					<th class="w3-center">Template</th>
					<th class="w3-center">Engine</th>
					<th class="w3-center">Inicio</th>
					<th class="w3-center">Fin</th>
					<th class="w3-center">Vulnerabilidades</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($scans as $s)
				<tr>
					<td class="w3-center">{{ $s->type }}</td>
					<td class="w3-center">{{ $s->name }}</td>
					<td class="w3-center">{{ $s->template }}</td>
					<td class="w3-center">{{ $s->engine }}</td>
					<td class="w3-center">{{ $s->start_at }}</td>
					<td class="w3-center">{{ $s->finish_at }}</td>
					<td class="w3-center">{{ $s->vulnerabilitys }}</td>
					<td>
						@if(Auth::user()->role == 'Administrador' || Auth::user()->can('VulDelete' . Auth::user()->client))
							<a style="cursor: pointer;" onclick="modalDelete({{ $s->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
						@endif
					</td>
				</tr>
				@empty
				<td>No se han encontrado Assets</td>
				@endforelse
				
			</tbody>
		</table>
</div>

<!-- modal -->
<div id="modalDelete" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalDelete').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Eliminar Scan</h3>
	  </header>
	  	Aunque se elimine un escaneo, las ocurrencias seguirán asociadas a cada asset.
	    <br> ¿Desea realmente eliminar el escaneo de vulnerabilidades?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
	  		<br><br>
	  </div>
	</div>
</div>


<script>
function modalDelete(id){
	document.getElementById('modalDelete').style.display='block';
	var url = '{{ route("vul.eliminar_scan", ":id") }}';
	url = url.replace(':id', id);
	document.getElementById('idDelete').href= url;
}

</script>


@endsection