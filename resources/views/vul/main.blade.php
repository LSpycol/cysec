@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Vulnerabilidades - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom">
	<div class="w3-col s6 m6 l4 w3-padding-small w3-margin-bottom w3-responsive w3-animate-zoom">
		<div class="w3-col s12 m12 l12 w3-theme-d5 w3-center"><h3>Sites</h3></div>
		<div class="w3-col s6 m6 l6 w3-center w3-container">
			<img class="w3-margin-top" src="{{ asset('img/site.png') }}" width="100px">
		</div>
		<div class="w3-col s6 m6 l6">
			<br>
			<div class="w3-center"><font size="7" id="total_sites"></font></div>
		</div>
	</div>
	<div class="w3-col s6 m6 l4 w3-padding-small w3-margin-bottom w3-responsive w3-animate-zoom">
		<div class="w3-col s12 m12 l12 w3-theme-d5 w3-center"><h3>Assets</h3></div>
		<div class="w3-col s6 m6 l6 w3-center w3-container">
			<img class="w3-margin-top" src="{{ asset('img/asset.png') }}" width="100px">
		</div>
		<div class="w3-col s6 m6 l6">
			<br>
			<div class="w3-center"><font size="7" id="total_assets"></font></div>
		</div>
	</div>
	<div class="w3-col s6 m6 l4 w3-padding-small w3-margin-bottom w3-responsive w3-animate-zoom">
		<div class="w3-col s12 m12 l12 w3-theme-d5 w3-center"><h3>Vulnerabilidades</h3></div>
		<div class="w3-col s6 m6 l6 w3-center w3-container">
			<img class="w3-margin-top" src="{{ asset('img/vulnerability.png') }}" width="100px">
		</div>
		<div class="w3-col s6 m6 l6">
			<br>
			<div class="w3-center"><font size="7" id="total_vulnerabilidades"></font></div>
		</div>
	</div>
</div>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-col s12 m12 l12 w3-center">
		<canvas id="plot1" height="100"></canvas>
	</div>
</div>



<script type="text/javascript">
	var occurrences = {!! $occurrences !!};
</script>

<script type="text/javascript" src="{{asset('js/chart.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vul_main.js')}}"></script>

@endsection