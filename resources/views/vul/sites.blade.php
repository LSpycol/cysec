@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Sites - {{ Auth::user()->clientName }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">

	@forelse($sites as $site)
		<div class="w3-col s6 m6 l4">
				<div class="w3-card-4 w3-margin w3-white">
					<header class="w3-center w3-theme w3-padding-8">
						<h3><a href="{{ route('vul.ver_site', $site->id) }}">{{ $site->name }}</a></h3>
					</header>
					<div class="w3-center w3-margin-bottom">
						Assets: {{ $site->assets }}
						<br>
					</div>
					<footer class="w3-center w3-light-grey w3-container">
					</footer>
				</div>
			</div>
	@empty
		No hay Sites.
	@endforelse

</div>

@endsection