@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Editar ocurrencias - {{ $ocurrencias }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-col s12 m12 l12 w3-center w3-theme-d5 w3-margin-bottom">
		<h2>Cambiar estado</h2>
	</div>
	<div class="w3-col s12 m12 l12 w3-center">
		<form method="POST" action="{{ route('vul.cambiar_estado', $ocurrencias) }}">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<input type="hidden" name="status" value="Pendiente">
			<button class="w3-button w3-yellow" id="boton_editar">Pendiente</button>	
		</form>
		
	</div>
	<div class="w3-col s12 m12 l12 w3-center">
		<br><br><br>
	</div>
	<div class="w3-col s12 m12 l12 w3-center">
		<form method="POST" action="{{ route('vul.cambiar_estado', $ocurrencias) }}">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<input type="hidden" name="status" value="Activa">
			<button class="w3-button w3-red" id="boton_editar">Activa</button>	
		</form>
		
	</div>
	<div class="w3-col s12 m12 l12 w3-center">
		<br><br><br>
	</div>
	<div class="w3-col s12 m12 l12 w3-margin-bottom">
		<div class="w3-col s4 m4 l4 w3-center">
			<form method="POST" action="{{ route('vul.cambiar_estado', $ocurrencias)}}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<input type="hidden" name="status" value="Asumida">
				<button class="w3-button w3-blue" id="boton_editar">Asumida</button>	
			</form>
			
		</div>
		<div class="w3-col s4 m4 l4 w3-center">
			<form method="POST" action="{{ route('vul.cambiar_estado', $ocurrencias) }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<input type="hidden" name="status" value="Corregida">
				<button class="w3-button w3-green" id="boton_editar">Corregida</button>	
			</form>
			
		</div>
		<div class="w3-col s4 m4 l4 w3-center">
			<form method="POST" action="{{ route('vul.cambiar_estado', $ocurrencias) }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<input type="hidden" name="status" value="Descartada">
				<button class="w3-button w3-grey" id="boton_editar">Descartada</button>
			</form>
		</div>
	</div>
</div>



@endsection