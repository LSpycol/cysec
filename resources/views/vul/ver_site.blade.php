@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Site - {{ $site->name }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-col s12 m12 l6 w3-container">
		<div class="w3-col s12 m12 l12 w3-center w3-theme-d3 ">
			<h3>{{ $site->name }}</h3>
		</div>
		<div class="w3-col s12 m12 l6 w3-center w3-light-grey w3-margin-top">
			<h3>Assets: <a id="assets"></a></h3>
		</div>
		<div class="w3-col s12 m12 l6 w3-center w3-light-grey w3-margin-top">
			<h3>Ocurrencias: <a id="ocurrencias"></a></h3>
		</div>

		<div class="w3-col s12 m12 l12 w3-center w3-theme-d3 w3-margin-top w3-margin-bottom">
			<h3>Ocurrencias</h3>
		</div>
		<div class="w3-col s12 m12 l12 w3-margin-bottom">
			<div class="w3-col s1 m1 l1">&nbsp;</div>
			<div class="w3-col s5 m5 l5">
				<div><a href="{{ route('vul.ocurrencias', "status=Pendiente&site=$site->name") }}" id="pendiente"></a></div>
				<br>
				<div><a href="{{ route('vul.ocurrencias', "status=Activa&site=$site->name") }}"  id="activa"></a></div>
			</div>
			<div class="w3-col s1 m1 l1">&nbsp;</div>
			<div class="w3-col s5 m5 l5">
				<div><a href="{{ route('vul.ocurrencias', "status=Asumida&site=$site->name") }}" id="asumida"></a></div>
				<div><a href="{{ route('vul.ocurrencias', "status=Corregida&site=$site->name") }}" id="corregida"></a></div>
				<div><a href="{{ route('vul.ocurrencias', "status=Descartada&site=$site->name") }}" id="descartada"></a></div>
			</div>
		</div>
	</div>

	<div class="w3-col s12 m12 l6 w3-container">
		<div class="w3-col s12 m12 l12 w3-center w3-theme-d3 ">
			<h3>Assets</h3>
		</div>
		<div id="assets_vul">
			<div>
				<div class="w3-col s4 m4 l3 w3-center"><strong>IP</strong></div>
				<div class="w3-col s8 m8 l9 w3-center"><strong>Ocurrencias por severidad</strong></div>
			</div>
		</div>
	</div>

</div>


<script type="text/javascript">
	var ocurrences = {!! $occurrences !!};
</script>

<script type="text/javascript" src="{{asset('js/vul_sites.js')}}"></script>


@endsection