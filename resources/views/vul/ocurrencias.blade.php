@extends('general/layout')

@section('submenu')
	@include('vul/submenu')
@endsection

@section('contenido')
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Ocurrencias - {{ Auth::user()->clientName }}</h3></div>
  <div class="w3-display-right">
  		@if(Auth::user()->role == 'Administrador' || Auth::user()->can('VulEdit' . Auth::user()->client))
			<button class="w3-button w3-red w3-hide" onclick="document.getElementById('modalDelete').style.display='block'" id="boton_actualizar">Eliminar</button>
			<button class="w3-button w3-theme-d2 w3-hide" id="boton_editar">Editar</button>
		@endif
			<a href="{{ route('vul.exportar_ocurrencias', $_GET) }}">
	  		  <button class="w3-button w3-theme-d2">Exportar</button>
	      	</a>
			<button onclick="document.getElementById('modalFiltroAvanzado').style.display='block'" class="w3-button w3-theme-d2">Filtro Avanzado</button>
	</div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<table class="w3-table w3-striped w3-bordered w3-hoverable w3-small" id="OcTable">
			<thead>
				<tr>
					<th>
						<input type="checkbox" style="transform: scale(1.5);" id="checkbox_ticket_all">
					</th>
					<th class="w3-center">Vulnerabilidad</th>
					<th class="w3-center">Ip</th>
					<th class="w3-center">Puerto</th>
					<th class="w3-center">Estado</th>
					<th class="w3-center">Site</th>
					<th class="w3-center">Fecha</th>
					<th class="w3-center">Severidad</th>
				</tr>
			</thead>
			<tbody>
				@forelse($occurrences as $o)
				<tr>
					<td><input type="checkbox" class="checkbox_ticket" id="check_{{ $o->id }}" style="transform: scale(1.5);">
					</td>
					<td class="w3-center"><a href="{{ route('vul.ver_ocurrencia', $o->id) }}">{{ $o->vulnerability }}</a></td>
					<td class="w3-center">{{ $o->ip }}</td>
					<td class="w3-center">{{ $o->port }}</td>
					<td class="w3-center">{{ $o->status }}</td>
					<td class="w3-center">{{ $o->name }}</td>
					<td class="w3-center">{{ $o->created_at }}</td>
					<td class="w3-center severity">{{ $o->severity }}</td>
				</tr>
				@empty
				<td>No se han encontrado ocurrencias</td>
				@endforelse
				
			</tbody>
		</table>
</div>



<!-- modal -->
<div id="modalDelete" class="w3-modal">
	<div class="w3-modal-content w3-card-4 w3-animate-opacity">
	  <header class="w3-container w3-theme-d1"> 
	    <span onclick="document.getElementById('modalDelete').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h3>Eliminar Ocurrencias</h3>
	  </header>
	    <br> ¿Desea realmente eliminar las ocurrencias seleccionadas?
	    <br><br>
	    <div class="w3-container w3-center">
	  		<a id="idDelete"><button type="button" class="w3-btn w3-red" >Eliminar </button></a>
	  		<br><br>
	  </div>
	</div>
</div>




<!-- -->

<div id="modalFiltroAvanzado" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalFiltroAvanzado').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Filtro Avanzado</h3>
  </header>
  <div class="w3-container">

  	<form method="GET" action="{{ route('vul.ocurrencias') }}">
  		{{ csrf_field() }}
  	<div class="w3-row-padding">
	  <div class="w3-col s12 l6 w3-padding-small">
	    <label>Vulnerabilidad</label>
	    <input class="w3-input w3-border" name="vulnerability" type="text">
	  </div>

	  <div class="w3-col s12 l6 w3-padding-small">
	    <label>Ip</label>
	    <input class="w3-input w3-border" name="ip" type="text">
	  </div>

	  <div class="w3-col s12 l6 w3-padding-small">
	    <label>Puerto</label>
	    <input class="w3-input w3-border" name="port" type="text">
	  </div>

	  <div class="w3-col s12 l6 w3-padding-small">
	    <label>Site</label>
	    <input class="w3-input w3-border" name="site" type="text">
	  </div>

	  <div class="w3-col s12 l6 w3-padding-small">
	  	<label><br>Estado</label>
	  	<select class="w3-select" name="status">
		  <option value="" selected></option>
		  <option value="Pendiente">Pendiente</option>
		  <option value="Activa">Activa</option>
		  <option value="Asumida">Asumida</option>
		  <option value="Corregida">Corregida</option>
		  <option value="Descartada">Descartada</option>
		</select>
	  </div>

	</div>

	<br>
  </div>
  <footer class="w3-container w3-light-grey w3-center">
    <button class="w3-button w3-theme-d2">Filtrar</button>
  </footer>
  </form>
</div>
</div>




<script src="{{ asset('js/tickets_listado.js') }}" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('#OcTable').DataTable( {
    	"pageLength": 50,
    	"language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    } );
} );

$("#boton_editar").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("vul.editar_varias_ocurrencias", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});

$("#idDelete").click(function(){
	var checkboxs = '';
	$('input:checkbox[id^="check_"]:checked').each(function(){
		var checkbox = $(this).attr("id").replace('check_','');
		checkboxs = checkboxs.concat(checkbox, ',');
	});
	checkboxs = checkboxs.substring(0, checkboxs.length - 1);

	var url = '{{ route("vul.eliminar_varias_ocurrencias", "checkboxs") }}';
	
	url = url.replace('checkboxs', checkboxs);
	window.location.href = url;
});


$('.severity').each(function(i, obj) {
	var color = "";

	switch($(this).html()){
		case "0": color = "#a1f98b";
				break;
		case "1": color = "#b3f98b";
				break;
		case "2": color = "#cbf98b";
				break;
		case "3": color = "#d8f98b";
				break;
		case "4": color = "#eaf98b";
				break;
		case "5": color = "#f9f58b";
				break;
		case "6": color = "#f9ea8b";
				break;
		case "7": color = "#f9df8b";
				break;
		case "8": color = "#f9c98b";
				break;
		case "9": color = "#f9b88b";
				break;
		case "10": color = "#f99f8b";
				break;
		
	}
    $(this).css("background-color",  color);
});

</script>



@endsection