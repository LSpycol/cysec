<!DOCTYPE html>
<html>
<head>
    <title>CySec-Portal</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="{{ asset('css/colors.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('js/rainyday.js') }}"></script>
</head>
<body>

<img id="background" class="imagenFondoIndex" alt="background"  src="{{ asset("img/bg2.jpg") }}" />

<div class="w3-col s10 m8 l5 w3-display-middle w3-center w3-round-large" id="form_login">
    <div class="content">
      <div class="w3-center w3-border-bottom w3-border-theme"><h2>Inicio Sesión</h2></div>

      <form method="POST" action="{{ route('login') }}">
          @csrf
      <label class="w3-padding-small">Email</label>
      <input id="email" type="email" class="w3-input w3-center w3-light-grey w3-round-xlarge input_login" name="email" value="{{ old('email') }}" required autofocus>

      @if ($errors->has('email'))
          <span class="invalid-feedback" role="alert">
              <font color="red">{{ $errors->first('email') }}</font>
          </span>
          blur
      @endif

      <label class="w3-padding-small">Contraseña</label>
      <input id="password" type="password" class="w3-input w3-center w3-light-grey w3-round-xlarge input_login" name="password" required>

      @if ($errors->has('password'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif

      <input class="w3-check" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
      <label>Recordar</label>

      <br>
      <div class="w3-center w3-margin-top">
          <button type="submit" class="w3-btn w3-white w3-border w3-border-theme">
              {{ __('Login') }}
          </button>
      </div>
      <br>
      <a class="btn btn-link" href="{{ route('password.request') }}"> ¿Olvidaste la contraseña?</a>
      </form>
      <br>
    </div>
</div>

</body>
