@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - Nuevo Usuarios</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">

	<form method="POST" action="{{ route('control.crear_usuario') }}">
		@csrf

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Nombre</strong></label>
			<input id="name" type="text" class="w3-input" name="name" value="{{ old('name') }}" required autofocus>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Teléfono</strong></label>
			<input id="phone" type="text" class="w3-input" name="phone" value="{{ old('phone') }}">
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Email</strong></label>
			<input id="email" type="email" class="w3-input" name="email" value="{{ old('email') }}" required>
		</div>

		<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Rol</strong></label>
			<select class="w3-select" name="role" required>
			  <option value="" disabled selected></option>
			  <option value="Administrador">Administrador</option>
			  <option value="Analista">Analista</option>
			  <option value="Primera Línea">Primera Línea</option>
			  <option value="Cliente">Cliente</option>
			</select>
		</div>

		<br><br>
		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
			<button type="submit" class="w3-btn w3-theme">
	            Enviar
	        </button>
	    </div>

	</div>

@endsection