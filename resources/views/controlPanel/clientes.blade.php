@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - Clientes</h3></div>
  <div class="w3-display-right">
  	<a href="{{ route('control.nuevo_cliente') }}"><button class="w3-button w3-theme-d2">Nuevo Cliente</button></a>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div id="data" class="w3-container form_display">
		<table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Cliente</th>
					<th class="w3-center">IP</th>
					<th class="w3-center">Opciones</th>
				</tr>
			</thead>
			<tbody>
				@forelse($clients as $c)
				<tr>
					<td>{{ $c->name }}</td>
					<td class="w3-center">{{ $c->ip }}</td>
					<td class="w3-center">
						<a href="{{ route('control.editar_cliente', $c->id) }}"><img width="20" ="" src="{{ asset('img/edit.png') }}"></a>
						<a style="cursor: pointer;" onclick="modalDelete({{ $c->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
					</td>
				</tr>
				@empty
				<td>No se han encontrado clientes</td>
				@endforelse
				
			</tbody>
		</table>
	</div>

	<!-- modal -->
	<div id="modalDelete" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-opacity">
		  <header class="w3-container w3-theme-d1"> 
		    <span onclick="document.getElementById('modalDelete').style.display='none'" 
		    class="w3-button w3-display-topright">&times;</span>
		    <h3>Eliminar Cliente</h3>
		  </header>
		  	Al borrar un cliente, se desactivará en el portal y no se podrá acceder a los datos almacenados aunque no serán borrados. Para recuperar un cliente, contacte con el administrador del portal.
		    <br> ¿Desea realmente eliminar el cliente?
		    <br><br>
		    <div class="w3-container w3-center">
		  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
		  		<br><br>
		  </div>
		</div>
	</div>
</div>


<script>
function modalDelete(id){
	document.getElementById('modalDelete').style.display='block';
	var url = '{{ route("control.eliminar_cliente", ":id") }}';
	url = url.replace(':id', id);
	document.getElementById('idDelete').href= url;
}

</script>
@endsection