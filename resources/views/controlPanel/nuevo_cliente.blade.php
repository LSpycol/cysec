@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - Clientes</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
		
	<form method="POST" action="{{ route('control.crear_cliente')}} ">

	{{ csrf_field() }}
	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>Nombre</strong></label>
		<input class="w3-input" type="name" name="name" required>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>IP*</strong></label>
		<input class="w3-input" type="ip" name="ip">
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<p><strong>Nota:</strong> La IP se usará como referencia para los tickets que se generen automáticamente que provengan de un SIEM. Este campo es opcional.</p>
		<p>Se debe de usar la ip del servidor que vaya a enviar los logs.</p>
	</div>


	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<button type="button" onclick="document.getElementById('modalConfirmate').style.display='block'" class="w3-btn w3-theme" >Enviar </button>
	</div>

	<!-- modal -->
	<div id="modalConfirmate" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-opacity">
		  <header class="w3-container w3-theme-d1"> 
		    <span onclick="document.getElementById('modalConfirmate').style.display='none'" 
		    class="w3-button w3-display-topright">&times;</span>
		    <h3>Confirmar la creación de un nuevo cliente</h3>
		  </header>
		  Al crear un nuevo cliente, ningún usuario que no sea administrador tendrá permisos para este cliente. Se recomienda revisar los permisos.
		  <div class="w3-container w3-center">
		  	<br><br><input type="submit" value="Confirmar" class="w3-btn w3-theme"><br><br>
		  </div>
		</div>
	</div>

	</form>

</div>
@endsection