@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - Grupos</h3></div>
  <div class="w3-display-right">
  	<a href="{{ route('control.nuevo_grupo') }}"><button class="w3-button w3-theme-d2">Nuevo Grupo</button></a>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">

	<div id="data" class="w3-container form_display">
		<table class="w3-table w3-bordered w3-striped w3-hoverable">
			<thead>
				<tr>
					<th class="w3-center">Grupo</th>
					<th class="w3-center">Propietario</th>
					<th class="w3-center">Componentes</th>
					<th class="w3-center">Opciones</th>
				</tr>
			</thead>
			<tbody>
				@forelse($groups as $g)
				<tr>
					<td class="w3-center">{{ $g->name }}</td>
					<td class="w3-center">{{ $g->owner }}</td>
					<td class="w3-center">{{ $g->users }}</td>
					<td class="w3-center">
						<a href="{{ route('control.editar_grupo', $g->id) }}"><img width="20" ="" src="{{ asset('img/edit.png') }}"></a>
						<a style="cursor: pointer;" onclick="modalDelete({{ $g->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
					</td>
				</tr>
				@empty
				<td>No se han encontrado grupos</td>
				@endforelse
			</tbody>
		</table>
	</div>

	<!-- modal -->
	<div id="modalDelete" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-opacity">
		  <header class="w3-container w3-theme-d1"> 
		    <span onclick="document.getElementById('modalDelete').style.display='none'" 
		    class="w3-button w3-display-topright">&times;</span>
		    <h3>Eliminar Grupo</h3>
		  </header>
		    <div class="w3-container w3-center">
		  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
		  		<br><br>
		  </div>
		</div>
	</div>
</div>


<script>

var users = [];

function openSub(sub) {
    var i;
    var x = document.getElementsByClassName("form_display");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(sub).style.display = "block";  
}

function modalDelete(id){
	document.getElementById('modalDelete').style.display='block';
	var url = '{{ route("control.eliminar_grupo", ":id") }}';
	url = url.replace(':id', id);
	document.getElementById('idDelete').href= url;
}



</script>
@endsection