@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - Nuevo Grupo</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('control.crear_grupo')}} ">

	{{ csrf_field() }}

	<input type="hidden" name="users" value="" id="inputUsers">

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>Nombre</strong></label>
		<input class="w3-input" type="text" name="name" required>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>Propietario</strong></label>
		<select class="w3-select" name="owner" required>
			@foreach($users as $s)
		  		<option value="{{$s->id}}">{{$s->name}}</option>
			@endforeach
		</select>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>Email</strong></label>
		<input class="w3-input" type="email" name="email">
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<label><strong>Usuarios:</strong></label> 	
			Arrastra los usuarios que pertenecerán al grupo.
		<br><br>

		<div class="w3-col s12 m6 l6 w3-padding-small w3-margin-bottom">
			<div class="w3-center w3-margin-bottom"><strong>Listado de usuarios</strong></div>
			<div id="div1" class="w3-card-4" ondrop="drop(event)" ondragover="allowDrop(event)" style="height: 500px; overflow-y: scroll;">
				@foreach($users as $s)
					<button type="button" class="w3-col s12 m12 l12 w3-margin-bottom w3-card-4 w3-btn" id="drag{{$s->id}}" draggable="true" ondragstart="drag(event)">{{$s->name}}</button>
				
				@endforeach
			</div>
		</div>

		<div class="w3-col s12 m6 l6 w3-padding-small w3-margin-bottom">
			<div class="w3-center w3-margin-bottom"><strong>Usuarios en el grupo</strong></div>
			<div id="div2" class="w3-card-4" ondrop="drop(event)" ondragover="allowDrop(event)" style="height: 500px; overflow-y: scroll;">
			</div>
		</div>

	</div>


	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" class="w3-btn w3-theme" value="Enviar">
	</div>

	</form>
</div>


<script>
function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  
  var padre = $('#' + data).parent().attr("id");
  
  if(padre == 'div1'){
  	document.getElementById('div2').appendChild(document.getElementById(data));
  	var id = data.replace("drag", "");

  	document.getElementById('inputUsers').value = document.getElementById('inputUsers').value + id + ","

  }else{
  	document.getElementById('div1').appendChild(document.getElementById(data));
  	var id = data.replace("drag", "");
  	document.getElementById('inputUsers').value = document.getElementById('inputUsers').value.replace(id + ",", "");
  }
   
}
</script>
@endsection