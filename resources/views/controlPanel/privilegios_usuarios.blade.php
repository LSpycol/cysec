@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Privilegios de {{ $user->name }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-responsive">
		<form method="POST" action="{{ route('control.privilegios_usuario_guardar', $user)}} ">
			{{ csrf_field() }}
		<table class="w3-table w3-bordered w3-small w3-hoverable">
			<thead>
				<tr>
					<td></td>
					<td></td>
					<th class="w3-center w3-light-gray">Cliente*</th>
					<th colspan="4" class="w3-center w3-sand">Tickets</th>
					<th colspan="6" class="w3-center w3-pale-blue">Servicios Gestionados</th>
					<th colspan="4" class="w3-center" style="background-color: #c3bcd6">Vulnerabilidades</th>
					<th colspan="3" class="w3-center" style="background-color: #ffff80">Ciber Fraude</th>
					<th colspan="3" class="w3-center" style="background-color: #ffbb99">Vigilancia Digital</th>
					<th colspan="1" class="w3-center" style="background-color: #b3ff99">ANVD</th>
				</tr>
				<tr>
					<th></th>
					<th class="w3-center">Cliente</th>
					<td class="w3-center">
						<strong><abbr title="El usuario podrá ver el cliente.">Ver</abbr></strong>
					</td>

					<td class="w3-center">
						<strong><abbr title="El usuario podrá ver tickets del cliente.">Ver</abbr>
					</td></strong>
					<td class="w3-center">
						<abbr title="El usuario podrá actualizar tickets del cliente.">Actualizar</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá editar tickets del cliente.">Editar</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá crear tickets del cliente.">Crear</abbr>
					</td>
					<!-- SSGG -->
					<td class="w3-center">
						<strong><abbr title="El usuario podrá ver panel de monitorización del cliente.">Ver</abbr></strong>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá editar y eliminar los assets y servicios del cliente.">Editar</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá crear assets y servicios del cliente.">Crear</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá ver las ips bloqueadas del cliente.">IpVer</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá añadir, eliminar y editar las ips bloqueadas del cliente.">IpEditar</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá gestionar el engine del cliente.">Engine</abbr>
					</td>

					<!-- Vulnerabilidades -->
					<td class="w3-center">
						<strong><abbr title="El usuario podrá ver las vulnerabilidades del cliente.">Ver</abbr></strong>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá activar las vulnerabilidades del cliente.">Activar</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá editar las vulnerabilidades del cliente y subir nuevos escaneos.">Editar</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá eliminar vulnerabilidades, assets y sites del cliente.">Eliminar</abbr>
					</td>

					<!-- Ciber Fraude -->
					<td class="w3-center">
						<strong><abbr title="El usuario podrá ver el panel de ciber fraude del cliente.">Ver</abbr></strong>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá ver y cambiar el  estado de typosquatting del cliente.">Typo</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá crear y eliminar dominios del cliente.">TypoEdit</abbr>
					</td>

					<!-- Vigilancia Digital -->
					<td class="w3-center">
						<strong><abbr title="El usuario podrá ver el panel de vigilancia digital del cliente.">Ver</abbr></strong>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá operar con las noticias del cliente.">Noticias</abbr>
					</td>
					<td class="w3-center">
						<abbr title="El usuario podrá operar con las notificaciones del cliente.">Notific.</abbr>
					</td>

					<!-- ANVD -->
					<td class="w3-center">
						<strong><abbr title="El usuario tendrá acceso a la sección de ANVD del cliente.">Ver</abbr></strong>
					</td>
				</tr>
			</thead>
			<tbody>
				@foreach($clients as $c)
				<tr id="tr{{$c->id}}">
					<td><span onclick="$('#tr{{$c->id}}').hide()" class="w3-button">&times;</span></td>
					<td>{{ $c->name }}</td>
					@foreach($role as $privileges)
					<td class="w3-center tdPv" id="td{{$privileges->name . $c->id}}">
						<input class="w3-check" type="checkbox" id="{{$privileges->name . $c->id}}" name="{{$privileges->name . $c->id}}"></td>
					@endforeach
				</tr>
				@endforeach
			</tbody>
		</table>
		<br>

		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
			<p>* Para que un usuario tenga permiso a una sección de un cliente, debe tener permiso para ver dicho cliente (primera  columna).</p>
		</div>

		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
			<input type="submit" value="Guardar Permisos" class="w3-btn w3-theme">
		</div>
		</form>
	</div>

</div>


<script type="text/javascript">

var permisos = {!! $permisosUsuario !!};
var i = 0;
while(permisos[i] !=  null){
	$('#' + permisos[i]['name']).prop('checked', true);
	i++;
}

$( ".tdPv" ).each(function() {
  var id = $(this).attr('id').replace("td", "");
  if($('#' + id).is(":checked")){
  	$(this).addClass('w3-pale-green');
  }else{
  	$(this).addClass('w3-pale-red');
  }
});

$(document).ready(function(){
  $('.w3-check').click(function(){
    if ($(this).is(":checked")){
    	$('#td' + $(this).attr('id')).removeClass('w3-pale-red');
    	$('#td' + $(this).attr('id')).addClass('w3-pale-green');
    } else{
    	$('#td' + $(this).attr('id')).removeClass('w3-pale-green');
    	$('#td' + $(this).attr('id')).addClass('w3-pale-red');
    } 
    
  });
});
</script>

@endsection