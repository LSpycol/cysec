@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General - Usuarios</h3></div>
  <div class="w3-display-right">
  	<a href="{{ route('control.nuevo_usuario') }}"><button class="w3-button w3-theme-d2">Nuevo Usuario</button></a>
  </div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<table class="w3-table w3-bordered w3-striped w3-hoverable">
		<thead>
			<tr>
				<th class="w3-center">Nombre</th>
				<th class="w3-center">Correo</th>
				<th class="w3-center">Teléfono</th>
				<th class="w3-center">Rol</th>
				<th class="w3-center">Creado</th>
				<th class="w3-center">Opciones</th>
			</tr>
		</thead>
		<tbody>
			@forelse($users as $u)
			<tr>
				<td>{{ $u->name }}</td>
				<td class="w3-center">{{ $u->email }}</td>
				<td class="w3-center">{{ $u->phone }}</td>
				<td class="w3-center">{{ $u->role }}</td>
				<td class="w3-center">{{ $u->created_at }}</td>
				<td class="w3-center">
					<a href="{{ route('control.privilegios_usuario', $u->id) }}"><img width="20" ="" src="{{ asset('img/key.png') }}"></a>
					<a style="cursor: pointer;" onclick="modalDelete({{ $u->id }})"><img width="20" ="" src="{{ asset('img/delete.png') }}"></a>
				</td>
			</tr>
			@empty
			<td>No se han encontrado usuarios</td>
			@endforelse
			
		</tbody>
	</table>
</div>

	<!-- modal -->
	<div id="modalDelete" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-opacity">
		  <header class="w3-container w3-theme-d1"> 
		    <span onclick="document.getElementById('modalDelete').style.display='none'" 
		    class="w3-button w3-display-topright">&times;</span>
		    <h3>Eliminar Usuario</h3>
		  </header>
		    <div class="w3-container w3-center">
		    	¿Desea realmente eliminar el usuario?
		   		 <br><br>
		  		<a id="idDelete"><button type="button" class="w3-btn w3-theme" >Eliminar </button></a>
		  		<br><br>
		  </div>
		</div>
	</div>

	

</div>


<script>

function modalDelete(id){
	document.getElementById('modalDelete').style.display='block';
	var url = '{{ route("control.eliminar_usuario", ":id") }}';
	url = url.replace(':id', id);
	document.getElementById('idDelete').href= url;
}

</script>




@endsection