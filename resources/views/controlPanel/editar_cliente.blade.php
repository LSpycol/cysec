@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Editar Cliente</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<form method="POST" action="{{ route('control.edicion_cliente', $client->id)}} ">

	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>Nombre</strong></label>
		<input class="w3-input" type="name" name="name" value="{{ $client->name }}" required>
	</div>

	<div class="w3-col s12 m12 l6 w3-padding-small w3-margin-bottom">
		<label><strong>IP*</strong></label>
		<input class="w3-input" type="ip" name="ip" value="{{ $client->ip }}">
	</div>

	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom">
		<p><strong>Nota:</strong> La IP se usará como referencia para los tickets que se generen automáticamente que provengan de un SIEM.</p>
		<p>Se debe de usar la ip del servidor que vaya a enviar los logs.</p>
	</div>
	<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
		<input type="submit" value="Editar" class="w3-btn w3-theme">
	</div>
</form>

</div>

@endsection