@extends('general/layout')

@section('submenu')
	@include('controlPanel/submenu')
@endsection

@section('contenido')

<br>
<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Panel General</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white">
	<div class="w3-bar w3-light-grey">
	  <button class="w3-bar-item w3-button" onclick="openSub('data')">Datos de la cuenta</button>
	  <button class="w3-bar-item w3-button" onclick="openSub('password')">Reiniciar contraseña</button>
	</div>

	<div id="data" class="w3-container form_display">
		<h2>Datos de la cuenta</h2>
	  	<form method="POST" action="{{ route('control.editar_datos') }}">
	  		{{ csrf_field() }}
			{{ method_field('PUT') }}
		<div class="w3-col s12 m6 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Nombre:</strong> </label>
			<input class="w3-input" type="text" name="name" value="{{ Auth::user()->name }}" >
		</div>

		<div class="w3-col s12 m6 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Email:</strong> </label>
			<input class="w3-input" type="text" name="email" value="{{ Auth::user()->email }}" disabled>
		</div>

		<div class="w3-col s12 m6 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Teléfono:</strong> </label>
			<input class="w3-input" type="text" name="phone" value="{{ Auth::user()->phone }}" >
		</div>

		<div class="w3-col s12 m6 l6 w3-padding-small w3-margin-bottom">
			<label><strong>Idioma:</strong> </label>
			<select class="w3-select" name="language" required>
			  <option selected value="Español">Español</option>
			  <option value="English">English</option>
			</select>
		</div>

		<div class="w3-col s12 m12 l12 w3-padding-small w3-margin-bottom w3-center">
			<input type="submit" value="Guardar" class="w3-btn w3-theme">
		</div>
		</form>
	</div>

	<div id="password" class="w3-container form_display" style="display:none">
		<div class="w3-col s12 m6 l4 w3-padding-small ">
			<form method="POST" action="{{ route('control.password') }}">
			{{ csrf_field() }}
			{{ method_field('PUT') }}

			<h2>Reiniciar contraseña</h2>
			@csrf
			<label><strong>Email:</strong> </label>
			<input id="email" type="email" class="w3-input" name="email" value="{{ $email ?? old('email') }}" required autofocus>

	        <label><strong>Contraseña:</strong> </label>
	        <input id="password" type="password" class="w3-input" name="password" required>

	        <label><strong>Re-Contraseña:</strong> </label>
	        <input id="password-confirm" type="password" class="w3-input" name="password_confirmation" required>
	        <br>
	        <button type="submit" class="w3-btn w3-theme">
	            Reiniciar
	        </button>
	    	</form>
		</div>
	</div>

</div>


<script>
function openSub(Sub) {
    var i;
    var x = document.getElementsByClassName("form_display");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(Sub).style.display = "block";  
}
</script>




@endsection