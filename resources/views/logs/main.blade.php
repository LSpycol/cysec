@extends('general/layout')

@section('submenu')
	@include('logs/submenu')
@endsection

@section('contenido')

<br>

<div class="w3-container w3-display-container w3-padding-16">
  <div class="w3-display-left"><h3>Logs - {{ $tipo }}</h3></div>
</div>

<br>

<div class="w3-responsive w3-card-4 w3-white w3-margin-bottom w3-light-grey">
	@foreach($logs as $l)
		<p>{{ $l }}</p>
	@endforeach
</div>

@endsection