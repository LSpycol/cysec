<div class="w3-container w3-card-4 w3-theme-d5 w3-display-container">
	<div class="w3-left">
		<img width="150px" src="{{ asset('img/cysec_blanco.png') }}">
	</div>

	<div class="w3-display-right w3-margin-right">
		
			<button id="ButtonClients" class="w3-btn w3-hover-light-grey" onclick="document.getElementById('modalClient').style.display='block'">
				{{ Auth::user()->clientName }} 
			</button>
		- 
		<div class="w3-dropdown-hover">
		    <button class="w3-button w3-theme-d5">{{ Auth::user()->name }}</button>

		    <div class="w3-dropdown-content w3-bar-block w3-border" style="right:0">
		    	<a href="{{ route('control.main') }}" class="w3-bar-item w3-button">Panel de Control</a>

		    	<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="w3-bar-item w3-button">Cerrar Sesión</a>

		    	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
				</form>
		    </div>
		  </div>	
	</div>

</div>


<!-- modal -->
<div id="modalClient" class="w3-modal">
<div class="w3-modal-content w3-card-4 w3-animate-opacity">
<form method="POST" action="{{ route('changeClient') }}">
	{{ csrf_field() }}
  <header class="w3-container w3-theme-d1"> 
    <span onclick="document.getElementById('modalClient').style.display='none'" 
    class="w3-button w3-display-topright">&times;</span>
    <h3>Selecciona un Cliente</h3>
  </header>
  <div class="w3-container">
  	<br>
  	<div class="formInModal" id="formClients">
	    <!-- Clientes cargados por Ajax -->
	 </div>

	<br>
  </div>
  <footer class="w3-container w3-light-grey w3-center">
    <button class="w3-button w3-white w3-border w3-border-theme w3-round-large">Enviar</button>
  </footer>
  </form>
</div>
</div>


<script>
$(document).ready(function(){
    $("#ButtonClients").click(function(){
        $.ajax({
		    url: '{{ route('loadClients') }}',
		    type: "GET",
		    success: function(data){
		      $('#formClients').html(data);    
		    }
		});
    });
});
</script>
