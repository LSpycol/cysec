<div class="w3-sidebar w3-animate-left w3-bar-block w3-border-right" style="display:none" id="mySidebar">
  
  <div class="w3-container w3-theme-d3">
    <span onclick="w3_close()" class="w3-button w3-display-topright">&times;</span>
    <p><h4>Menu</h4></p>
  </div>

  <a href="{{ route('tickets.main') }}" class="w3-bar-item w3-button">Tickets</a>

  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('VulView' . Auth::user()->client))
    <a href="{{ route('vul.main') }}" class="w3-bar-item w3-button">Vulnerabilidades</a>
  @endif

  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('VdView' . Auth::user()->client))
    <a href="{{ route('vd.main') }}" class="w3-bar-item w3-button">Vigilancia Digital</a>
  @endif

  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('CfView' . Auth::user()->client))
    <a href="{{ route('cf.main') }}" class="w3-bar-item w3-button">Ciber Fraude</a>
  @endif

  @if(Auth::user()->role == 'Administrador' || Auth::user()->can('SsggView' . Auth::user()->client))
    <a href="{{ route('ssgg.main') }}" class="w3-bar-item w3-button">Servicios Gestionados</a>
  @endif
  
  <br>

  <div class="w3-container w3-theme-d3">
    <h4>Utilidades</h4>
  </div>

   @if(Auth::user()->role == 'Administrador' || Auth::user()->can('AnvdView' . Auth::user()->client))
  <a href="{{ route('anvd.main') }}" class="w3-bar-item w3-button">ANVD</a>
  @endif

  @if(Auth::user()->role == 'Administrador' || Auth::user()->role == 'Primera Línea')
    <a href="{{ route('pl.main') }}" class="w3-bar-item w3-button">Primera línea</a>
  @endif


  @if(Auth::user()->role == 'Administrador')
    <a href="{{ route('logs.portal') }}" class="w3-bar-item w3-button">Logs</a>
  @endif

</div>
