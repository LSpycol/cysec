<!DOCTYPE html>
<html>
<head>
	<title>CySec-Portal</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="{{ asset('css/colors.css') }}" rel="stylesheet">
	<link href="{{ asset('css/main.css') }}" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="w3-theme-l5">
<header>
	@include('general/cabecera')
	@include('general/menu')
	<div class="w3-white">
		@yield('submenu')
	</div>
</header>

@if(session()->has('errors'))
	<div class="w3-panel w3-pale-red w3-border w3-display-container">
	  <span onclick="this.parentElement.style.display='none'" class="w3-button w3-large w3-display-topright">&times;</span>

		<h4>{{ session('errors')}}</h4>

	</div>
@endif

@if(session()->has('success'))
	<div class="w3-panel w3-pale-green w3-border w3-display-container">
	  <span onclick="this.parentElement.style.display='none'" class="w3-button w3-large w3-display-topright">&times;</span>

		<h4>{{ session('success')}}</h4>

	</div>
@endif


<div class="w3-row w3-theme-l5">
	<div id="div_contenedor_principal">
		@yield('contenido')
	</div>
</div>
	<br><br><br>
<footer>
	<div class="w3-display-bottommiddle">
	</div>
</footer>
</body>
</html>