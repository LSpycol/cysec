<?php

return [

    'menu.Listado' => 'Listado',
    'menu.NuevoTicket' => 'Nuevo Ticket',
    'menu.Informes' => 'Informe',
    'main.PanelGeneral'   => 'Panel General',
    'main.TicketsAbiertos' => 'Tickets Abiertos',
    'main.HistoricoTickets' => 'Histórico de Tickets desde',
    'main.OrigenHistorico' => 'Origen del histórico',
    'main.Desde' => 'Desde',
    'main.Filtrar' => 'Filtrar',
    'listado.FiltroAvanzado' => 'Filtro Avanzado',
    'listado.Exportar' => 'Exportar',
    'listado.Apertura' => 'Apertura',
    'listado.Titulo' => 'Título',
    'listado.Tipo' => 'Tipo',
    'listado.Estado' => 'Estado',
    'listado.Prioridad' => 'Prioridad',
    'listado.Hasta' => 'Hasta',
    'listado.Contiene' => 'Contiene',
    'listado.Generado' => 'Generado',
    'listado.Cierre' => 'Cierre',
];
