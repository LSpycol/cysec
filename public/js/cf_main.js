
var typo_activos = 0
var typo_descartados = 0
var typo_reportados = 0

var i = 0;
while(typos[i] != null){
    if(typos[i]['status'] == 'Activo') typo_activos++;
    if(typos[i]['status'] == 'Reportado') typo_descartados++;
    if(typos[i]['status'] == 'Descartado') typo_reportados++;
    i++;
}

var values = [typo_activos, typo_reportados, typo_descartados];

/* grafico 1*/

var ctx = document.getElementById("plot1");
var grafico2 = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Activos','Descartados','Reportados'],
        datasets: [{
            data: values,
            backgroundColor: ['rgba(0, 153, 255, 0.5)','rgba(115, 115, 115, 0.5)','rgba(0, 255, 0, 0.5)'],
            borderColor: ['rgba(0, 153, 255, 1)','rgba(115, 115, 115, 1)','rgba(0, 255, 0, 1)'],
            borderWidth: 1
        }]
    },
    options: {
    title: {
      display: true,
      text: 'TypoSquatting por estado.'
    }
  }
});