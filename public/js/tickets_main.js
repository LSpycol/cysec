function getData(tickets, campo, valor){
		var i = 0;
		var contador = 0;
		while(tickets[i] != null){
			if(tickets[i][campo] == valor && tickets[i]["status"] != "Cerrado") contador++;
			i++;
		}
		return contador;
	}

function getHistorico(tickets, arrayFechas, estado){
//obtiene todos los tickets que estén dentro del array de fechas
	var historico = new Array();
	var i = 0;
	while(arrayFechas[i] != null){
		var j = 0;
		var contador = 0;
		while(tickets[j] != null){
			if(estado == 'Cerrado'){
				if(tickets[j]['closed'] == arrayFechas[i]) contador++;
			} 
			else{
				if(tickets[j]['creation'] == arrayFechas[i]) contador++;
			} 
			j++;
		}
		historico.push(contador);
		i++;
	}
	return historico;
}

function formatoFecha(date){
//formato a la  fecha
    var fecha = new Date(date);
    var dd = fecha.getDate();
    var mm = fecha.getMonth()+1;
    var yyyy = fecha.getFullYear();
    if(dd<10) dd = '0'+dd;
    if(mm<10) mm = '0'+mm;
    return yyyy + '-' + mm + '-' + dd
}


function getAllDays(from) {
//obtiene todas las fechas hasta ahora
  var dates = [];
  var fechaActual = new Date(from);
  var hoy = formatoFecha(new Date());
  while(adaptado != hoy){
    fechaActual = new Date(fechaActual);
    var adaptado = formatoFecha(fechaActual);
    dates.push(adaptado);
    fechaActual = fechaActual.setDate(fechaActual.getDate() + 1);
  }

  return dates;
};          

function getAbiertos(tickets, arrayFechas){
//obtiene todos los tickets que abiertos por días
    var abiertos = new Array();
    var i = 0;
    while(arrayFechas[i] != null){
        var j = 0;
        var contador = 0;
        while(tickets[j] != null){
            if(tickets[j]['creation'] <= arrayFechas[i]) contador++;
            if(tickets[j]['closed'] <= arrayFechas[i]) contador--; 
            j++;
        }
        abiertos.push(contador);
        i++;
    }
    return abiertos;
}



/*************************************/                                                                                              
/*************************************/ 
/*************************************/ 
var desde = window.location.search.substr(1);
if(desde != ''){
	desde = desde.replace("from=", "");
}else{
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth();
    var yyyy = today.getFullYear();
    if(dd<10) dd = '0'+dd;
    if(mm<10) mm = '0'+mm;
    if(mm==0){
        mm = '12';
        yyyy--;
    } 
    desde = yyyy + '-' + mm + '-' + dd
}


//array con todas las fechas hasta ahora
var arrayFechas = getAllDays(desde);

var creados = getHistorico(tickets, arrayFechas, 'NoCerrado');
var cerrados = getHistorico(tickets, arrayFechas, 'Cerrado');

var abiertos = getAbiertos(tickets, arrayFechas);

var totalCriticos =getData(tickets, "priority", "Crítico");
var totalAltos =getData(tickets, "priority", "Alto");;
var totalMedios =getData(tickets, "priority", "Medio");;
var totalBajos=getData(tickets, "priority", "Bajo");;
var total = totalCriticos + totalAltos + totalMedios + totalBajos;

document.getElementById("openTickets").innerHTML ="Tickets abiertos: " + total;
document.getElementById("historico").innerHTML ="Histórico de tickets desde " + desde;

if(total == 0) total = total + 1;

var criticos = Math.round(totalCriticos / total * 99);
var altos = Math.round(totalAltos / total * 99);
var medios = Math.round(totalMedios / total * 99);
var bajos = Math.round(totalBajos / total * 99);

	/* barras de tickets*/
function rellenarBarra(barra, tope){
    if(tope == 0){
        document.getElementById(barra).remove();
        return 0;
    }
	var elem = document.getElementById(barra);   
	var width = 0;
	var id = setInterval(frame, 2);
	function frame() {
	if (width >= tope) {
	  clearInterval(id);
	} else {
	  width = width + 1; 
	  elem.style.width = Math.round(width) + '%'; 
	  elem.innerHTML = Math.round(width) * 1  + '%';
	}
	}
}

rellenarBarra('barCritico', criticos);
rellenarBarra('barAlto', altos);
rellenarBarra('barMedio', medios);
rellenarBarra('barBajo', bajos);


//obtenemos los  tipos de tickets
var types = [];
var i = 0;
while(tickets[i] != null){
    types.push(tickets[i]['type']);
    i++;
}
//quitamos duplicados
var types_u = [];
$.each(types, function(i, el){
    if($.inArray(el, types_u) === -1) types_u.push(el);
});

var values = [];
var colores1 = [];
var colores2 = [];
var j = 0;
while(types_u[j] != null){
    values.push(getData(tickets, "type", types_u[j]));
    var color = 'rgba(' + (Math.floor(Math.random() * 255) + 1) + ', ' + (Math.floor(Math.random() * 255) + 1) + ', ' + (Math.floor(Math.random() * 255) + 1);
    colores1.push(color + ', 0.2)');
    colores2.push(color + ', 1)');
    j++;
}


/* grafico 1*/

var ctx = document.getElementById("plot1");
var grafico1 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Crítico", "Alto", "Medio", "Bajo"],
        datasets: [{
        	label:"Tickets por prioridad",
            data: [totalCriticos,totalAltos,totalMedios,totalBajos],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 128, 0, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(255, 128, 0, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
    	title: {
	      display: true,
	      text: 'Tickets abiertos por criticidad.'
	    },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

/* grafico 2*/

var ctx = document.getElementById("plot2");
var grafico2 = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: types_u,
        datasets: [{
            data: values,
            backgroundColor: colores1,
            borderColor: colores2,
            borderWidth: 1
        }]
    },
    options: {
    title: {
      display: true,
      text: 'Tickets abiertos por tipo.'
    }
  }
});


	/* grafico 3*/
var ctx = document.getElementById("plot3");
var grafico3 = new Chart(ctx, {
	type: 'line',
  data: {
    labels: arrayFechas,
    datasets: [{ 
        data: creados,
        label: "Creados",
        borderColor: "rgba(75, 192, 192, 1)",
        fill: true,
        backgroundColor:"rgba(75, 192, 192, 0.2)"
      }, { 
        data: cerrados,
        label: "Cerrados",
        borderColor: "rgba(255, 99, 132, 1)",
        fill: true,
        backgroundColor:"rgba(255, 99, 132, 0.2)"
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Histórico de creación y cerrado de tickets.'
    }
  }
});


    /* grafico 4*/
var ctx = document.getElementById("plot4");
var grafico3 = new Chart(ctx, {
    type: 'line',
  data: {
    labels: arrayFechas,
    datasets: [{ 
        data: abiertos,
        label: "Abiertos",
        borderColor: "rgba(51, 51, 255, 1)",
        fill: false,
        backgroundColor:"rgba(51, 51, 255, 0.2)"
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Histórico de tickets abiertos'
    }
  }
});