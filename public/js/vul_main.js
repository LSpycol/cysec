
var sites = [];
var assets = [];


var i = 0;
while(occurrences[i] != null){
    var j = 0;
    var encontrado = 0;
    //sites con sus vulnerabilidades
    while(sites[j] != occurrences[i]['site'] && sites[j] != null){
        if(sites[j]['site'] == occurrences[i]['site']){
            encontrado = 1;
            switch(occurrences[i]['status']){
                case 'Pendiente':
                            sites[j]['Pendiente']++;
                            break;
                case 'Activa':
                            sites[j]['Activa']++;
                            break;
                case 'Asumida':
                            sites[j]['Asumida']++;
                            break;
                case 'Corregida':
                            sites[j]['Corregida']++;
                            break;
                case 'Descartada':
                            sites[j]['Descartada']++;
                            break;
            }
        }
        j++;
    }

    if(!encontrado){
        var s = {
            "site":  occurrences[i]['site'],
            "Pendiente":  0,
            "Activa":  0,
            "Asumida":  0,
            "Corregida":  0,
            "Descartada":  0
        };
        sites.push(s);
    }

    //assets
    if(!assets.includes(occurrences[i]['ip'])){
        assets.push(occurrences[i]['ip']);
    }

    i++;
}

$("#total_sites").text(sites.length);
$("#total_assets").text(assets.length);
$("#total_vulnerabilidades").text(i);

var sites_plot = [];

var pendiente_plot = [];
var activa_plot = [];
var asumida_plot = [];
var corregida_plot = [];
var descartada_plot = [];

var pendiente_color = [];
var activa_color = [];
var asumida_color = [];
var corregida_color = [];
var descartada_color = [];

var datasets = [];


i = 0;
while(sites[i] != null){
    sites_plot.push(sites[i]['site']);
    pendiente_plot.push(sites[i]['Pendiente']);
    activa_plot.push(sites[i]['Activa']);
    asumida_plot.push(sites[i]['Asumida']);
    corregida_plot.push(sites[i]['Corregida']);
    descartada_plot.push(sites[i]['Descartada']);

    pendiente_color.push('rgba(255, 255, 102, 1)');
    activa_color.push('rgba(255, 112, 77, 1)');
    asumida_color.push('rgba(102, 140, 255, 1)');
    corregida_color.push('rgba(102, 255, 102, 1)');
    descartada_color.push('rgba(140, 140, 140, 1)');

    i++;
}

var dataset_pendiente = {
            label:"Pendientes",
            data: pendiente_plot,
            backgroundColor: pendiente_color};

var dataset_activa = {
            label:"Activas",
            data: activa_plot,
            backgroundColor: activa_color};
var dataset_asumida = {
            label:"Asumidas",
            data: asumida_plot,
            backgroundColor: asumida_color};
var dataset_corregida = {
            label:"Corregidas",
            data: corregida_plot,
            backgroundColor: corregida_color};
var dataset_descartada = {
            label:"Descartadas",
            data: descartada_plot,
            backgroundColor: descartada_color};

datasets.push(dataset_pendiente);
datasets.push(dataset_activa);
datasets.push(dataset_asumida);
datasets.push(dataset_corregida);
datasets.push(dataset_descartada);





var ctx = document.getElementById("plot1");
var grafico1 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: sites_plot,
        datasets: datasets
    },
    options: {
        title: {
          display: true,
          text: 'Vulnerabilidades por Site.'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

