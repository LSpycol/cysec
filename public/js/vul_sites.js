function vul_create(ip, severity, vul){
    var i = 0;
    while(vul[i]['ip'] != ip){
        i++;
    }

    if(severity < 4){
        vul[i]["bajo"]++;
    } else{
        if(severity < 7){
            vul[i]["medio"]++;
        }
        else{
            vul[i]["alto"]++;
        }
    }

    return vul;
}




var ips = [];
var vul = [];

var pendiente = 0;
var activa = 0;
var asumida = 0;
var corregida = 0;
var descartada = 0;

var i = 0;
while(ocurrences[i] != null){
    if(ips.includes(ocurrences[i]['ip'])){
        vul = vul_create(ocurrences[i]['ip'], ocurrences[i]['severity'], vul);
    }else{
        ips.push(ocurrences[i]['ip']);

        var v = {
            "ip":  ocurrences[i]['ip'],
            "bajo":  0,
            "medio":  0,
            "alto":  0 
        };
        vul.push(v);
        vul = vul_create(ocurrences[i]['ip'], ocurrences[i]['severity'], vul);
    }

    switch(ocurrences[i]['status']){
        case 'Pendiente': pendiente++;
                        break;
        case 'Activa': activa++;
                        break;
        case 'Asumida': asumida++;
                        break;
        case 'Corregida': corregida++;
                            break;
        case 'Descartada': descartada++;
                            break;
    }


    i++;
}

$("#assets").text(ips.length);
$("#ocurrencias").text(i);

var j = 0;
while(vul[j] != null){

    var bajo = vul[j]['bajo'];
    var medio = vul[j]['medio'];
    var alto = vul[j]['alto'];
    var total = bajo + medio + alto;

    var div = '<div class="w3-col s12 m12 l12 w3-padding"><div class="w3-col s4 m4 l3">' + vul[j]['ip'] + '</div><div class="w3-col s8 m8 l9 w3-light-grey">';
    if(bajo > 0) div += '<div class="w3-container w3-green w3-center w3-col" style="width: ' + ((bajo/total*100) - 0.1) + '%">' + bajo + '</div>';
    if(medio > 0) div += '<div class="w3-container w3-yellow w3-center w3-col" style="width: ' + (medio/total*100) + '%">' + medio + '</div>' ;
    if(alto > 0) div +='<div class="w3-container w3-red w3-center w3-col" style="width: ' + (alto/total*100) + '%">' + alto + '</div>';

    div += "</div></div>";

    $("#assets_vul").append(div);
    //alert(vul[j]['bajo']);
    j++;
}

$("#pendiente").text("Pendientes: " + pendiente);
$("#activa").text("Activas: " + activa);
$("#asumida").text("Asumidas: " + asumida);
$("#corregida").text("Corregidas: " + corregida);
$("#descartada").text("Descartadas: " + descartada);