function formatoFecha(date) {
  var day = date.getDate();
  var month = date.getMonth()+1;
  var year = date.getFullYear();

  if(day<10) day = '0'+day;
  if(month<10) month = '0'+month;
  if(month==0){
        month = '12';
        year--;
  }

  

  return year + "-" + month + "-" + day;
}

function getAllDays(from) {
//obtiene todas las fechas hasta ahora
  var dates = [];
  var fechaActual = new Date(from);
  var hoy = formatoFecha(new Date());
  while(adaptado != hoy){
    fechaActual = new Date(fechaActual);
    var adaptado = formatoFecha(fechaActual);
    dates.push(adaptado);
    fechaActual = fechaActual.setDate(fechaActual.getDate() + 1);
  }

  return dates;
};  


var desde=new Date;
desde.setDate(desde.getDate()-7);

var arrayFechas = getAllDays(desde);

var cantidad = [0,0,0,0,0,0,0,0];
var i = 0;

while(notificaciones[i] != null){
  var fecha = notificaciones[i]['created_at'].substring(0, 10);
  var j = 0;
  while(arrayFechas[j] != null){
    if(arrayFechas[j] == fecha){
      cantidad[j]++;
    } 
    j++
  }
  i++;
}



/* grafico 1*/

var ctx = document.getElementById("plot1");
var grafico1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: arrayFechas,
        datasets: [{
          label:"Notificaciones por día",
            data: cantidad,
            backgroundColor: [
                'rgba(128, 255, 128, 0.2)',
            ],
            borderColor: [
                'rgba(128, 255, 128,1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      title: {
        display: true,
        text: 'Notificaciones por día.'
      },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});