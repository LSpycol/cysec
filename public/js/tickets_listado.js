//  modal
var modal = document.getElementById('modalFiltroAvanzado');

// Cuando se hace click fuera, se oculta el modal
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

$(".checkbox_ticket").click(function(){
	//comprueba que no haya ninguno checkeado
	if ($('.checkbox_ticket:checked').length == 0) {

	    $("#boton_editar").addClass("w3-hide");
	    $("#boton_editar").removeClass("w3-animate-opacity");

	    $("#boton_actualizar").addClass("w3-hide");
	    $("#boton_actualizar").removeClass("w3-animate-opacity");

	    $("#boton_3").addClass("w3-hide");
	    $("#boton_3").removeClass("w3-animate-opacity");

	    $("#checkbox_ticket_all").prop('checked', false);
	}else{
		$("#boton_editar").removeClass("w3-hide");
	    $("#boton_editar").addClass("w3-animate-opacity");

	    $("#boton_actualizar").removeClass("w3-hide");
	    $("#boton_actualizar").addClass("w3-animate-opacity");

	    $("#boton_3").removeClass("w3-hide");
	    $("#boton_3").addClass("w3-animate-opacity");

	}
    
});

$("#checkbox_ticket_all").click(function(){
	if($("#checkbox_ticket_all").prop('checked') == true){
	    $(".checkbox_ticket").prop('checked', true);

	    $("#boton_editar").removeClass("w3-hide");
	    $("#boton_editar").addClass("w3-animate-opacity");

	    $("#boton_actualizar").removeClass("w3-hide");
	    $("#boton_actualizar").addClass("w3-animate-opacity");

	    $("#boton_3").removeClass("w3-hide");
	    $("#boton_3").addClass("w3-animate-opacity");

	}else{
		$(".checkbox_ticket").prop('checked', false);

		$("#boton_editar").addClass("w3-hide");
	    $("#boton_editar").removeClass("w3-animate-opacity");

	    $("#boton_actualizar").addClass("w3-hide");
	    $("#boton_actualizar").removeClass("w3-animate-opacity");

	    $("#boton_3").addClass("w3-hide");
	    $("#boton_3").removeClass("w3-animate-opacity");

	}
    
});


