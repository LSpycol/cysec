<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class permissionModel extends Model
{
    //lista de permisos generales, se usará para los  permisos por cada cliente.
    private $permisos = array(
        'ClientView', 'TicketView', 'TicketUpdate',  'TicketEdit',  'TicketCreate',
        'SsggView',  'SsggEdit', 'SsggCreate', 'SsggIpView', 'SsggIpEdit', 'SsggEngine', 
        'VulView', 'VulActivate', 'VulEdit', 'VulDelete',
        'CfView', 'CfTypo', 'CfTypoEdit',
        'VdView', 'VdNews', 'VdNotifications',
        'AnvdView',
    );


    /* crea un nuevo permiso  */
    public function nuevoPermiso($permisoStr){
        $permiso = Permission::create(['name' => $permisoStr]);
        $rol = Role::find(1);
        $rol->givePermissionTo($permiso);
    }

    //crea la lista de permisos generales
    public function crearListaInicial(){
        $rol = Role::create(['name' => 'permissionList']);


        foreach ($this->permisos as $p) {
            $permiso = Permission::create(['name' => $p]);
            $rol->givePermissionTo($permiso);
        }
    }

    //añade a la lista de todos los permisos los de un nuevo cliente
    public function crearPermisos($idCliente){
    	
    	$rol = Role::find(1);

    	if($rol == null){
           $rol = Role::create(['name' => 'permissionListAll']);
           $this->crearListaInicial();
        } 

    	foreach ($this->permisos as $p) {
            $permiso = Permission::create(['name' => $p . $idCliente]);
            $rol->givePermissionTo($permiso);
        }


    }

}
