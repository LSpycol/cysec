<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cf_domains extends Model
{
    protected $table = 'cf_domains';
    protected $fillable = array('domain', 'client');
}
