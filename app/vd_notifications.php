<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vd_notifications extends Model
{
    protected $table = 'vd_notifications';
    protected $fillable = array('type', 'title', 'source', 'link', 'tool', 'notificated_at', 'client');
}
