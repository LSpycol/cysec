<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vd_words extends Model
{
    protected $table = 'vd_words';
    protected $fillable = array('client', 'word');
}
