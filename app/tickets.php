<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tickets extends Model
{
    protected $fillable = array('title', 'creation', 'type', 'status', 'priority', 'generated', 'assigned_to_user','assigned_to_group', 'client', 'solution',  'closed');

    public function getPriority(){
    	if($this->priority == 'Crítico'){
    		$color = 'w3-red';
    	}
    	if($this->priority == 'Alto'){
    		$color = 'w3-deep-orange';
    	}
    	if($this->priority == 'Medio'){
    		$color = 'w3-yellow';
    	}
    	if($this->priority == 'Bajo'){
    		$color = 'w3-green';
    	}

    	return $color;
    }

}
