<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ssgg_services extends Model
{
    protected $table = 'ssgg_services';
    protected $fillable = array('name', 'info', 'service', 'asset', 'client', 'status');
}
