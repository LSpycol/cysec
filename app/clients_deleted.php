<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clients_deleted extends Model
{
	protected $table = 'clients_deleted';
    protected $fillable = array('name', 'ip', 'last_id');
}
