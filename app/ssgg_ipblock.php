<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ssgg_ipblock extends Model
{
    protected $table = 'ssgg_ipblock';
    protected $fillable = array('ip', 'blocked_at', 'reason', 'revision', 'client');
}
