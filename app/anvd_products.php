<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class anvd_products extends Model
{
    protected $table = 'anvd_products';
    protected $fillable = array('vendor', 'product', 'email', 'client');
}
