<?php

namespace App\Exports;

use App\ssgg_ipblock;
use App\vul_occurrences;
use App\tickets;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class ModalExport implements FromQuery
{
	use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($toExport)
    {
        $this->toExport = $toExport;
    }

    
    public function query()
    {
        return $this->toExport;
    }
}
