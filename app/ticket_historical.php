<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket_historical extends Model
{
    protected $table = 'ticket_historical';
    protected $fillable = array('id_ticket', 'text', 'user', 'file', 'changes_before', 'changes_after', 'actualization');
}
