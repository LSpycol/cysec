<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ssgg_assets extends Model
{
    protected $table = 'ssgg_assets';
    protected $fillable = array('name', 'ip', 'info', 'client', 'status');
}
