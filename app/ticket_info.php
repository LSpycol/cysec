<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket_info extends Model
{
    protected $table = 'ticket_info';
    protected $fillable = array('id_ticket', 'information_1');
}
