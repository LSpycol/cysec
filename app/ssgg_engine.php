<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ssgg_engine extends Model
{
    protected $table = 'ssgg_engine';
    protected $fillable = array('regexpr', 'type_close',  'autoassign_group', 'client', 'position');
}
