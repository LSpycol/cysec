<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\clients;
use \App\User;
use \App\groups;
use \App\ssgg_assets;
use \App\ssgg_services;
use \App\ssgg_ipblock;
use \App\ssgg_engine;
use \App\Exports\ModalExport;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ssggController extends Controller
{
    /* comprueba que estás logeado, si no, te devuelvea al login */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }


    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        /* ***** permisos disponibles 
        'SsggView',  'SsggEdit', 'SsggCreate', 'SsggIpView', 'SsggIpEdit', 'SsggEngine'
        ****** */
        $user = Auth::user(); 
        if($user->role != 'Administrador' && !$user->can($permiso . $user->client)){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }

        //Si el objeto pasado no es el del cliente seleccionado
        if(isset($objeto)){
            if($objeto->client != Auth::user()->client){
                return abort(403); //devolvemos la vista de permisos insuficientes
            }
        }
        
    }


    /* Enlace a la vista principal*/
    public function main(){
        $this->verifyPermission('SsggView'); //primero verificamos los permisos
        Log::info('[SSGG] Acceso a Main. User:'  . Auth::user()->email . '. Cliente: ' . Auth::user()->client);
    	return view('ssgg/main');
    }

    /* Apartado de monitorización dentro de la vista principal main */
    public function monitorizacion(){
        $this->verifyPermission('SsggView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $assets = json_encode(ssgg_assets::query()->where('client', '=', $clientUserSelected)->get());
        $services = json_encode(ssgg_services::query()->where('client', '=', $clientUserSelected)->get());
        return view('ssgg/monitorizacion', compact('assets', 'services'));   
    }

    /* Vista de crear un nuevo asset */
    public function nuevo_asset(){
        $this->verifyPermission('SsggCreate'); //primero verificamos los permisos
        return view('ssgg/nuevo_asset');
    }

    /* crear un nuevo asset */
    public function crear_asset(){
        $this->verifyPermission('SsggCreate'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $datos = request()->all();

        $asset = ssgg_assets::create([
            'name' => $datos['name'],
            'ip' => $datos['ip'],
            'status' => 'Down',
            'info' => $datos['info'],
            'client' => $clientUserSelected,
            'created_at' => now()
        ]);

        Log::info('[CF] Asset creado:' . json_encode($asset) . ' User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
        return redirect()
            ->route('ssgg.main')
            ->with('success', "Se ha creado el asset: $asset->name");

    }


    /* Vista de ver un nuevo asset */
    public function ver_asset(ssgg_assets $asset){
        $this->verifyPermission('SsggView', $asset); //primero verificamos los permisos
        $services = ssgg_services::query()->where('asset', $asset->id)->get();
        return view('ssgg/ver_asset', compact('asset', 'services'));
    }


    /* Vista de crear un nuevo servicio */
    public function nuevo_servicio($asset){
        $this->verifyPermission('SsggCreate'); //primero verificamos los permisos
        return view('ssgg/nuevo_servicio', compact('asset'));
    }


    /* crear un nuevo servicio */
    public function crear_servicio(){
        $this->verifyPermission('SsggCreate'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $datos = request()->all();

         $servicio = ssgg_services::create([
            'name' => $datos['name'],
            'service' => $datos['service'],
            'status' => 'Down',
            'info' => $datos['info'],
            'client' => $clientUserSelected,
            'asset' => $datos['asset'],
            'created_at' => now()
        ]);

         Log::info('[CF] Servicio creado:' . json_encode($servicio) . ' User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
         return redirect()
            ->route('ssgg.ver_asset', $datos['asset'])
            ->with('success', "Se ha creado el servicio: $servicio->name");

    }

     /* Vista de ver un nuevo asset */
    public function editar_asset(ssgg_assets $asset){
        $this->verifyPermission('SsggEdit', $asset); //primero verificamos los permisos
        $services = ssgg_services::query()->where('asset', $asset->id)->get();
        return view('ssgg/editar_asset', compact('asset', 'services'));
    }

    /* Edita un asset */ 
    public function edicion_asset(ssgg_assets $asset){
        $this->verifyPermission('SsggEdit', $asset); //primero verificamos los permisos
        $form = request()->all();

        
        $asset->name = $form['name'];
        $asset->ip = $form['ip'];
        $asset->info = $form['info'];

        $asset->save();

        Log::info('[CF] Asset editado:' . json_encode($asset) . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('ssgg.ver_asset', $asset->id)
            ->with('success', "Se editado el asset");
    }

    /* Elimina un Asset */
    public function eliminar_asset(ssgg_assets $asset){
        $this->verifyPermission('SsggEdit', $asset); //primero verificamos los permisos
        $services = ssgg_services::query()->where('asset', $asset->id)->get();

        if(count($services) === 0){
            $asset->delete();

            Log::info('[CF] Asset eliminado:' . $asset->name . ' User:'  . Auth::user()->email);
            return redirect()
                ->route('ssgg.main', $asset->id)
                ->with('success', "Se ha eliminado el asset.");

        }else{
            return redirect()
                ->route('ssgg.ver_asset', $asset->id)
                ->with('errors', "No se puede eliminar el asset ya que tiene servicios monitorizados.");
        }

    }


    /* Elimina un Asset */
    public function eliminar_servicio(ssgg_services $service){
        $this->verifyPermission('SsggEdit', $service); //primero verificamos los permisos
        $service->delete();
        Log::info('[CF] Servicio eliminado:' . $service->name . ' User:'  . Auth::user()->email);
         return redirect()
            ->route('ssgg.ver_asset', $service->asset)
            ->with('success', "Se ha eliminado el servicio.");
    }


    /* ********************** */

    /* Enlace a la vista de Ips Bloqueadas */
    public function ver_ipblock(){
        $this->verifyPermission('SsggIpView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $ips = ssgg_ipblock::query()->where('client', '=', $clientUserSelected)->get();
        return view('ssgg/ipBlock', compact('ips'));  
    }

    /* Enlace a la vista de editar Ips Bloqueadas */
    public function editar_ipblock(ssgg_ipblock $ip){
        $this->verifyPermission('SsggIpEdit', $ip); //primero verificamos los permisos
        return view('ssgg/editar_ipBlock', compact('ip'));
    }

    /* Elimina una Ips Bloqueada */
    public function eliminar_ipblock(ssgg_ipblock $ip){
        $this->verifyPermission('SsggIpEdit', $ip); //primero verificamos los permisos
        $ip->delete();
        Log::info('[CF] IpBlock creada:' . $ip->ip . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('ssgg.ipBlock')
            ->with('success', "Se ha eliminado la IP: " . $ip->ip .".");
    }

    /* Enlace a la vista de crear una Ips Bloqueada */
    public function crear_ipblock(){
        $this->verifyPermission('SsggIpEdit'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $form = request()->all();
        unset($form['_token']);

        $ip = ssgg_ipblock::create([
            'ip' => $form['ip'],
            'client' => $clientUserSelected,
            'blocked_at' => $form['blocked_at'],
            'reason' => $form['reason'],
            
            'created_at' => now()
        ]);

        Log::info('[CF] Ipblock creada:' . json_encode($ip) . ' User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
        return redirect()
            ->route('ssgg.ipBlock')
            ->with('success', "Se ha añadido la IP: " . $ip->ip .".");

    }

    /* edita una Ips Bloqueada */
    public function edicion_ipblock(ssgg_ipblock $ip){
        $this->verifyPermission('SsggIpEdit', $ip); //primero verificamos los permisos
        $form = request()->all();
        
        if($form['revision']  == null) unset($form['revision']);
        if($form['blocked_at']  == null) unset($form['blocked_at']);
        unset($form['_token']);
        unset($form['_method']);

        $ip->update($form);

        Log::info('[CF] Ipblock editada:' . json_encode($ip) . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('ssgg.ipBlock')
            ->with('success', "Se ha modificado la IP: " . $ip->ip .".");

    }

    /* enlace a la vista de Engine */
    public function engine(){
        $this->verifyPermission('SsggEngine'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $engines = ssgg_engine::query()
                    ->where('client', $clientUserSelected)->orderBy('position', 'asc')
                    ->leftjoin('groups', 'ssgg_engine.autoassign_group', '=',  'groups.id')
                    ->get();
        return view('ssgg/engine', compact('engines'));
    }

    /* Baja el engine una posición */
    public function bajar_engine(ssgg_engine $engine){
        $this->verifyPermission('SsggEngine', $engine); //primero verificamos los permisos

        $engine2 = ssgg_engine::query()->where('position',$engine->position + 1)->get();

        if(count($engine2) == 0){
            return redirect()
                ->route('ssgg.engine')
                ->with('errors', "El engine no se puede bajar de posición.");
        }
        $engine2 = $engine2[0];
        $engine2->position = $engine2->position - 1;
        $engine->position = $engine->position + 1;

        $engine->save();
        $engine2->save();

        return redirect()
            ->route('ssgg.engine')
            ->with('success', "Se ha cambiado de posición el engine.");
    }

    /* Sube el engine una posición */
    public function subir_engine(ssgg_engine $engine){
        $this->verifyPermission('SsggEngine', $engine); //primero verificamos los permisos
        if($engine->position != 1){
            $engine2 = ssgg_engine::query()->where('position',$engine->position - 1)->get()[0];
            $engine2->position = $engine2->position + 1;
            $engine->position = $engine->position - 1;

            $engine->save();
            $engine2->save();

            return redirect()
                ->route('ssgg.engine')
                ->with('success', "Se ha cambiado de posición el engine.");

        }
         return redirect()
                ->route('ssgg.engine')
                ->with('errors', "El engine no se puede subir de posición.");
    }


    /* Elilmina un engine */
    public function eliminar_engine(ssgg_engine $engine){
        $this->verifyPermission('SsggEngine', $engine); //primero verificamos los permisos
        $engine->delete();
        Log::info('[CF] Engine eliminado:' . $engine->regexp . ' User:'  . Auth::user()->email);
        return redirect()
                ->route('ssgg.engine')
                ->with('success', "Se ha eliminado el engine.");
    }

    /* enlace a la vista de nuevo un engine */
    public function nuevo_engine(){
        $this->verifyPermission('SsggEngine'); //primero verificamos los permisos
        $groups = groups::all();
        return view('ssgg/nuevo_engine', compact('groups'));
    }

    /* Crea un nuevo engine */
    public function crear_engine(){
        $this->verifyPermission('SsggEngine'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $datos = request()->all();
        //obtenemos el engine con la ultima posicion
        $last_engine = ssgg_engine::query()->where('client', $clientUserSelected)->orderBy('position', 'desc')->first();

        $engine = ssgg_engine::create([
            'regexpr' => $datos['regexpr'],
            'autoassign_group' => $datos['autoassign_group'],
            'type_close' => $datos['type_close'],
            'position' => $last_engine['position'] + 1,
            'client' => $clientUserSelected,
            'created_at' => now()
        ]);

        Log::info('[CF] Engine creado:' . json_encode($engine) . ' User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
        return redirect()
                ->route('ssgg.engine')
                ->with('success', "Se ha creado el engine.");

    }

    /* enlace a la vista de editar un engine */
    public function editar_engine(ssgg_engine $engine){
        $this->verifyPermission('SsggEngine', $engine); //primero verificamos los permisos
        $groups = groups::all();
        return view('ssgg/editar_engine', compact('engine', 'groups'));
    }

    /* edita un engine */
    public function edicion_engine(ssgg_engine $engine){
        $this->verifyPermission('SsggEngine', $engine); //primero verificamos los permisos
        $datos = request()->all();

        $engine->regexpr = $datos['regexpr'];
        $engine->autoassign_group = $datos['autoassign_group'];
        $engine->type_close = $datos['type_close'];
        $engine->save();
        
        Log::info('[CF] Engine editado:' . json_encode($engine) . ' User:'  . Auth::user()->email);
        return redirect()
                ->route('ssgg.engine')
                ->with('success', "Se ha editado el engine.");
    }


    /* exporta las ips bloqueadas */
    public function exportar_ipBlock(){
        $this->verifyPermission('SsggIpView');
        $clientUserSelected = Auth::user()->client;

        $ips = ssgg_ipblock::query()->where('client', '=', $clientUserSelected);
        return (new ModalExport($ips))->download('ips_bloqueadas' . Auth::user()->clientName . '.csv');
    }


}
