<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Passwords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use \App\clients;
use \App\clients_deleted;
use \App\User;

use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \App\permissionModel;

class clientsPanelController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission(){
        if(Auth::user()->role != 'Administrador'){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }
    }



     /***************CLIENTES******************/
    /* Enlace a la vista de clientes */
    public function clientes(){
        $this->verifyPermission(); //primero verificamos los permisos
    	$clients = clients::all();

        Log::info('[Control] Acceso a Clientes. User:'  . Auth::user()->email);
    	return view('controlPanel/clientes', compact('clients'));
    }

    /* Enlace a la vista de editar un cliente */
    public function editar_cliente($client){
        $this->verifyPermission(); //primero verificamos los permisos
        $client = clients::findOrFail($client);
        return view('controlPanel/editar_cliente', compact('client'));
    }

    /* Editar un cliente */
     public function edicion_cliente(clients $client){
        $this->verifyPermission(); //primero verificamos los permisos
        $datos = request()->all();

        $client->name = $datos['name'];
        $client->ip = $datos['ip'];
        $client->update();
        
        Log::info('[Control] Cliente editado:' . json_encode($client) . '. User:'  . Auth::user()->email);
        return redirect()
            ->route('control.clientes')
            ->with('success', "Se ha editado el cliente: $client->id");
    }

    /* Eliminar un cliente */
    public function eliminar_cliente(clients $client){
        $this->verifyPermission(); //primero verificamos los permisos
        clients_deleted::create([
            'name' => $client->name,
            'ip' => $client->ip,
            'last_id' => $client->id
        ]);

        //los usuarios que tengan ese cliente seleccionamo, se lo reseteamoos
        DB::table('users')->where('client' ,'=', $client->id)->update(['client' => 0]);
        Log::info('[Control] Cliente eliminado:' . $client->id . '. User:'  . Auth::user()->email);
        $client->delete();
        return redirect()
            ->route('control.clientes')
            ->with('success', "Se ha eliminado el cliente: $client->name");
    }

    /* Enlace a la vista de crear un nuevo cliente */
    public function nuevo_cliente(){
        $this->verifyPermission(); //primero verificamos los permisos
        return view('controlPanel/nuevo_cliente');
    }


    /* Crear un nuevo cliente */
    public function crear_cliente(){
        $this->verifyPermission(); //primero verificamos los permisos
    	$datos = request()->all();
    	$client = clients::create([
    		'name' => $datos['name'],
    		'ip' => $datos['ip']
    	]);

        $permisos = new permissionModel;
        $permisos->crearPermisos($client->id);

        Log::info('[Control] Cliente creado:' . json_encode($client) . '. User:'  . Auth::user()->email);
    	return redirect()
            ->back()
            ->with('success', "Se ha creado el cliente con id: $client->id");
    }

}
