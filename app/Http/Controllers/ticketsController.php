<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\tickets;
use \App\ticket_info;
use \App\ticket_historical;
use \App\type_ticket;
use \App\clients;
use \App\User;
use \App\groups;
use \App\Exports\ModalExport;
use Illuminate\Support\Facades\DB;
use Mail;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maatwebsite\Excel\Facades\Excel;

class ticketsController extends Controller
{
    /* comprueba que estás logeado, si no, te devuelvea al login */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }
   
    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        /* ***** permisos disponibles para los tickets
        'ClientView', 'TicketView', 'TicketUpdate',  'TicketEdit',  'TicketCreate'
        ****** */
        $user = Auth::user(); 
        if($user->role != 'Administrador' && !$user->can($permiso . $user->client)){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }

        //Si el objeto pasado no es el del cliente seleccionado
        if(isset($objeto)){
            if($objeto->client != Auth::user()->client){
                return abort(403); //devolvemos la vista de permisos insuficientes
            }
        }
        
    }

    /* - Enlace a la página principal de los tickets- */
    public function main(){
        $this->verifyPermission('TicketView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $fecha = request()->from;

        if ($fecha == null || $fecha > now()) {
            $fecha = now()->subMonths(1);  
        }

        $ticketsA = tickets::query()->
                    selectRaw('type,status,priority,SUBSTR(creation, 1, 10) as creation,SUBSTR(closed, 1, 10) as closed')
                    ->where([
                    ['created_at',  '>=', $fecha],
                    ['client', '=', $clientUserSelected], 
                    ])->orWhere([
                        ['status', '!=', 'Cerrado'],
                        ['client', '=', $clientUserSelected],
                    ])->get();
        $tickets = json_encode($ticketsA->toArray());

        Log::info('[Tickets] Acceso a Main. User:'  . Auth::user()->email . '. Cliente: ' . Auth::user()->client);
        return view('tickets/main', compact('tickets'));
    }

    /* Auxiliar que devuelve el listado de tickets según los filtros que se le pasen */
    public function listado_aux($filtros){
        $clientUserSelected = Auth::user()->client;
        if($filtros != null){
            if(isset($title_op))$title_op = $filtros['title_op'];
            unset($filtros['_token']);
            unset($filtros['title_op']);


            foreach ($filtros as $key => $value) {
                if(!isset($filtros[$key])) unset($filtros[$key]);
            }

            $condiciones = array();
            $condiciones2 = array();
            foreach ($filtros as $key => $value){
                switch ($key) {
                    case 'title':
                        if($title_op = 'contiene'){
                            array_push($condiciones, array($key, 'like', '%' . $value . '%'));
                        }else{
                           array_push($condiciones, array($key, $title_op, $value)); 
                        }
                        break;
                    case 'open_from':
                        array_push($condiciones, array('creation', '>', $value . ' 00:00:00'));
                        break;
                    case 'open_to':
                        array_push($condiciones, array('creation', '<', $value . ' 23:59:59'));
                        break;
                    case 'closed_from':
                        array_push($condiciones, array('closed', '>', $value . ' 00:00:00'));
                        break;
                    case 'closed_to':
                        array_push($condiciones, array('closed', '<', $value . ' 23:59:59'));
                        break;
                    
                    default:
                       $condiciones2[$key] = explode(',', $value);
                        break;
                }
            }

            array_push($condiciones, array('client', '=', $clientUserSelected));

            $tickets = tickets::query();
            $tickets->where($condiciones);
            //para cuando le pasamos un array en una condicion, necesitamos hacer un whereIn de cada 
            foreach ($condiciones2 as $key => $value) {
                $tickets->whereIn($key, $value);
            }
            $tickets = $tickets;

        }else{
            $tickets = tickets::where([
                ['status',  '!=' ,'Cerrado'],
                ['client', '=', $clientUserSelected] 
            ]);
        }

        return $tickets;
    }

     /* - Enlace a la vista de listado de tickets con o sin filtro- */
    public function listado(){
        $this->verifyPermission('TicketView'); //primero verificamos los permisos
        $filtros = request()->all();

        $tickets = $this->listado_aux($filtros);
        $tickets = $tickets->get();

        return view('tickets/tickets_listado', compact('tickets'));
    }



    /* - Enlace a la vista de ticket individual - */
    public function individual(tickets $ticket){
        $this->verifyPermission('TicketView', $ticket); //primero verificamos los permisos
        //obtenemos el nombre del cliente y lo pasamos a ticket para que se muestre en la view
        $clientUserSelected = Auth::user()->client;
        $ticket->client = clients::find($clientUserSelected)->name;

        if($ticket->assigned_to_group> 0) $ticket->assigned_to_group = groups::find($ticket->assigned_to_group)->name;

        $info = ticket_info::where('id_ticket', '=', $ticket->id)->get();

        $historico = ticket_historical::where('id_ticket', '=', $ticket->id)
                    ->orderBy('id', 'desc')
                    ->get();
        foreach ($historico as $h) {
          if($h->user > 0) $h->user = User::find($h->user)->name;
        }
    	return view('tickets/ver_ticket', compact('ticket', 'info', 'historico'));
    }

    /* - Enlace a la vista de nuevo ticket - */
    public function nuevo(){
        $this->verifyPermission('TicketCreate'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $groups = groups::all();
        $types = type_ticket::all();
        return view('tickets/nuevo_ticket', compact('groups', 'types'));
    }

    /* - Creación de un nuevo ticket - */
    public function crear_ticket(){
        $this->verifyPermission('TicketCreate'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $datos = request()->all();

        if(!isset($datos['assigned_to_group'])) $datos['assigned_to_group'] = null;

        $ticket = tickets::create([
            'title' => $datos['title'],
            'creation' => now(),
            'type' => $datos['type'],
            'status' => $datos['status'],
            'priority' => $datos['priority'],
            'generated' => 'Manual',
            'assigned_to_group' => $datos['assigned_to_group'],
            'client' => $clientUserSelected
        ]);


        ticket_historical::create([
            'id_ticket' => $ticket->id,
            'text' => 'Se ha creado el ticket',
            'user' => Auth::user()->id 
        ]);

        if(isset($datos['information'])){
           ticket_info::create([
            'id_ticket' => $ticket->id,
            'information_1' => $datos['information']
           ]); 
        }
        

        if ($datos['assigned_to_group'] != null){

            ticket_historical::create([
                    'id_ticket' => $ticket->id,
                    'text' => 'Se ha asigado el ticket al grupo: ' . groups::find($datos['assigned_to_group'])->name,
                    'user' => Auth::user()->id 
                ]);


        //enviamos el correo al grupo
        //CAMBIAR!!!!
        }

        Log::info('[Ticket] Ticket creado: ' . json_encode($ticket) . ' User:'  . Auth::user()->email . '. Cliente: ' . Auth::user()->client);
        return redirect()
            ->route('tickets.individual', $ticket->id)
            ->with('success', "Se ha creado el ticket con id: $ticket->id");
    }

    /* - Enlace a la edición de un ticket - */
    public function editar(tickets $ticket){
        $this->verifyPermission('TicketEdit', $ticket); //primero verificamos los permisos
        $groups = groups::all();
        $types = type_ticket::all();
        return view('tickets/editar_ticket', compact('ticket', 'groups', 'types'));
    }

    /* - Edición de un ticket - */
    public function editar_ticket(tickets $ticket){
        $this->verifyPermission('TicketEdit', $ticket); //primero verificamos los permisos
        $datos = request()->all();
        unset($datos['_token']);
        unset($datos['_method']);

        //funcion auxiliar para editar un ticket
        $this->editar_un_ticket($ticket, $datos);

        return redirect()
            ->route('tickets.individual', $ticket->id)
            ->with('success', "Se ha editado el ticket: $ticket->id");
    }

    /* auxiliar para editar un ticket pasándole el ticket y los datos */ 
    public function editar_un_ticket(tickets $ticket, $datos){
        $antes = array();
        //dejamos solo lo que se ha editado
        foreach ($datos as $key => $value) {
            if($value == $ticket->$key){
                unset($datos[$key]);
            }else{
                $antes[$key] = $ticket->$key;
            }
        }


        $datos['updated_at'] = now();
        if(isset($datos['status'])){
            if($datos['status'] == 'Cerrado') $datos['closed'] = now();
        }

        $ticket->update($datos);
        Log::info('[SSGG] Ticket editado: ' . json_encode($ticket) . ' User:'  . Auth::user()->email);

        if (isset($datos['assigned_to_group'])){

            ticket_historical::create([
                    'id_ticket' => $ticket->id,
                    'text' => 'Se ha asigado el ticket al grupo: ' . groups::find($datos['assigned_to_group'])->name,
                    'user' => Auth::user()->id 
                ]);


        //enviamos el correo al grupo
        //CAMBIAR!!!!

            unset($datos['assigned_to_group']);
            unset($antes['assigned_to_group']);
        }

        if($antes){

            ticket_historical::create([
                'id_ticket' => $ticket->id,
                'text' => 'Se ha modificado el ticket',
                'changes_before' => json_encode($antes),
                'changes_after' => json_encode($datos),
                'user' => Auth::user()->id
            ]);
        }
    }



    /* - Enlace a la vista de actualizar tickets- */
    public function actualizar($tickets){
        $this->verifyPermission('TicketUpdate'); //primero verificamos los permisos
        $tickets = explode(',', $tickets);

        $longitud = count($tickets);

        return view('tickets/actualizar_tickets', compact('tickets', 'longitud'));
    }

    /* - Actualización de los tickets- */
    public function actualizar_tickets($tickets){
        $this->verifyPermission('TicketUpdate'); //primero verificamos los permisos
        $form = request()->all();

        $tickets = explode(',', $tickets);

        foreach ($tickets as &$ticket) {
            ticket_historical::create([
                'id_ticket' => $ticket,
                'text' => 'Se ha actualizado el ticket',
                'actualization' => $form['actualization'],
                'user' => Auth::user()->id 
            ]);

            if(isset($form['solution'])){
                $tk = tickets::find($ticket);
                $tk->status = 'Cerrado';
                $tk->closed = now();
                $tk->solution = $form['solution'];

                $tk->save();

                ticket_historical::create([
                    'id_ticket' => $ticket,
                    'text' => 'Se ha cerrado el ticket',
                    'user' => Auth::user()->id 
                ]);
            }
        }

        Log::info('[SSGG] Ticket actualizado: ' . json_encode($tickets) . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('tickets.listado')
            ->with('success', 'Actualización realizada');
    }


    /* - Enlace a la vista de editar varios tickets- */
    public function editar_varios($tickets){
        $this->verifyPermission('TicketEdit'); //primero verificamos los permisos
        $tickets = explode(',', $tickets);
        $groups = groups::all();
        $types = type_ticket::all();
        $longitud = count($tickets);

        if($longitud == 1){ //edición individual
            return redirect()->route('tickets.editar', $tickets); 
        } 
        else { //edición en grupo
            return view('tickets/editar_varios_tickets', compact('tickets', 'longitud', 'groups', 'types'));
        }
    }

    /* - editar varios tickets- */
    public function editar_varios_tickets($tickets){
        $this->verifyPermission('TicketEdit'); //primero verificamos los permisos
        $form = request()->all();
        unset($form['_token']);

        //dejamos solo lo que se ha editado
        foreach ($form as $key => $value) if($value == null)unset($form[$key]);

        $tickets = explode(',', $tickets);


        if($form == null)return redirect()->route('tickets.listado')->with('errors', 'No se ha editado ningún dato.');

        if(isset($form['assigned_to_group']))$assigned_to_group = $form['assigned_to_group'];
        else $assigned_to_group = null;
        

        foreach ($tickets as &$ticket) {
            $tk = tickets::find($ticket);
            $this->verifyPermission('TicketEdit', $tk); //primero verificamos los permisos
            //Para cada ticket, llamamos a la funcion auxiliar que edita un ticket
            $this->editar_un_ticket($tk, $form);
        }

        return redirect()
            ->route('tickets.listado')
            ->with('success', 'Edición realizada');
    }

    /* Exporta los tickets filtrados */
    public function exportar_tickets(){
        $this->verifyPermission('TicketView'); //primero verificamos los permisos
        $filtros = request()->all();

        $tickets = $this->listado_aux($filtros);

        return (new ModalExport($tickets))->download('tickets' . Auth::user()->clientName . '.csv');

    }

    public function enviar_correo(){

        $vista = "";
        //dentro de la vista, se podrá utilizar la variable $msg con los datos de $mensaje
        //por ejemplo, si hay $mensaje->nombre, entonces en la vista podremos usar $m->nombre
        Mail::send('emails.' . $vista, ['msg' => $mensaje],  function($m) use ($mensaje){
            $m->to($destino)->subject($asunto);
        });
    }

}