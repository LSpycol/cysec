<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\clients;
use \App\User;
use \App\vul_assets;
use \App\vul_occurrences;
use \App\vul_scans;
use \App\vul_sites;
use \App\vul_vulnerabilitys;
use \App\Exports\ModalExport;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class vulController extends Controller
{
    /* comprueba que estás logeado, si no, te devuelvea al login */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }

    /*
      Estados de las vulnerabilidades
      - Pendiente
      - Activa
      - Asumida - Corregida - Descartada
    */


    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        /* ***** permisos disponibles 
        'VulView', 'VulActivate', 'VulEdit', 'VulDelete'
        ****** */
        $user = Auth::user(); 
        if($user->role != 'Administrador' && !$user->can($permiso . $user->client)){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }

        //Si el objeto pasado no es el del cliente seleccionado
        if(isset($objeto)){
            if($objeto->client != Auth::user()->client){
                return abort(403); //devolvemos la vista de permisos insuficientes
            }
        }
        
    }

    /* Enlace a la vista principal*/
    public function main(){
        $this->verifyPermission('VulView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $occurrences= DB::table('vul_occurrences')
                    ->select('ip', 'severity', 'status', 'severity', 'vul_sites.name as site')
                    ->leftjoin('vul_assets', 'vul_occurrences.asset', '=',  'vul_assets.id')
                    ->leftjoin('vul_sites', 'vul_assets.site', '=',  'vul_sites.id')
                    ->leftjoin('vul_vulnerabilitys', 'vulnerability', '=',  'vul_vulnerabilitys.id')
                    ->where('client', $clientUserSelected)
                    ->get();
        $occurrences = json_encode($occurrences);

        Log::info('[Vul] Acceso a Main. User:'  . Auth::user()->email . '. Cliente: ' . Auth::user()->client);
    	return view('vul/main', compact('occurrences'));
    }

    /* Enlace a la vista de sites*/
    public function sites(){
        $this->verifyPermission('VulView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $sites = vul_sites::query()->where('client', $clientUserSelected)->get();

        foreach ($sites as $site) {
            $assets = vul_assets::query()->where('site', $site->id)->count();
            $site->assets = $assets;
        }


        return view('vul/sites', compact('sites'));
    }

    /* Enlace a la vista de assets*/
    public function assets(){
        $this->verifyPermission('VulView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $assets= DB::table('vul_assets')
                    ->select('name', 'vul_assets.id', 'site', 'ip')
                    ->leftjoin('vul_sites', 'vul_assets.site', '=',  'vul_sites.id')
                    ->where('client', $clientUserSelected)
                    ->get();

        foreach ($assets as $a) {
            $a->vulnerabilitys = vul_occurrences::query()->where('asset', $a->id)->count();
        }

        return view('vul/assets', compact('assets'));
    }

    /* Enlace a la vista de scans*/
    public function scans(){
        $this->verifyPermission('VulView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $scans= DB::table('vul_scans')
                    ->select('name', 'vul_scans.id', 'site', 'type', 'template', 'engine', 'vulnerabilitys', 'start_at', 'finish_at')
                    ->leftjoin('vul_sites', 'vul_scans.site', '=',  'vul_sites.id')
                    ->where('client', $clientUserSelected)
                    ->get();
        //dd($scans);
        return view('vul/scans', compact('scans'));
    }

    /* funcion auxilar para obtener las ocurrencias  con el filtro */
    public function ocurrencias_aux($filtros){
        $clientUserSelected = Auth::user()->client;
        foreach ($filtros as $key => $value) {
            if($value == null) unset($filtros[$key]);
        }

        $condiciones = array();
        foreach ($filtros as $key => $value) {
            switch ($key) {
                case 'vulnerability':
                    array_push($condiciones, array('vul_vulnerabilitys.name', 'LIKE', '%'.$value.'%'));
                    break;
                case 'site':
                    array_push($condiciones, array('vul_sites.name', 'LIKE', '%'.$value.'%'));
                    break;
                
                default:
                    array_push($condiciones, array($key, 'LIKE', '%'.$value.'%'));
                    break;
            }
        }

        array_push($condiciones, array('client', '=', $clientUserSelected));

        $occurrences= DB::table('vul_occurrences')
                ->select('vul_occurrences.id', 'ip', 'port', 'vul_vulnerabilitys.name as vulnerability', 'status', 'vul_sites.name', 'vul_occurrences.created_at', 'severity')
                ->leftjoin('vul_assets', 'vul_occurrences.asset', '=',  'vul_assets.id')
                ->leftjoin('vul_sites', 'vul_assets.site', '=',  'vul_sites.id')
                ->leftjoin('vul_vulnerabilitys', 'vulnerability', '=',  'vul_vulnerabilitys.id')
                ->where($condiciones)
                ->orderBy('vul_occurrences.id', 'asc');

        return $occurrences;
    }

    /* Enlace a la vista de ocurrencias*/
    public function ocurrencias(){
        $this->verifyPermission('VulView'); //primero verificamos los permisos

        $filtros = request()->all();
        unset($filtros['_token']);

        
        $occurrences = $this->ocurrencias_aux($filtros); 
        $occurrences = $occurrences->get();       

        return view('vul/ocurrencias', compact('occurrences'));
    }

    /* Eliminar un asset*/
    public function eliminar_asset(vul_assets $asset){
        $this->verifyPermission('VulDelete'); //primero verificamos los permisos
        $ocurrences = vul_occurrences::where('asset',  $asset->id)->count();
        if($ocurrences > 0){
            return redirect()
                ->route('vul.assets')
                ->with('errors', "El asset tiene ocurrencias asociadas");
        }else{

            $asset->delete();

            Log::info('[Vul] Asset eliminado: ' . $asset->ip . ' . User:'  . Auth::user()->email);
            return redirect()
                ->route('vul.assets')
                ->with('success', "Se ha eliminado el asset: $asset->ip");
        }
    }

    /* Eliminar un scan*/
    public function eliminar_scan(vul_scans $scan){
        $this->verifyPermission('VulDelete'); //primero verificamos los permisos

        $scan->delete();
        Log::info('[Vul] Scan eliminado: ' . $scan->site . ' . User:'  . Auth::user()->email);
        return redirect()
                ->route('vul.scans')
                ->with('success', "Se ha eliminado el escaneo.");
    }

    /* Eliminar una o varias ocurrencias*/
    public function eliminar_varias_ocurrencias($ocurrencias){
        $this->verifyPermission('VulDelete'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;


        $ocurrencias = explode(',', $ocurrencias);
        foreach ($ocurrencias as $o) {


            $ocurrencia= DB::table('vul_occurrences')
                    ->select('client')
                    ->leftjoin('vul_assets', 'vul_occurrences.asset', '=',  'vul_assets.id')
                    ->leftjoin('vul_sites', 'vul_assets.site', '=',  'vul_sites.id')
                    ->where('vul_occurrences.id', $o)
                    ->get();


            if($ocurrencia[0]->client !=  $clientUserSelected){
                return abort(403);
            }else{
                DB::table('vul_occurrences')->where('id', $o)->delete();
            }
        }
        Log::info('[Vul] Ocurrencias eliminadas: ' . json_encode($ocurrencias) . ' . User:'  . Auth::user()->email);
        return redirect()
                ->route('vul.ocurrencias')
                ->with('success', "Se han eliminado las ocurrencias.");

    }


    /* Enlace a la vista de un site*/
    public function ver_site(vul_sites $site){
        $this->verifyPermission('VulView', $site); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $occurrences= DB::table('vul_occurrences')
                    ->select('port', 'status', 'ip', 'severity')
                    ->leftjoin('vul_assets', 'vul_occurrences.asset', '=',  'vul_assets.id')
                    ->leftjoin('vul_sites', 'vul_assets.site', '=',  'vul_sites.id')
                    ->leftjoin('vul_vulnerabilitys', 'vulnerability', '=',  'vul_vulnerabilitys.id')
                    ->where('vul_sites.id', $site->id)
                    ->get();

        $occurrences = json_encode($occurrences->toArray());

        return view('vul/ver_site', compact('site', 'occurrences'));
    }


    /* Enlace a la vista de editar varias ocurrencias*/
    public function editar_varias_ocurrencias($ocurrencias){
        $this->verifyPermission('VulEdit'); //primero verificamos los permisos
        return view('vul/editar_ocurrencias', compact('ocurrencias'));
    }

    /* edita el estado de varias ocurrencias */
    public function cambiar_estado_ocurrencia($ocurrencias){
        $this->verifyPermission('VulEdit'); //primero verificamos los permisos
        $datos = request()->all();
        if($datos['status'] == 'Pendiente' || $datos['status'] == 'Activa' || $datos['status'] == 'Asumida' || $datos['status'] == 'Corregida' || $datos['status'] == 'Descartada'){

            $ocurrencias = explode(',', $ocurrencias);
            foreach ($ocurrencias as $o) {
                $o = vul_occurrences::find($o);
                $o->status = $datos['status'];
                $o->save();
            }
            Log::info('[Vul] Ocurrencias editadas: ' . json_encode($ocurrencias) . ' . User:'  . Auth::user()->email);
            return redirect()
                ->route('vul.ocurrencias')
                ->with('success', "Se han editado las ocurrencias.");
        } else{
            return redirect()
                ->route('vul.ocurrencias')
                ->with('errors', "Ha ocurrido un error al editar las ocurrencias.");
        }
    }

    /* Enlace a la vista para ver una ocurrencia */
    public function ver_ocurrencia(vul_occurrences $ocurrencia){
        $this->verifyPermission('VulView');
        $ocurrencia= DB::table('vul_occurrences')
                    ->select('port', 'status', 'ip', 'severity', 'vul_sites.client as client', 'vul_occurrences.created_at', 'vul_occurrences.id', 'vul_vulnerabilitys.name as vulnerability', 'vul_sites.name as site', 'published', 'description', 'cve')
                    ->leftjoin('vul_assets', 'vul_occurrences.asset', '=',  'vul_assets.id')
                    ->leftjoin('vul_sites', 'vul_assets.site', '=',  'vul_sites.id')
                    ->leftjoin('vul_vulnerabilitys', 'vulnerability', '=',  'vul_vulnerabilitys.id')
                    ->where('vul_occurrences.id', $ocurrencia->id)
                    ->get()[0];
        
        $this->verifyPermission('VulView', $ocurrencia); //verificamos los permisos

        return view('vul/ver_ocurrencia', compact('ocurrencia'));
    }

    /* exporta las ocurrencias filtradas */
    public function exportar_ocurrencias(){
        $this->verifyPermission('VulView');
        $filtros = request()->all();
        unset($filtros['_token']);

        $occurrences = $this->ocurrencias_aux($filtros);

        return (new ModalExport($occurrences))->download('ocurrencias' . Auth::user()->clientName . '.csv');

    }

}

