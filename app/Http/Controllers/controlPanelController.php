<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Passwords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \App\clients;
use \App\clients_deleted;
use \App\User;

use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \App\permissionModel;

class controlPanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* - Enlace a la vista de los datos de la cuenta- */
    public function main(){
    	$token = 'getNewPassResetUrl()';
    	return view('controlPanel/main')->with(['token' => $token]);
    }

    public function editar_datos(){
        $datos = request()->all();
        unset($datos['_token']);
        unset($datos['_method']);

        $user = Auth::user();

        if($datos['name'] == ''){
            return redirect()->route('control.main')->with('errors', "El nombre no puede estar vacío.");
        }

        $user->update($datos);

        Log::info('[Control] Datos de usuario editados. User:'  . Auth::user()->email);
        return redirect()
            ->route('control.main')
            ->with('success', "Se han modificado tus datos");
    }

    public function password(){
        $datos = request()->all();

        $user = Auth::user();
        if($datos['email'] != $user->email){
            return redirect()->route('control.main')->with('errors', "El email no es correcto");
        }

        if($datos['password'] != $datos['password_confirmation']){
            return redirect()->route('control.main')->with('errors', "Las contraseñas no coinciden");
        }

        if(strlen($datos['password'] < 8)){
            return redirect()->route('control.main')->with('errors', "Las contraseña debe tener 8 caracteres como  mínimo");
        }

        

        $user->update(['password' => bcrypt($datos['password'])]);

        return redirect()
            ->route('control.main')
            ->with('success', "Se han modificado tu contraseña");
    }

    //Form con los clientes que puede seleccionar el usuario
    public function loadClients(){
        $user = Auth::user();
        if($user->role == 'Administrador') return $this->loadClientsAdmin();

        $permisos= $user->getAllPermissions();

        $clientes = array();

        //obtenemos todos los clientes a los que se tiene acceso
        foreach ($permisos as $p) {
            if(strpos($p->name, 'ClientView') === 0){
                array_push($clientes, str_replace('ClientView', '', $p->name));
            }
        }

        $totalClientes = clients::All();

        echo '<select class="w3-select" name="client" required>';
        //mostramos los clientes al que el usuario tiene acceso
        foreach ($clientes as $c) {
            foreach ($totalClientes as $tc) {
                if($tc->id == $c) echo "<option value='$tc->id'>$tc->name</option>";
            }
        }
        echo '</select>';
    }

    //Form con todos los clientes, solo para usuarios con rol admin
    public function loadClientsAdmin(){
        $totalClientes = clients::All();

        echo '<select class="w3-select" name="client" required>';
        //mostramos los clientes al que el usuario tiene acceso
        foreach ($totalClientes as $tc) {
            echo "<option value='$tc->id'>$tc->name</option>";
        }

        echo '</select>';
    }

    public function changeClient(){
        $datos = request()->all();
        $user = Auth::user();

        if($user->role == "Administrador" || $user->can('ClientView' . $datos['client'])){
            $client = clients::find($datos['client']);

            if($client != null){
                $u = new user();
                $u = $user;
                $u->client = $client->id;
                $u->clientName = $client->name;
                $u->save();
                return redirect()
                    ->back()
                    ->with('success', "Se ha cambiado a $client->name");
            }
        }
        dd('No');
    }

}
