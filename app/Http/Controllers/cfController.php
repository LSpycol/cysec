<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\clients;
use \App\User;
use \App\type_ticket;
use \App\groups;
use \App\cf_domains;
use \App\cf_typosquatting;
use \App\Exports\ModalExport;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class cfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        /* ***** permisos disponibles 
        'CfView',  'CfTypo', 'CfTypoEdit'
        ****** */
        $user = Auth::user(); 
        if($user->role != 'Administrador' && !$user->can($permiso . $user->client)){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }

        //Si el objeto pasado no es el del cliente seleccionado
        if(isset($objeto)){
            if($objeto->client != Auth::user()->client){
                return abort(403); //devolvemos la vista de permisos insuficientes
            }
        }

    }


    /* Enlace a la vista principal*/
    public function main(){
        $this->verifyPermission('CfView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $typos = cf_typosquatting::query()->where('client', $clientUserSelected)->get();

        Log::info('[CF] Acceso a Main. User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
    	return view('cf/main', compact('typos'));
    }

    /* Enlace a la vista de typosquatting*/
    public function typosquatting($domain = null){
        $this->verifyPermission('CfTypo'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        if($domain == null){
        	$domain = cf_domains::query()->where('client', $clientUserSelected)->get();
        }else{
            $domain = cf_domains::query()->where('id', $domain)->get(); 
        }

        if($domain->count() == 1){
        	$domain = $domain->first();
        	$typos = cf_typosquatting::query()->where('domain', $domain->id)->get();
        }else{
            foreach ($domain as $d) {
                $d->typos = cf_typosquatting::query()
                            ->where('domain', $d->id)
                            ->where('status', 'Activo')
                            ->count();
            }
            return view('cf/typosquatting_menu', compact('domain', 'typos'));
        }


    	return view('cf/typosquatting', compact('domain', 'typos'));
    }

    /* Cambia el estado a Activo de un typosquatting*/
    public function activar_typosquatting(cf_typosquatting $typo){
    	$this->verifyPermission('CfTypo', $typo); //primero verificamos los permisos
    	$typo->status = "Activo";
    	$typo->discarted_at = null;
    	$typo->save();

        Log::info('[CF] Typo activado: ' . $typo->typo . ' User:'  . Auth::user()->email);
    	return redirect()
            ->route('cf.typosquatting')
            ->with('success', "Se ha activado el dominio: $typo->typo");

    }

    /* Cambia el estado a Activo de un typosquatting*/
    public function descartar_typosquatting(cf_typosquatting $typo){
    	$this->verifyPermission('CfTypo', $typo); //primero verificamos los permisos
    	$typo->discarted_at = now();
    	$typo->status = "Descartado";
		$typo->save();

        Log::info('[CF] Typo descartado: ' . $typo->typo . ' User:'  . Auth::user()->email);
    	return redirect()
            ->route('cf.typosquatting')
            ->with('success', "Se ha descartado el dominio: $typo->typo");

    }

    /* Cambia el estado a Activo de un typosquatting*/
    public function reportar_typosquatting(cf_typosquatting $typo){
    	$this->verifyPermission('CfTypo', $typo); //primero verificamos los permisos
    	$typo->reported_at = now();
    	$typo->status = "Reportado";
		$typo->save();
		
		$groups = groups::all();
        $types = type_ticket::all();

        Log::info('[CF] Typo reportado: ' . $typo->typo . ' User:'  . Auth::user()->email);
    	return view('cf/nuevo_ticket_typo', compact('typo', 'groups', 'types'));
    }


    /* Exporta los typosquating */
    public function exportar_typosquatting($domain){
        $this->verifyPermission('CfTypo'); //primero verificamos los permisos

         $typos = cf_typosquatting::query()->where('domain', $domain);

        return (new ModalExport($typos))->download('typosquatting' . Auth::user()->clientName . '.csv');

    }

    /* Exporta los typosquating */
    public function nuevo_dominio(){
        $this->verifyPermission('CfTypoEdit'); //primero verificamos los permisos
        return view('cf/nuevo_dominio');
    }

    public function crear_dominio(){
        $this->verifyPermission('CfTypoEdit'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $datos = request()->all();

        $dominio = cf_domains::create([
            'domain' => $datos['domain'],
            'client' => $clientUserSelected,
            'created_at' => now()
        ]);

        Log::info('[CF] Dominio nuevo: ' . json_encode($dominio) . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('cf.typosquatting')
            ->with('success', "Se ha creado el dominio: $dominio->domain" . '. Cliente: ' . $clientUserSelected);

    }

       

}
