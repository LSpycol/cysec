<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Passwords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \App\clients;
use \App\User;
use \App\groups;

use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \App\permissionModel;

class groupsPanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission(){
    	if(Auth::user()->role != 'Administrador'){
    		return abort(403); //devolvemos la vista de permisos insuficientes
    	}
    }

    //de una lista de usuarios, pasándole la id obtien el nombre
    public function getNameUser($users, $id){
    	$this->verifyPermission(); //primero verificamos los permisos
    	foreach ($users as $u) {
    		if($u->id == $id) return $u->name;
    	}
    }

    //vista principal de grupos
    public function grupos(){
    	$this->verifyPermission(); //primero verificamos los permisos
    	$groups = groups::all();
    	$users = User::all('id', 'name')->sortBy("name");
    	$i = 0;
    	//cambiamos las ID de los usuarios por su nombre
    	foreach ($groups as $g) {
    		$groups[$i]->owner = $this->getNameUser($users, $g->owner);

    		$groupUsers = json_decode($g->users);
    		$j = 0;
    		foreach ($groupUsers as $idUser) {
    			$groupUsers[$j] = $this->getNameUser($users, $idUser);
    			$j++;
    		}

    		$groups[$i]->users = implode(", ",$groupUsers);
    		$i++;
    	}

        Log::info('[Control] Acceso a Grupos. User:'  . Auth::user()->email);
    	return view('controlPanel/grupos', compact('groups', 'users'));
    }

    /* Enlace a la vista de crear un nuevo grupo */
    public function nuevo_grupo(){
        $this->verifyPermission(); //primero verificamos los permisos
        $users = User::all('id', 'name')->sortBy("name");
        return view('controlPanel/nuevo_grupo', compact('users'));
    }


    /* crea un nuevo  grupo */
    public function crear_grupo(){
    	$this->verifyPermission(); //primero verificamos los permisos
    	$datos = request()->all();
        //quitamos la coma del  final
        $datos['users'] = substr($datos['users'], 0, -1);

    	$group = groups::create([
            'name' => $datos['name'],
            'email' => $datos['email'],
            'owner' => $datos['owner'],
            'users' => '[' . $datos['users'] . ']',
        ]);

        Log::info('[Control] Grupo creado: ' . json_encode($group) . ' User:'  . Auth::user()->email);
        return redirect()
                 ->route('control.grupos')
                 ->with('success', "Se ha creado el usuario: $group->name.");
    }

    /* Elimina un grupo */
    public function eliminar_grupo(groups $group){
    	$this->verifyPermission(); //primero verificamos los permisos
    	$group->delete();

        Log::info('[Control] Grupo eliminado: ' . $group->name . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('control.grupos')
            ->with('success', "Se ha eliminado el grupo: $group->name");
    }

    /* Enlace a la vista de editar un grupo */
    public function editar_grupo(groups $group){
    	$this->verifyPermission(); //primero verificamos los permisos
    	$users = User::all('id', 'name')->sortBy("name");
    	$group->owner = $this->getNameUser($users, $group->owner);
    	return view('controlPanel/editar_grupo', compact('group', 'users'));
    }

    public function edicion_grupo(groups $group){
    	$this->verifyPermission(); //primero verificamos los permisos

    	$datos = request()->all();
        $datos['users'] = substr($datos['users'], 0, -1);
        
        $group->name = $datos['name'];
        $group->email = $datos['email'];
        $group->users = '[' . $datos['users'] . ']';
        $group->update();
        
        Log::info('[Control] Grupo editado: ' . json_encode($group) . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('control.grupos')
            ->with('success', "Se ha editado el grupo: $group->name");
    }
}
