<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\tickets;
use \App\ssgg_assets;
use \App\ssgg_services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class plController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        $user = Auth::user(); 
        if($user->role != 'Administrador' && $user->role != 'Primera Línea'){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }
    }

    /* Enlace a la vista principal*/
    public function main(){
    	$this->verifyPermission('plView'); //primero verificamos los permisos

        $tickets = tickets::query()->where('assigned_to_group', null)->count();

        Log::info('[PL] Acceso a Main. User:'  . Auth::user()->email);
    	return view('pl/main', compact('tickets'));
    }

    /* Enlace a la vista de tickets sin asignar */
    public function tickets(){
        $this->verifyPermission('plView'); //primero verificamos los permisos
        $tickets = tickets::query()->
                            select('tickets.id', 'name', 'creation', 'type', 'status', 'priority', 'title')->
                            leftjoin('clients', 'tickets.client', 'clients.id')->
                            where('assigned_to_group', null)->
                            get();
        return view('pl/tickets_listado', compact('tickets'));
    }


    /* Enlace a la vista de monitorización */
    public function monitorizacion(){
        $this->verifyPermission('plView'); //primero verificamos los permisos

        $assets = ssgg_assets::query()->
                            select('clients.name','status', DB::raw('count(*) as cantidad'))->
                            leftjoin('clients', 'clients.id', 'client')->
                            groupby('clients.name', 'status')->get();

        $services = ssgg_services::query()->
                            select('clients.name','status', DB::raw('count(*) as cantidad'))->
                            leftjoin('clients', 'clients.id', 'client')->
                            groupby('clients.name', 'status')->get();
        return view('pl/monitorizacion', compact('assets', 'services'));
    }


}
