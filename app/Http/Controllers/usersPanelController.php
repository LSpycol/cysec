<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Passwords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \App\clients;
use \App\clients_deleted;
use \App\User;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \App\permissionModel;

class usersPanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission(){
        if(Auth::user()->role != 'Administrador'){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }
    }



    /*********************************/
    /***************USUARIOS******************/

    /* Enlace a la vista de usuarios */
    public function usuarios(){
        $this->verifyPermission(); //primero verificamos los permisos
        $users = User::all();

        Log::info('[Control] Acceso a Usuarios. User:'  . Auth::user()->email);
        return view('controlPanel/usuarios', compact('users'));
    }

    /* Enlace a la vista de crear un nuevo usuarios */
    public function nuevo_usuario(){
        $this->verifyPermission(); //primero verificamos los permisos
        return view('controlPanel/nuevo_usuario');
    }

    /* Enlace a la vista de usuarios */
    public function crear_usuario(){
        $this->verifyPermission(); //primero verificamos los permisos
        $datos = request()->all();
        if(!isset($datos['phone'])) $datos['phone'] = '';

        $user = User::create([
            'name' => $datos['name'],
            'email' => $datos['email'],
            'phone' => $datos['phone'],
            'role' => $datos['role'],
            'password' => Hash::make('12345'), //ESTO HABRÁ QUE CAMBIARLO
        ]);

        Log::info('[Control] Usuario creado: ' . json_encode($user) . '. User:'  . Auth::user()->email);
        return redirect()
                 ->route('control.privilegios_usuario', compact('user'))
                 ->with('success', "Se ha creado el usuario: $user->name, configure los privilegios.");
    }

    /* Enlace a la vista de privilegios */
    public function privilegios_usuario(User $user){
        $this->verifyPermission(); //primero verificamos los permisos
        $clients = clients::all();
        $role = Role::find(2);
        $role = $role->getAllPermissions();

        $permisosUsuario = DB::table('model_has_permissions')
                            ->select('name')
                            ->leftjoin('permissions', 'permissions.id', 'model_has_permissions.permission_id')
                            ->where('model_id', $user->id)
                            ->get();
        return view('controlPanel/privilegios_usuarios', compact('user', 'clients', 'role', 'permisosUsuario'));
    }

    /* guarda los privilelgios del usuario */
    public function privilegios_usuario_guardar(User $user){
        $this->verifyPermission(); //primero verificamos los permisos
        $datos = request()->all();
        unset($datos['_token']);

        //quitamos todos los permisos del usuario
        $permisosDelUsuario = $user->getAllPermissions();
        $user->revokePermissionTo($permisosDelUsuario);

        //Como reiniciamos los permisos, quitamos el cliente que tiene por defecto
        $user->client = 0;
        $user->save();

        //añadimos los  nuevos
        foreach ($datos as $key => $value) {
            $user->givePermissionTo($key);
        }

        Log::info('[Control] Usuario privilegios modificados de: ' . $user->name . '. User:'  . Auth::user()->email);
        return redirect()
            ->route('control.usuarios')
            ->with('success', "Se han guardado los privilegios de: $user->name");
    }


    /* Eliminar un usuario */
    public function eliminar_usuario(User $user){
        $this->verifyPermission(); //primero verificamos los permisos
        $user->delete();

        Log::info('[Control] Usuario eiminado: ' . $user->name . '. User:'  . Auth::user()->email);
        return redirect()
            ->route('control.usuarios')
            ->with('success', "Se ha eliminado el usuario: $user->name");
    }
}
