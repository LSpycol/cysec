<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class loginController extends Controller
{
    
    public function main(){
        return view('login/login');
    }

    public function login(){
        return redirect()->route('tickets.main');
    }

}
