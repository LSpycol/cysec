<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\anvd_products;
use \App\anvd_occurrences;
use \App\anvd_nist_products;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class anvdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        /* ***** permisos disponibles 
        'AnvdView'
        ****** */
        $user = Auth::user(); 
        if($user->role != 'Administrador' && !$user->can($permiso . $user->client)){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }

        //Si el objeto pasado no es el del cliente seleccionado
        if(isset($objeto)){
            if($objeto->client != Auth::user()->client){
                return abort(403); //devolvemos la vista de permisos insuficientes
            }
        }

    }


    /* Enlace a la vista principal*/
    public function main(){
    	$this->verifyPermission('AnvdView'); //primero verificamos los permisos
    	$clientUserSelected = Auth::user()->client;

    	$ocurrencias = anvd_occurrences::query()->
    						select('anvd_occurrences.id', 'status','published', 'description', 'severity', 'cve', 'anvd_products.product', 'anvd_occurrences.created_at')->
					    	where('anvd_occurrences.client', $clientUserSelected)->
                            where('anvd_occurrences.status', 'Activa')->
					    	leftjoin('vul_vulnerabilitys', 'anvd_occurrences.vulnerability', 'vul_vulnerabilitys.id')->
                            leftjoin('anvd_products','anvd_occurrences.product_id',  'anvd_products.id')->
					    	orderby('severity',  'desc')->
					    	get();
        $resueltas = anvd_occurrences::query()->where('client', $clientUserSelected)->where('status', 'Resuelta')->count();
        $descartadas = anvd_occurrences::query()->where('client', $clientUserSelected)->where('status', 'Descartada')->count();

        Log::info('[ANVD] Acceso a Main. User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
    	return view('anvd/main', compact('ocurrencias', 'resueltas', 'descartadas'));
    }

     /* Enlace a la vista de productos*/
    public function productos(){
    	$this->verifyPermission('AnvdView'); //primero verificamos los permisos
    	$clientUserSelected = Auth::user()->client;
    	$products = anvd_products::query()->where('client', $clientUserSelected)->get();
    	return view('anvd/productos', compact('products'));
    }

    /* Elimina un productos*/
    public function eliminar_producto(anvd_products $product){
    	$this->verifyPermission('AnvdView', $product); //primero verificamos los permisos
    	$product->delete();

        Log::info('[ANVD] Producto eliminado:' . $product->product . '. User:'  . Auth::user()->email);

    	return redirect()
            ->route('anvd.productos')
            ->with('success', "Se ha eliminado el producto: $product->product");
    }

    /* Enlace a la vista de crear un nuevo productos*/
    public function nuevo_producto(){
    	$this->verifyPermission('AnvdView'); //primero verificamos los permisos
    	return view('anvd/nuevo_producto');
    }

    /* Crea un nuevo productos*/
    public function crear_producto(){
    	$this->verifyPermission('AnvdView'); //primero verificamos los permisos
    	$clientUserSelected = Auth::user()->client;
        $datos = request()->all();
        $product = anvd_products::create([
            'vendor' => $datos['vendor'],
            'product' => $datos['product'],
            'email' => $datos['email'],
            'client' => $clientUserSelected,
            'created_at' => now()
        ]);

        Log::info('[ANVD] Producto añadido:' . json_encode($product) . '. User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);

        return redirect()
            ->route('anvd.productos')
            ->with('success', "Se ha creado el producto: $product->product");
    }

    /* Enlace a la vista de editar un productos*/
    public function editar_producto(anvd_products $product){
    	$this->verifyPermission('AnvdView', $product); //primero verificamos los permisos
    	return view('anvd/editar_producto', compact('product'));
    }

     /* Enlace a la vista de editar un productos*/
    public function edicion_producto(anvd_products $product){
    	$this->verifyPermission('AnvdView', $product); //primero verificamos los permisos
        $datos = request()->all();

        $product->email = $datos['email'];
        $product->vendor = $datos['vendor'];
        $product->product = $datos['product'];
        $product->save();

        Log::info('[ANVD] Producto editado:' . json_encode($product) . '. User:'  . Auth::user()->email);

    	return redirect()
            ->route('anvd.productos')
            ->with('success', "Se ha editar el producto: $product->product");
    }


     /* Enlace a la vista de ocurrencias de un tipo*/
    public function ocurrencias($estado = '%'){
        $this->verifyPermission('AnvdView'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $ocurrencias = anvd_occurrences::query()->
                            select('anvd_occurrences.id', 'status','published', 'description', 'severity', 'cve', 'anvd_products.product', 'anvd_occurrences.created_at')->
                            where('anvd_occurrences.client', $clientUserSelected)->
                            where('anvd_occurrences.status', 'like',  $estado)->
                            leftjoin('vul_vulnerabilitys', 'anvd_occurrences.vulnerability', 'vul_vulnerabilitys.id')->
                            leftjoin('anvd_products','anvd_occurrences.product_id',  'anvd_products.id')->
                            orderby('severity',  'desc')->
                            get();
        return view('anvd/ocurrencias', compact('ocurrencias'));
    }

    /* descarta las ocurrencias seleccionadas */
    public function descartar_ocurrencias($lista){
        $this->verifyPermission('AnvdView'); //primero verificamos los permisos
        $lista = explode(",",$lista);
        foreach ($lista as $l) {
            $oc = anvd_occurrences::find($l);
            $oc->status = 'Descartada';
            $oc->save();
        }

        Log::info('[ANVD] Ocurrencias descartadas:' . json_encode($lista) . '. User:'  . Auth::user()->email);
        return redirect()
            ->back()
            ->with('success', "Se han descartado las ocurrencias");
    }

    /* descarta las ocurrencias seleccionadas */
    public function activar_ocurrencias($lista){
        $this->verifyPermission('AnvdView'); //primero verificamos los permisos
        $lista = explode(",",$lista);
        foreach ($lista as $l) {
            $oc = anvd_occurrences::find($l);
            $oc->status = 'Activa';
            $oc->save();
        }

        Log::info('[ANVD] Ocurrencias activadas:' . json_encode($lista) . '. User:'  . Auth::user()->email);
        return redirect()
            ->back()
            ->with('success', "Se han activado las ocurrencias");
    }

    /* descarta las ocurrencias seleccionadas */
    public function resolver_ocurrencias($lista){
        $this->verifyPermission('AnvdView'); //primero verificamos los permisos
        $lista = explode(",",$lista);
        foreach ($lista as $l) {
            $oc = anvd_occurrences::find($l);
            $oc->status = 'Resuelta';
            $oc->save();
        }

        Log::info('[ANVD] Ocurrencias resueltas:' . json_encode($lista) . '. User:'  . Auth::user()->email);
        return redirect()
            ->back()
            ->with('success', "Se han resuelto las ocurrencias");
    }

    /* Enlace a la vista de ver ocurrencias */
    public function ver_ocurrencia($ocurrencia){
        $this->verifyPermission('AnvdView'); //primero verificamos los permisos
        $ocurrencia = anvd_occurrences::query()->
                            select('anvd_occurrences.id', 'status','published', 'description', 'severity', 'cve', 'anvd_products.product','anvd_products.vendor' ,  'anvd_occurrences.created_at')->
                            where('anvd_occurrences.id', $ocurrencia)->
                            leftjoin('vul_vulnerabilitys', 'anvd_occurrences.vulnerability', 'vul_vulnerabilitys.id')->
                            leftjoin('anvd_products','anvd_occurrences.product_id',  'anvd_products.id')->
                            orderby('severity',  'desc')->
                            get()[0];
        return view('anvd/ver_ocurrencia', compact('ocurrencia'));
    }


    public function cargar_productos($texto, $texto2){
        $products = anvd_nist_products::query()
                                        ->where('vendor', $texto)
                                        ->where('product','like','%' . $texto2 . '%')->get();
        return view('anvd/cargar_productos', compact('products'));
    }

    public function cargar_vendedor($texto){
        $products = anvd_nist_products::query()
                                        ->select('vendor')
                                        ->where('vendor','like','%' . $texto . '%')
                                        ->groupby('vendor')
                                        ->get();
        return view('anvd/cargar_vendedores', compact('products'));
    }

}
