<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\clients;
use \App\vd_words;
use \App\vd_news;
use \App\vd_notifications;
use \App\type_ticket;
use \App\groups;
use \App\Exports\ModalExport;
use Illuminate\Support\Facades\DB;

use \App\Http\Controllers\ticketsController;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class vdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verifyClientPermission');
    }

    /* Verificamos los permisos del usuario */
    public function verifyPermission($permiso, $objeto = null){
        /* ***** permisos disponibles 
        'VdView', 'VdNews', 'VdNotifications'
        ****** */
        $user = Auth::user(); 
        if($user->role != 'Administrador' && !$user->can($permiso . $user->client)){
            return abort(403); //devolvemos la vista de permisos insuficientes
        }

        //Si el objeto pasado no es el del cliente seleccionado
        if(isset($objeto)){
            if($objeto->client != Auth::user()->client){
                return abort(403); //devolvemos la vista de permisos insuficientes
            }
        }

    }


     /* Enlace a la vista principal*/
    public function main(){
    	$this->verifyPermission('VdView'); //primero verificamos los permisos
    	$clientUserSelected = Auth::user()->client;
        $notificaciones = vd_notifications::query()->where('client', $clientUserSelected)->get();
    	$notificaciones = json_encode($notificaciones);
        Log::info('[VD] Acceso a Main. User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
        return view('vd/main', compact('notificaciones'));
    }

     /* Enlace a la vista de noticias*/
    public function noticias(){
    	$this->verifyPermission('VdNews'); //primero verificamos los permisos
    	$clientUserSelected = Auth::user()->client;

    	$palabras = vd_words::query()->where('client', $clientUserSelected)->get();
        $noticias = vd_news::query()->where('client', $clientUserSelected)->get();

    	return view('vd/noticias', compact('palabras', 'noticias'));
    }

 	/* Crea una nueva palabra*/
    public function crear_palabra(){
    	$this->verifyPermission('VdNews'); //primero verificamos los permisos
    	$clientUserSelected = Auth::user()->client;
    	$datos = request()->all();

    	$palabra = vd_words::create([
            'word' => $datos['word'],
            'client' => $clientUserSelected,
            'created_at' => now()
        ]);

    	Log::info('[VD] Palabra creada:' . json_encode($palabra) . ' User:'  . Auth::user()->email . '. Cliente: ' . $clientUserSelected);
    	return redirect()
            ->route('vd.noticias')
            ->with('success', "Se ha creado la palabra: " . $palabra->word);
    }

    /* Elimina una palabra*/
    public function eliminar_palabra(vd_words $palabra){
    	$this->verifyPermission('VdNews', $palabra); //primero verificamos los permisos
		$palabra->delete();
		
    	Log::info('[VD] Palabra eliminada:' . $palabra->word . ' User:'  . Auth::user()->email);
    	 return redirect()
            ->route('vd.noticias')
            ->with('success', "Se ha eliminado la palabra: " . $palabra->word);
    }


    /* Cambia el estado a descartado de una noticia*/
    public function descartar_noticia(vd_news $noticia){
        $this->verifyPermission('VdNews', $noticia); //primero verificamos los permisos
        $noticia->discarted_at = now();
        $noticia->status = "Descartada";
        $noticia->save();

        Log::info('[VD] Noticia descartada: ' . $noticia->title . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('vd.noticias')
            ->with('success', "Se ha descartado la noticia: $noticia->title");
    }

     /* Cambia el estado a reportado de una noticia*/
    public function reportar_noticia(vd_news $noticia){
        $this->verifyPermission('VdNews', $noticia); //primero verificamos los permisos
        $groups = groups::all();
        $types = type_ticket::all();
        return view('vd/reportar_noticia', compact('noticia', 'groups', 'types'));
    }

    /* Crea un ticket y/o unanotificación de la noticia*/
    public function crear_ticket_notificacion(vd_news $noticia){
        $this->verifyPermission('VdNews'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $datos = request()->all();

        //creamos la notificacion
        if(isset($datos['crear_notificacion'])){
            $notificacion = vd_notifications::create([
                'type' => $datos['notification_type'],
                'title' => $datos['notification_title'],
                'source' => $datos['source'],
                'link' => $datos['link'],
                'tool' => $datos['tool'],
                'notificated_at' => $datos['notificated_at'],
                'client' => $clientUserSelected,
                'created_at' => now()
             ]);
            Log::info('[VD] Notificación creada:' . json_encode($notificacion) . ' User:'  . Auth::user()->email);
        }

        //creamos el ticket
        if(isset($datos['crear_ticket'])){
            $ticket = new ticketsController;
            $ticket->crear_ticket(); 
        }  

        $noticia->status = "Reportada";
        $noticia->save();

        return redirect()
            ->route('vd.noticias')
            ->with('success', "Se ha reportado la noticia:" .  $datos['title']);
    }

     /* Cambia el estado a Activo de una  noticia*/
    public function activar_noticia(vd_news $noticia){
        $this->verifyPermission('VdNews', $noticia); //primero verificamos los permisos
        $noticia->discarted_at = now();
        $noticia->status = "Activa";
        $noticia->discarted_at = null;
        $noticia->save();

        Log::info('[VD] Noticia activada: ' . $noticia->title . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('vd.noticias')
            ->with('success', "Se ha activado la noticia: $noticia->title");
    }


    /* Enlace a la vista de notificaciones*/
    public function notificaciones(){
        $this->verifyPermission('VdNotifications'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;

        $notificaciones = vd_notifications::query()
                            ->where('client', $clientUserSelected)
                            ->orderby('created_at', 'desc')
                            ->get();
        return view('vd/notificaciones', compact('notificaciones'));
    }


    /* Enlace a la vista de crear una nueva notificacion*/
    public function nueva_notificacion(){
        $this->verifyPermission('VdNotifications'); //primero verificamos los permisos
        return view('vd/nueva_notificacion');
    }

    /* Crea una nueva notificacion*/
    public function crear_notificacion(){
        $this->verifyPermission('VdNotifications'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $datos = request()->all();

        $notificacion = vd_notifications::create([
            'type' => $datos['type'],
            'title' => $datos['title'],
            'source' => $datos['source'],
            'link' => $datos['link'],
            'tool' => $datos['tool'],
            'notificated_at' => $datos['notificated_at'],
            'client' => $clientUserSelected,
            'created_at' => now()
        ]);

        Log::info('[VD] Notificación creada:' . json_encode($notificacion) . ' User:'  . Auth::user()->email);
        return redirect()
            ->route('vd.notificaciones')
            ->with('success', "Se ha creado la notificación.");
    }

    /* EExporta las  notificaciones*/
    public function exportar_notificaciones(){
        $this->verifyPermission('VdNotifications'); //primero verificamos los permisos
        $clientUserSelected = Auth::user()->client;
        $notificaciones = vd_notifications::query()->where('client', $clientUserSelected);

        return (new ModalExport($notificaciones))->download('notificaciones' . Auth::user()->clientName . '.csv');


    }


}
