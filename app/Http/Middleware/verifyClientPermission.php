<?php

namespace App\Http\Middleware;

use Closure;
use \App\clients;
use \App\User;

class verifyClientPermission
{
    /**
     * Este middleware veririca a qué cliente tienes  permiso y lo selecciona automáticamente
     */

    public function handle($request, Closure $next)
    {
        

        //Si ya tiene el cliente puesto, lo dejamos.
        if($request->user()->client > 0) return $next($request);
        $permissions = $request->user()->getAllPermissions();
        //si el usuario es admin, se le asigna el primer cliente
        if($request->user()->role == 'Administrador'){
            //guardamos en la bbdd el cliente
            $user = new User();
            $user = $request->user();
            $c = clients::first();
            $user->client = $c->id;
            $user->clientName = $c->name;
            $user->save(); 

            return $next($request);
        }

        $encontrado = 0;
        $i = 0;
        //Si no se ha encontrado, devolvemos una página de que no se tienen permisos para ver ningún cliente
        if (!isset($permissions[$i]))return abort(403);

        while ($encontrado != 1 && $permissions[$i] != null) {
            $p = $permissions[$i]->name;

            //obtenemos los permisos del usuario
            $j = strlen($permissions[$i]->name);
            $num = "";
            //obtenemos la ID del cliente por el permiso encontrado
            while (is_numeric(substr($permissions[$i]->name, $j-1, strlen($permissions[$i]->name)))) {
                $num = substr($permissions[$i]->name, $j-1, $j);
                $j = $j - 1;
            }
            //buscamos si existe el cliente del cual se tiene permiso
            if (clients::find($num) != null) $encontrado = 1;
            //si no se ha encontrado, se sigue hasta encontrar un cliente que exista
            $i++;
        }

        $user = new User();
        $user = $request->user();
        $c = clients::find($num);
        //guaramos en el usuario la ID del cliente y el nombre.
        $user->client = $c->id;
        $user->clientName = $c->name;
        $user->save();
        return $next($request);
    }
}
